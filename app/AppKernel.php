<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            // Utils
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Vich\UploaderBundle\VichUploaderBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new \DataDog\PagerBundle\DataDogPagerBundle(),
            new JMose\CommandSchedulerBundle\JMoseCommandSchedulerBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),
            new ADesigns\CalendarBundle\ADesignsCalendarBundle(),
            new EWZ\Bundle\RecaptchaBundle\EWZRecaptchaBundle(),
            new Tetranz\Select2EntityBundle\TetranzSelect2EntityBundle(),
            new Endroid\QrCode\Bundle\QrCodeBundle\EndroidQrCodeBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new Nelmio\CorsBundle\NelmioCorsBundle(),


            new FM\ElfinderBundle\FMElfinderBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new Oneup\UploaderBundle\OneupUploaderBundle(),
            new Mopa\Bundle\BootstrapBundle\MopaBootstrapBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),


            // FCM
            new FCM\ThemeBundle\FCMThemeBundle(),
            new FCM\UserBundle\FCMUserBundle(),
            new FCM\PermissaoBundle\FCMPermissaoBundle(),
            new FCM\EventoBundle\FCMEventoBundle(),
            new FCM\QuestionBundle\FCMQuestionBundle(),
            new FCM\BoletoFuncampBundle\FCMBoletoFuncampBundle(),
            new FCM\LabelBundle\FCMLabelBundle(),
            new FCM\ApiBundle\FCMApiBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
