#!/bin/bash

sudo git pull origin master
sudo php app/console doctrine:schema:update --force
sudo php app/console cache:clear --env=prod
sudo php app/console assetic:dump --env=prod
sudo chown nobody:nobody -R *
