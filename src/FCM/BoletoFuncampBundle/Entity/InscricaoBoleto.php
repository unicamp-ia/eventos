<?php

namespace FCM\BoletoFuncampBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DomCrawler\Crawler;
use FCM\BoletoFuncampBundle\Validator\Constraints as InscricaoBoletoAssert;
use JMS\Serializer\Annotation as Serializer;

/**
 * InscricaoBoleto
 *
 * @ORM\Table(name="inscricao_boleto")
 * @ORM\Entity(repositoryClass="FCM\BoletoFuncampBundle\Repository\InscricaoBoletoRepository")
 * @InscricaoBoletoAssert\InscricaoBoleto()
 * @Serializer\ExclusionPolicy("all")
 */
class InscricaoBoleto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="\FCM\EventoBundle\Entity\Inscricao", inversedBy="boleto")
     * @ORM\JoinColumn(name="inscricao_id", referencedColumnName="id")
     */
    private $inscricao;

    /**
     * @var int
     * 0 - Gerado
     * 1 - Enviado
     * 2 - Pago
     *
     * @ORM\Column(name="status", type="smallint")
     * @Serializer\Expose()
     */
    private $status;



    /**
     * @var int
     *
     * @ORM\Column(name="inscricao_funcamp", type="smallint", nullable=true)
     */
    private $inscricaoFuncamp;

    /**
     * @return int
     */
    public function getInscricaoFuncamp()
    {
        return $this->inscricaoFuncamp;
    }

    /**
     * @param int $inscricaoFuncamp
     */
    public function setInscricaoFuncamp($inscricaoFuncamp)
    {
        $this->inscricaoFuncamp = $inscricaoFuncamp;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2)
     * @Serializer\Expose()
     */
    private $valor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_baixa", type="datetime", nullable=true)
     * @Serializer\Expose()
     */
    private $dataBaixa;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_vencimento", type="datetime", nullable=true)
     */
    private $dataVencimento;

    /**
     * @return \DateTime
     */
    public function getDataVencimento()
    {
        return $this->dataVencimento;
    }

    /**
     * @param \DateTime $dataVencimento
     */
    public function setDataVencimento(\DateTime $dataVencimento)
    {
        $this->dataVencimento = $dataVencimento;
    }

    /**
     * @return \DateTime
     */
    public function getDataBaixa()
    {
        return $this->dataBaixa;
    }

    /**
     * @param \DateTime $dataBaixa
     */
    public function setDataBaixa($dataBaixa)
    {
        $this->dataBaixa = $dataBaixa;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * @param \DateTime $lastUpdate
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="url_boleto", type="text", nullable=true)
     */
    private $urlBoleto;

    /**
     * @ORM\ManyToOne(targetEntity="EventoAtividadeBoleto", inversedBy="boletos")
     * @ORM\JoinColumn(name="evento_atividade_boleto_id", referencedColumnName="id")
     */
    private $eventoAtividadeBoleto;

    /**
     * @return string
     */
    public function getUrlBoleto()
    {
        return $this->urlBoleto;
    }

    /**
     * @param string $urlBoleto
     */
    public function setUrlBoleto($urlBoleto)
    {
        $this->urlBoleto = $urlBoleto;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return InscricaoBoleto
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns all status choices
     * @return array
     */
    public static function getStatusChoices(){
        return [
            0 => 'Gerado',
            1 => 'Enviado',
            2 => 'Pago',
            9 => 'Isento',
        ];
    }

    /**
     * Returns a status label
     * @return string

     */
    public function getStatusLabel(){
        switch ($this->status){
            case 0: return 'Gerado'; break;
            case 1: return 'Enviado'; break;
            case 2: return 'Pago'; break;
            case 9: return 'Isento'; break;

        }
    }

    /**
     * Set valor
     *
     * @param string $valor
     *
     * @return InscricaoBoleto
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return InscricaoBoleto
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set inscricao
     *
     * @param \FCM\EventoBundle\Entity\Inscricao $inscricao
     *
     * @return InscricaoBoleto
     */
    public function setInscricao(\FCM\EventoBundle\Entity\Inscricao $inscricao = null)
    {
        $this->inscricao = $inscricao;

        return $this;
    }

    /**
     * Get inscricao
     *
     * @return \FCM\EventoBundle\Entity\Inscricao
     */
    public function getInscricao()
    {
        return $this->inscricao;
    }


    /**
     * Consulta na FUNCAMP o pagamento e atualiza lastupdate
     * @return bool
     */
    public function consultar(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getEventoAtividadeBoleto()->getReimpressao());
        curl_setopt($ch, CURLOPT_POST, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "identificador=". $this->getEventoAtividadeBoleto()->getIdentificador()
            . '&codigoinscricao='.$this->getInscricaoFuncamp()
            . '&documento='.$this->getInscricao()->getParticipante()->getDocumento()
            . '&email='.$this->getInscricao()->getParticipante()->getEmail()
        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        $crawler = new Crawler($result);

        if($crawler->filter('flagconfirmado')->count() > 0){
            // Boleto pago
            if($crawler->filter('flagconfirmado')->first()->text() != 'N') {
                $this->setStatus(2); // Pago
            }
        }

        $this->setLastUpdate(new \DateTime());

        return false;
    }



    /**
     * Set eventoAtividadeBoleto
     *
     * @param \FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto $eventoAtividadeBoleto
     *
     * @return InscricaoBoleto
     */
    public function setEventoAtividadeBoleto(\FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto $eventoAtividadeBoleto = null)
    {
        $this->eventoAtividadeBoleto = $eventoAtividadeBoleto;

        return $this;
    }

    /**
     * Get eventoAtividadeBoleto
     *
     * @return \FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto
     */
    public function getEventoAtividadeBoleto()
    {
        return $this->eventoAtividadeBoleto;
    }
}
