<?php

namespace FCM\BoletoFuncampBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EventoAtividadeBoleto
 *
 * @ORM\Table(name="evento_atividade_boleto")
 * @ORM\Entity(repositoryClass="FCM\BoletoFuncampBundle\Repository\EventoAtividadeBoletoRepository")
 */
class EventoAtividadeBoleto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function __construct()
    {
        $this->boletos = new ArrayCollection();

    }

    /**
     * @ORM\OneToOne(targetEntity="\FCM\EventoBundle\Entity\EventoAtividade", inversedBy="atividadeBoleto")
     * @ORM\JoinColumn(name="evento_atividade_id", referencedColumnName="id")
     */
    private $eventoAtividade;

    /**
     * @ORM\OneToMany(targetEntity="InscricaoBoleto", mappedBy="eventoAtividadeBoleto", cascade={"remove","persist"})
     */
    private $boletos;

    /**
     * @var string
     *
     * @ORM\Column(name="envio_post", type="text")
     */
    private $envioPost;



    /**
     * Indica se inscricao sera oculta
     * @var int
     *
     * @ORM\Column(name="auto", type="boolean", nullable=true)
     */
    private $auto;

    /**
     * @return int
     */
    public function getAuto()
    {
        return $this->auto;
    }

    /**
     * @param int $auto
     */
    public function setAuto($auto)
    {
        $this->auto = $auto;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="reimpressao", type="text")
     */
    private $reimpressao;

    /**
     * @var string
     *
     * @ORM\Column(name="lista_inscricoes", type="text")
     */
    private $listaInscricoes;

    /**
     * @return string
     */
    public function getListaInscricoes()
    {
        return $this->listaInscricoes;
    }

    /**
     * @param string $listaInscricoes
     */
    public function setListaInscricoes($listaInscricoes)
    {
        $this->listaInscricoes = $listaInscricoes;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text")
     */
    private $descricao;

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @var array
     *
     * @ORM\Column(name="token", type="string")
     */
    private $token;

    /**
     * @var array
     *
     * @ORM\Column(name="identificador", type="string")
     */
    private $identificador;

    /**
     * @return array
     */
    public function getIdentificador()
    {
        return $this->identificador;
    }

    /**
     * @param array $identificador
     */
    public function setIdentificador($identificador)
    {
        $this->identificador = $identificador;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vencimento", type="datetime", nullable=true)
     */
    private $vencimento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dias_vencimento", type="integer", nullable=true)
     */
    private $diasVencimento;

    /**
     * @return \DateTime
     */
    public function getDiasVencimento()
    {
        return $this->diasVencimento;
    }

    /**
     * @param \DateTime $diasVencimento
     */
    public function setDiasVencimento($diasVencimento)
    {
        $this->diasVencimento = $diasVencimento;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="valor_padrao", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valorPadrao;

    /**
     * @return \DateTime
     */
    public function getVencimento()
    {
        return $this->vencimento;
    }

    /**
     * @param \DateTime $vencimento
     */
    public function setVencimento($vencimento)
    {
        $this->vencimento = $vencimento;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set envioPost
     *
     * @param string $envioPost
     *
     * @return EventoAtividadeBoleto
     */
    public function setEnvioPost($envioPost)
    {
        $this->envioPost = $envioPost;

        return $this;
    }

    /**
     * Get envioPost
     *
     * @return string
     */
    public function getEnvioPost()
    {
        return $this->envioPost;
    }

    /**
     * Set reimpressao
     *
     * @param string $reimpressao
     *
     * @return EventoAtividadeBoleto
     */
    public function setReimpressao($reimpressao)
    {
        $this->reimpressao = $reimpressao;

        return $this;
    }

    /**
     * Get reimpressao
     *
     * @return string
     */
    public function getReimpressao()
    {
        return $this->reimpressao;
    }

    /**
     * Set token
     *
     * @param array $token
     *
     * @return EventoAtividadeBoleto
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return array
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set valorPadrao
     *
     * @param string $valorPadrao
     *
     * @return EventoAtividadeBoleto
     */
    public function setValorPadrao($valorPadrao)
    {
        $this->valorPadrao = $valorPadrao;

        return $this;
    }

    /**
     * Get valorPadrao
     *
     * @return string
     */
    public function getValorPadrao()
    {
        return $this->valorPadrao;
    }

    /**
     * Set eventoAtividade
     *
     * @param \FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade
     *
     * @return EventoAtividadeBoleto
     */
    public function setEventoAtividade(\FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade = null)
    {
        $this->eventoAtividade = $eventoAtividade;

        return $this;
    }

    /**
     * Get eventoAtividade
     *
     * @return \FCM\EventoBundle\Entity\EventoAtividade
     */
    public function getEventoAtividade()
    {
        return $this->eventoAtividade;
    }

    /**
     * Add boleto
     *
     * @param \FCM\BoletoFuncampBundle\Entity\InscricaoBoleto $boleto
     *
     * @return EventoAtividadeBoleto
     */
    public function addBoleto(\FCM\BoletoFuncampBundle\Entity\InscricaoBoleto $boleto)
    {
        $this->boletos[] = $boleto;

        return $this;
    }

    /**
     * Remove boleto
     *
     * @param \FCM\BoletoFuncampBundle\Entity\InscricaoBoleto $boleto
     */
    public function removeBoleto(\FCM\BoletoFuncampBundle\Entity\InscricaoBoleto $boleto)
    {
        $this->boletos->removeElement($boleto);
    }

    /**
     * Get boletos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBoletos()
    {
        return $this->boletos;
    }
}
