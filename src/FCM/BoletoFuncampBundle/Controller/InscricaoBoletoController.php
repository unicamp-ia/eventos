<?php

namespace FCM\BoletoFuncampBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto;
use FCM\BoletoFuncampBundle\Entity\InscricaoBoleto;
use FCM\BoletoFuncampBundle\Repository\EventoAtividadeBoletoRepository;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Eventoatividadeboleto controller.
 *
 * @Route("/admin/inscricao/{inscricao}/boleto")
 */
class InscricaoBoletoController extends Controller
{

    /**
     * @Route("/boletofuncamp/notify", name="ajax_boletofuncamp_notify",options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function ajaxNotifyAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var InscricaoBoleto $boleto */
        $boleto = $em->getRepository('FCMBoletoFuncampBundle:InscricaoBoleto')->find($request->get('boleto'));

        $this->sendNotifyAction($boleto);

        /** @var Response $template */
        $template = $this->render('FCMThemeBundle:Default:ajax.mail.html.twig', [
            'status' => 'success'
        ]);

        $json = json_encode($template->getContent());
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Lists all eventoAtividadeBoleto entities.
     *
     * @Route("/boletofuncamp", name="admin_inscricao_boletofuncamp_index")
     * @Method("GET")
     */
    public function indexAction(EventoAtividade $eventoAtividade)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EventoAtividadeBoletoRepository $repo */
        $repo = $em->getRepository('FCMBoletoFuncampBundle:EventoAtividadeBoleto');

        /** @var ArrayCollection $eventoAtividadeBoletos */
        $eventoAtividadeBoleto = $repo->findOneBy(['eventoAtividade' => $eventoAtividade->getId()]);

        if($eventoAtividadeBoleto instanceof EventoAtividadeBoleto){
            $action_links = [
                'edit' => [
                    'url' => $this->generateUrl('admin_evento_atividade_boletofuncamp_index', [
                        'eventoAtividade' => $eventoAtividade->getId(),
                        'id' => $eventoAtividadeBoleto->getId(),
                    ]),
                    'label' => 'Editar'
                ]
                /* @todo Incluir inscricoes e boletos */
            ];
        } else {
            $action_links = [
                'new' => [
                    'url' => $this->generateUrl('admin_evento_atividade_boletofuncamp_new', [
                        'eventoAtividade' => $eventoAtividade->getId(),
                    ]),
                    'label' => 'Incluir'
                ]
            ];
        }


        return $this->render('FCMBoletoFuncampBundle:EventoAtividadeBoleto:index.html.twig', array(
            'page_title' => 'Boleto Funcamp',
            'page_title_desc' => $eventoAtividade->getEvento()->getNome() . ' - ' . $eventoAtividade->getNome(),
            'eventoAtividadeBoleto' => $eventoAtividadeBoleto,
            'action_links' => $action_links,
            'back' => $this->generateUrl('admin_evento_atividade_edit',[
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId()
            ])
        ));

    }

    /**
     * Creates a new eventoAtividadeBoleto entity.
     *
     * @Route("/new", name="admin_inscricao_boleto_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Inscricao $inscricao)
    {


        /** @var EventoAtividadeBoleto $atividadeBoleto */
        $atividadeBoleto = $inscricao->getEventoAtividade()->getAtividadeBoleto();

        /** @var InscricaoBoleto $inscricaoBoleto */
        if(!$inscricaoBoleto = $inscricao->getBoleto()){
            $inscricaoBoleto = new InscricaoBoleto();
        }

        $inscricaoBoleto->setInscricao($inscricao);
        $inscricaoBoleto->setValor($atividadeBoleto->getValorPadrao());

        $form = $this->createForm('FCM\BoletoFuncampBundle\Form\InscricaoBoletoType', $inscricaoBoleto);
        $form->handleRequest($request);

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('admin_evento_atividade_presenca_index',[
                'evento' => $inscricaoBoleto->getInscricao()->getEventoAtividade()->getEvento()->getId(),
                'eventoAtividade' => $inscricaoBoleto->getInscricao()->getEventoAtividade()->getId()
            ]);
        }


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $inscricaoBoleto->setCreated(new \DateTime());
            $inscricaoBoleto->setEventoAtividadeBoleto($inscricao->getEventoAtividade()->getAtividadeBoleto());

            // Deve possuir valor e nao ser isento
            if($inscricaoBoleto->getValor() && $inscricaoBoleto->getStatus() != 9){
                if($this->get('fcm_boleto_funcamp.boleto_tasker')->requestNew($inscricaoBoleto)){
                    $inscricaoBoleto->setStatus(1);
                    $em->persist($inscricaoBoleto);
                    $em->flush();
                    $this->addFlash('success','Boleto gerado e enviado ao participante com sucesso');
                    $this->sendMessageAction($inscricaoBoleto);

                } else {
                    $this->addFlash('error','Erro ao gerar boleto');
                }
            } else { // Valor zerado
                $inscricaoBoleto->setValor(0);
                $em->persist($inscricaoBoleto);
                $em->flush();
                $this->addFlash('success','Forma de pagamento salva com sucesso');
            }

            return $this->redirectToRoute('admin_inscricao_boleto_show',[
                'inscricao' => $inscricao->getId(),
                'id' => $inscricaoBoleto->getId(),
                'back' => $back,
            ]);

        }


        return $this->render('FCMBoletoFuncampBundle:InscricaoBoleto:edit.html.twig', array(
            'page_title' => 'Gerar boleto',
            'inscricao' => $inscricao,
            'page_title_desc' => $inscricao->getEventoAtividade()->getEvento()->getNome() . ' - ' . $inscricao->getEventoAtividade()->getNome(),
            'back' => $back,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing eventoAtividadeBoleto entity.
     *
     * @Route("/{id}/show", name="admin_inscricao_boleto_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, InscricaoBoleto $inscricaoBoleto)
    {
        if(!empty($request->get('back'))) $back = $request->get('back');
        else $back = $this->generateUrl('evento_atividade_index',['evento' => $inscricaoBoleto->getEventoAtividadeBoleto()
            ->getEventoAtividade()->getEvento()->getId()]);


        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $inscricao = $inscricaoBoleto->getInscricao();

        $inscricaoBoleto->consultar();

        $em->persist($inscricaoBoleto);
        $em->flush();

        return $this->render('FCMBoletoFuncampBundle:InscricaoBoleto:show.html.twig', array(
            'page_title' => 'Boleto',
            'boleto' => $inscricaoBoleto,
            'page_title_desc' => $inscricao->getEventoAtividade()->getEvento()->getNome() . ' - ' . $inscricao->getEventoAtividade()->getNome(),
            'back' => $back,
        ));
    }


    /**
     * Displays a form to edit an existing eventoAtividadeBoleto entity.
     *
     * @Route("/{id}/edit", name="admin_inscricao_boleto_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, InscricaoBoleto $inscricaoBoleto)
    {
        $form = $this->createForm('FCM\BoletoFuncampBundle\Form\InscricaoBoletoType', $inscricaoBoleto,['type' => 'edit']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $inscricaoBoleto->setCreated(new \DateTime());
            $inscricaoBoleto->setEventoAtividadeBoleto($inscricaoBoleto->getInscricao()->getEventoAtividade()->getAtividadeBoleto());

            $em->persist($inscricaoBoleto);
            $em->flush();
            $this->addFlash('success','Forma de pagamento paga com sucesso');

        }

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('admin_evento_atividade_presenca_index',[
                'evento' => $inscricaoBoleto->getInscricao()->getEventoAtividade()->getEvento()->getId(),
                'eventoAtividade' => $inscricaoBoleto->getInscricao()->getEventoAtividade()->getId()
            ]);
        }

        return $this->render('FCMBoletoFuncampBundle:InscricaoBoleto:edit.html.twig', array(
            'page_title' => 'Editar boleto ' . $inscricaoBoleto->getId(),
            'back' => $back,
            'inscricao' => $inscricaoBoleto->getInscricao(),
            'boleto' => $inscricaoBoleto,
            'page_title_desc' => $inscricaoBoleto->getInscricao()->getEventoAtividade()->getEvento()->getNome() . ' - '
                . $inscricaoBoleto->getInscricao()->getEventoAtividade()->getNome(),

            'form' => $form->createView(),

        ));
    }

    /**
     * @param InscricaoBoleto $inscricaoBoleto
     */
    public function sendNotifyAction(InscricaoBoleto $inscricaoBoleto){
        $eventoAtividade = $inscricaoBoleto->getEventoAtividadeBoleto()->getEventoAtividade();

        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Evento] ' . $eventoAtividade->getEvento()->getNome() . ' Boleto para pagamento')
            ->setFrom($this->getParameter('mail'))
            ->setTo($inscricaoBoleto->getInscricao()->getParticipante()->getEmail())
            ->addCc($eventoAtividade->getEvento()->getEmail())
            ->setReplyTo($eventoAtividade->getEvento()->getEmail())
            ->setBody(
                $this->render('FCMBoletoFuncampBundle:Emails:boleto.notify.html.twig',[
                    'eventoAtividade' => $eventoAtividade,
                    'inscricaoBoleto' => $inscricaoBoleto,
                ]),
                'text/html'
            );

        $this->get('mailer')->send($message);
    }

    /**
     * @param InscricaoBoleto $inscricaoBoleto
     */
    public function sendMessageAction(InscricaoBoleto $inscricaoBoleto){
        $eventoAtividade = $inscricaoBoleto->getEventoAtividadeBoleto()->getEventoAtividade();

        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Evento] ' . $eventoAtividade->getEvento()->getNome() . ' Boleto para pagamento')
            ->setFrom($this->getParameter('mail'))
            ->setTo($inscricaoBoleto->getInscricao()->getParticipante()->getEmail())
            ->addCc($eventoAtividade->getEvento()->getEmail())
            ->setReplyTo($eventoAtividade->getEvento()->getEmail())
            ->setBody(
                $this->render('FCMBoletoFuncampBundle:Emails:boleto.html.twig',[
                    'eventoAtividade' => $eventoAtividade,
                    'inscricaoBoleto' => $inscricaoBoleto,
                ]),
                'text/html'
            );

        $this->get('mailer')->send($message);
    }
}
