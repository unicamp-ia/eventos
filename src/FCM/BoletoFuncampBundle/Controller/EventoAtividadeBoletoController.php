<?php

namespace FCM\BoletoFuncampBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto;
use FCM\BoletoFuncampBundle\Entity\InscricaoBoleto;
use FCM\BoletoFuncampBundle\Repository\EventoAtividadeBoletoRepository;
use FCM\EventoBundle\Entity\EventoAtividade;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;

/**
 * Eventoatividadeboleto controller.
 *
 * @Route("/admin/evento/{evento}/eventoAtividade/{eventoAtividade}/boletofuncamp")
 */
class EventoAtividadeBoletoController extends Controller
{
    /**
     * Lists all eventoAtividadeBoleto entities.
     *
     * @Route("/boletofuncamp", name="admin_evento_atividade_boletofuncamp_index")
     * @Method("GET")
     */
    public function indexAction(EventoAtividade $eventoAtividade)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EventoAtividadeBoletoRepository $repo */
        $repo = $em->getRepository('FCMBoletoFuncampBundle:EventoAtividadeBoleto');

        /** @var ArrayCollection $eventoAtividadeBoletos */
        $eventoAtividadeBoleto = $repo->findOneBy(['eventoAtividade' => $eventoAtividade->getId()]);

        if($eventoAtividadeBoleto instanceof EventoAtividadeBoleto){
            $action_links = [
                'edit' => [
                    'url' => $this->generateUrl('admin_evento_atividade_boletofuncamp_edit', [
                        'eventoAtividade' => $eventoAtividade->getId(),
                        'id' => $eventoAtividadeBoleto->getId(),
                    ]),
                    'label' => 'Editar'
                ]
                /* @todo Incluir inscricoes e boletos */
            ];
        } else {
            $action_links = [
                'new' => [
                    'url' => $this->generateUrl('admin_evento_atividade_boletofuncamp_new', [
                        'eventoAtividade' => $eventoAtividade->getId(),
                    ]),
                    'label' => 'Incluir'
                ]
            ];
        }


        return $this->render('FCMBoletoFuncampBundle:EventoAtividadeBoleto:index.html.twig', array(
            'page_title' => 'Boleto Funcamp',
            'page_title_desc' => $eventoAtividade->getEvento()->getNome() . ' - ' . $eventoAtividade->getNome(),
            'eventoAtividadeBoleto' => $eventoAtividadeBoleto,
            'action_links' => $action_links,
            'back' => $this->generateUrl('admin_evento_atividade_edit',[
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId()
            ])
        ));

    }

    /**
     * Creates a new eventoAtividadeBoleto entity.
     *
     * @Route("/new", name="admin_evento_atividade_boletofuncamp_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, EventoAtividade $eventoAtividade)
    {
        $eventoAtividadeBoleto = new Eventoatividadeboleto();
        $eventoAtividadeBoleto->setEventoAtividade($eventoAtividade);
        $eventoAtividadeBoleto->setDescricao('Inscrição do evento ' . $eventoAtividade->getEvento()->getNome() . ' - ' . $eventoAtividade->getNome());

        $form = $this->createForm('FCM\BoletoFuncampBundle\Form\EventoAtividadeBoletoType', $eventoAtividadeBoleto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($eventoAtividadeBoleto);
            $em->flush($eventoAtividadeBoleto);
            $this->addFlash('success','Inclusão efetuada com sucesso');

            return $this->redirectToRoute('admin_evento_atividade_edit',[
                'eventoAtividade' => $eventoAtividade->getId(),
                'evento' => $eventoAtividade->getEvento()->getId(),

            ]);
        }

        return $this->render('FCMBoletoFuncampBundle:EventoAtividadeBoleto:edit.html.twig', array(
            'page_title' => 'Incluir boleto funcamp',
            'page_title_desc' => $eventoAtividade->getEvento()->getNome() . ' - ' . $eventoAtividade->getNome(),
            'back' => $this->generateUrl('admin_evento_atividade_edit',[
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId()
            ]),
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing eventoAtividadeBoleto entity.
     *
     * @Route("/{id}/edit", name="admin_evento_atividade_boletofuncamp_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, EventoAtividadeBoleto $eventoAtividadeBoleto)
    {
        $eventoAtividade = $eventoAtividadeBoleto->getEventoAtividade();

        //$this->consultaPagamentos($eventoAtividadeBoleto);

        $form = $this->createForm('FCM\BoletoFuncampBundle\Form\EventoAtividadeBoletoType', $eventoAtividadeBoleto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Alteração efetuada com sucesso');

            return $this->redirectToRoute('admin_evento_atividade_edit',[
                'id' => $eventoAtividade->getId(),
                'evento' => $eventoAtividade->getEvento()->getId(),
            ]);
        }

        return $this->render('FCMBoletoFuncampBundle:EventoAtividadeBoleto:edit.html.twig', array(
            'page_title' => 'Incluir boleto funcamp',
            'page_title_desc' => $eventoAtividade->getEvento()->getNome() . ' - ' . $eventoAtividade->getNome(),
            'back' => $this->generateUrl('admin_evento_atividade_edit',[
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId()
            ]),
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing eventoAtividadeBoleto entity.
     *
     * @Route("/pagamentos", name="admin_evento_atividade_boletofuncamp_pagamentos")
     * @Method({"GET", "POST"})
     */
    public function pagamentosAction(Request $request, EventoAtividadeBoleto $eventoAtividadeBoleto)
    {
        $eventoAtividade = $eventoAtividadeBoleto->getEventoAtividade();

        return $this->render('FCMBoletoFuncampBundle:EventoAtividadeBoleto:pagamentos.html.twig', array(
            'page_title' => 'Pagamentos',
            'eventoAtividade' => $eventoAtividade,
            'inscricoes' => $eventoAtividade->getInscricoes(),
            'evento' => $eventoAtividade->getEvento(),
            'questionarios' => $eventoAtividade->getQuestionarios(),
            'page_title_desc' => $eventoAtividade->getEvento()->getNome() . ' - ' . $eventoAtividade->getNome(),
            'back' => $this->generateUrl('admin_evento_atividade_edit',[
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId()
            ]),
        ));
    }


}
