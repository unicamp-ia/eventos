<?php

namespace FCM\BoletoFuncampBundle\Form;

use FCM\BoletoFuncampBundle\Entity\InscricaoBoleto;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscricaoBoletoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('status', ChoiceType::class, [
                'label' => 'Status',
                'choices' => array_flip(InscricaoBoleto::getStatusChoices()),
            ])
            ->add('dataVencimento', DateType::class,[
                'required' => true,
                'disabled' => $options['type'] == 'new' ? false : true,
                'attr' => array('class'=>'form_date_picker_date'),
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                ])
            ->add('valor', NumberType::class, [
                'label' => 'Valor',
                'scale' => 2,
                'disabled' => $options['type'] == 'new' ? false : true,
            ])
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\BoletoFuncampBundle\Entity\InscricaoBoleto',
            'type' => 'new',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fcm_boletofuncampbundle_eventoatividadeboleto';
    }


}
