<?php

namespace FCM\BoletoFuncampBundle\Form;


use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventoAtividadeBoletoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descricao', TextType::class, [
                'label' => 'Descrição a ser exibida no boleto',
            ])
            ->add('envioPost', TextType::class, [
                'label' => 'Link para o formulário via POST'
            ])

            ->add('reimpressao',TextType::class, [
                'label' => 'Link para reimpressão'
            ])

            ->add('listaInscricoes',TextType::class, [
                'label' => 'Link para listar inscrições (utilizado para verificar pagamentos, terminados /ListarInscricoes)'
            ])

            ->add('token', TextType::class, [
                'label' => 'Token FUNCAMP'
            ])
            ->add('identificador', TextType::class, [
                'label' => 'Identificador'
            ])
            ->add('vencimento', DateType::class, [
                'label' => 'Vencimento dos boletos',
                'attr' => array('class'=>'form_date_picker_date'),
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'format' => 'dd/MM/yyyy',
                ])
            ->add('diasVencimento',NumberType::class,[
                'label' => 'Dias para vencimento após inscrição',
                'required' => false,
            ])
            ->add('valorPadrao', NumberType::class, [
                'label' => 'Valor padrão dos boletos',
                'required' => false,
                'scale' => 2
            ])
            ->add('auto', CheckboxType::class, [
                'label' => 'Enviar boleto automaticaente',
                'required' => false,
            ])
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fcm_boletofuncampbundle_eventoatividadeboleto';
    }


}
