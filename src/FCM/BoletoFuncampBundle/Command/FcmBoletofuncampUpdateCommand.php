<?php

namespace FCM\BoletoFuncampBundle\Command;

use Doctrine\ORM\EntityManager;
use FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto;
use FCM\BoletoFuncampBundle\Entity\InscricaoBoleto;
use FCM\EventoBundle\Entity\Evento;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class FcmBoletofuncampUpdateCommand extends ContainerAwareCommand
{
    /** @var  OutputInterface */
    protected $output;

    /** @var  EntityManager */
    protected $em;

    protected function configure()
    {
        // todo: Implement this
        $this
            ->setName('fcm:boletofuncamp:update')
            ->setDescription('Envia os boletos e atualiza seus status')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $this->em = $em = $this->getContainer()->get('doctrine')->getEntityManager();

        $this->output = $output;

        // Envia boletos
        $nao_enviados =  $em->getRepository('FCMBoletoFuncampBundle:InscricaoBoleto')->findBy(['status' => 0]);

        foreach ($nao_enviados as $inscricaoBoleto){
            $this->sendMessageAction($inscricaoBoleto);
            $inscricaoBoleto->setStatus(1);

            $output->writeln('Boleto enviado:' . $inscricaoBoleto->getUrlBoleto());

            $em->persist($inscricaoBoleto);
            $em->flush();
        }

        // Envia boletos
        $nao_pagos =  $em->getRepository('FCMBoletoFuncampBundle:InscricaoBoleto')->findBy(['status' => 1]);

        $eventoAtividadeBoletos = $this->em->getRepository('FCMBoletoFuncampBundle:EventoAtividadeBoleto')
            ->findBy(array(),['id' => 'DESC']);

        $today = new \DateTime();
        $today->modify('-3 months');

        /** @var EventoAtividadeBoleto $eventoAtividadeBoleto */
        foreach ($eventoAtividadeBoletos as $eventoAtividadeBoleto){
            if($eventoAtividadeBoleto->getListaInscricoes()){
                // Verifica somente eventos que ocorreram a no maximo 3 meses
                if($eventoAtividadeBoleto->getEventoAtividade()->getDataIniAtividade() > $today){
                    $this->output->writeln('Vericando ' . $eventoAtividadeBoleto->getEventoAtividade()->getEvento()->getNome() .
                        ' - ' . $eventoAtividadeBoleto->getEventoAtividade()->getNome());
                    $this->consultaPagamentos($eventoAtividadeBoleto);
                }
            }

        }

        $output->writeln('Finalizado');
    }

    public function consultaPagamentos(EventoAtividadeBoleto $eventoAtividadeBoleto){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $eventoAtividadeBoleto->getListaInscricoes());
        curl_setopt($ch, CURLOPT_POST, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "token=". $eventoAtividadeBoleto->getToken()
        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);


        $crawler = new Crawler($result);

        /** @var Crawler $inscricoes */
        if($inscricoes = $crawler->filter('inscricao')){

            /** @var \DOMElement $inscricao */
            foreach ($inscricoes as $inscricao){
                $codInscricao = ($inscricao->getElementsByTagName('codigoinscricao')->item(0)->nodeValue);
                $codInscricao = str_replace('>','',$codInscricao);

                /** @var InscricaoBoleto $inscricaoBoleto */
                if($inscricaoBoleto = $eventoAtividadeBoleto->getBoletos()->filter(
                    function(InscricaoBoleto $entry) use ($codInscricao) {
                        return $entry->getInscricaoFuncamp() == $codInscricao;
                    }
                )->first()){
                    $flagconfirmado = $inscricao->getElementsByTagName('flagconfirmado')->item(0)->nodeValue;
                    if($flagconfirmado != 'N'){
                        $this->output->writeln('BOLETO PAGO Evento: ' . $inscricaoBoleto->getInscricao()->getEventoAtividade()->getEvento()->getNome()
                            . ' - ' . $inscricaoBoleto->getInscricao()->getEventoAtividade()->getNome()
                        . ' Inscricao: ' . $inscricaoBoleto->getInscricao()->getId()
                        . ' Participante: ' . $inscricaoBoleto->getInscricao()->getParticipante()->getNome()
                        . ' Valor: ' . $inscricaoBoleto->getValor());

                        $inscricaoBoleto->setStatus(2); // Pago
                        $inscricaoBoleto->setDataBaixa(new \DateTime());
                        $this->em->persist($inscricaoBoleto);
                    }
                }
            }
        }
        $this->em->flush();
    }

    /**
     * @param InscricaoBoleto $inscricaoBoleto
     */
    public function sendMessageAction(InscricaoBoleto $inscricaoBoleto){
        /** @var RequestContext $context */
        $context = $this->getContainer()->get('router')->getContext();

        $eventoAtividade = $inscricaoBoleto->getEventoAtividadeBoleto()->getEventoAtividade();


        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Evento] ' . $eventoAtividade->getEvento()->getNome() . ' Boleto para pagamento')

            ->setTo($inscricaoBoleto->getInscricao()->getParticipante()->getEmail())
            ->addCc($eventoAtividade->getEvento()->getEmail())
            ->setReplyTo($eventoAtividade->getEvento()->getEmail())
            ->setBody(
                $this->getContainer()->get('templating')->render('FCMBoletoFuncampBundle:Emails:boleto.html.twig',[
                    'eventoAtividade' => $eventoAtividade,
                    'inscricaoBoleto' => $inscricaoBoleto,
                ]),
                'text/html'
            );

        $this->getContainer()->get('mailer')->send($message);
    }

}
