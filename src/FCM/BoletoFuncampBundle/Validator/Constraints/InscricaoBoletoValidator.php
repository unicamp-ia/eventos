<?php

namespace FCM\BoletoFuncampBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class InscricaoBoletoValidator extends ConstraintValidator
{

    /**
     * @param \FCM\BoletoFuncampBundle\Entity\InscricaoBoleto $inscricaoBoleto
     * @param Constraint $constraint
     */
    public function validate($inscricaoBoleto, Constraint $constraint)
    {
        // Possui valor e eh isento
        if(!$inscricaoBoleto->getId() and $inscricaoBoleto->getValor() and $inscricaoBoleto->getStatus() == 9){
            $this->context->buildViolation('Defina o valor para zero quando inscrição é isenta')
                ->atPath('valor')
                ->addViolation();
        }

        // Possui valor e eh isento
        if($inscricaoBoleto->getValor() == 0 and $inscricaoBoleto->getStatus() != 9){
            $this->context->buildViolation('Defina o valor maior que zero')
                ->atPath('valor')
                ->addViolation();
        }



    }
}