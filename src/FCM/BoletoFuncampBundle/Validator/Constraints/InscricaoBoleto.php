<?php
namespace FCM\BoletoFuncampBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class InscricaoBoleto extends Constraint
{
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return InscricaoBoletoValidator::class;
    }


}