<?
namespace FCM\BoletoFuncampBundle\Service;

use Doctrine\ORM\EntityManager;
use FCM\BoletoFuncampBundle\Entity\InscricaoBoleto;
use FCM\ContaBundle\Entity\ContaLog;
use FCM\ContaBundle\Entity\Usuario;
use FCM\ContaBundle\Repository\ADRepository;
use FCM\EventoBundle\Entity\Inscricao;
use LdapTools\LdapManager;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\DomCrawler\Crawler;

class BoletoTasker
{
    /** @var  EntityManager */
    protected $entityManager;

    /** @var  TwigEngine */
    protected $twigEngine;

    /** @var  \Swift_Mailer */
    protected $mailer;

    protected $mail;

    public function __construct(EntityManager $entityManager, TwigEngine $twigEngine,
                                \Swift_Mailer $mailer, $mail)
    {
        $this->entityManager = $entityManager;
        $this->twigEngine = $twigEngine;
        $this->mailer = $mailer;
        $this->mail = $mail;
    }

    /**
     * Gera um novo boleto e o envia
     * @param Inscricao $inscricao
     * @param $valor
     * @return InscricaoBoleto
     */
    public function newBoleto(Inscricao $inscricao, $valor){
        $valor = str_replace('.',',',$valor);

        /** @var InscricaoBoleto $inscricaoBoleto */
        $inscricaoBoleto = new InscricaoBoleto();

        $inscricaoBoleto->setInscricao($inscricao);
        $inscricaoBoleto->setValor($valor);
        $inscricaoBoleto->setCreated(new \DateTime());
        $inscricaoBoleto->setEventoAtividadeBoleto($inscricao->getEventoAtividade()->getAtividadeBoleto());

        // Vencimento pode ser uma data fixa ou a partir de n dias da inscricao
        if($inscricaoBoleto->getEventoAtividadeBoleto()->getVencimento()){
            $inscricaoBoleto->setDataVencimento($inscricaoBoleto->getEventoAtividadeBoleto()->getVencimento());
        } else {
            $today = new \DateTime();
            $vencimento = $today->modify('+' . $inscricaoBoleto->getEventoAtividadeBoleto()->getDiasVencimento() . ' days');

            // Verifica se eh domingo
            if($vencimento->format('w') == 0) $vencimento->modify('+1 days');

            // Verifica se eh sabado
            if($vencimento->format('w') == 6) $vencimento->modify('+2 days');

            $inscricaoBoleto->setDataVencimento($vencimento);
        }


        if($this->requestNew($inscricaoBoleto)){
            $inscricaoBoleto->setStatus(1);
            $this->entityManager->persist($inscricaoBoleto);
            $this->entityManager->flush();
            $this->send($inscricaoBoleto);

            return $inscricaoBoleto;
        }
    }

    /**
     * Gera boleto na FUNCAMP
     * @return bool
     */
    public function requestNew(InscricaoBoleto $inscricaoBoleto){


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $inscricaoBoleto->getEventoAtividadeBoleto()->getEnvioPost());
        curl_setopt($ch, CURLOPT_POST, 5);

        if(is_numeric($inscricaoBoleto->getInscricao()->getParticipante()->getDocumento())){
            $num = str_pad($inscricaoBoleto->getInscricao()->getParticipante()->getDocumento(),11,'0',STR_PAD_LEFT);
            $tipo = '1';
        } else {
            // Fake CPF - Deal with it!
            $num = '11144477735';
            $tipo = '1';
        }

        // Sempre gera numero de pedido diferente
        $numPedido = $inscricaoBoleto->getId() . rand(0,9999);

        curl_setopt($ch, CURLOPT_POSTFIELDS,
            'identificador='. $inscricaoBoleto->getEventoAtividadeBoleto()->getIdentificador()
            . '&tipodocumento='.$tipo // CPF
            . '&numerodocumento='.$num
            . '&descricao='.$inscricaoBoleto->getEventoAtividadeBoleto()->getDescricao()
            . '&nome='.$inscricaoBoleto->getInscricao()->getParticipante()->getNome()
            . '&email=sistemas@fcm.unicamp.br'
            . '&endereco=Av. Èrico Veríssimo'
            . '&bairro=DISTRITO BARÃO GERALDO'
            . '&cidade=Campinas'
            . '&uf=SP'
            . '&pais=Brasil'
            . '&numeropedido=' . $numPedido
            . '&cep=13.083-851'
            . '&telefone=1935219552'
            . '&celular=1935219552'
            . '&tipopagamento=2'
            . '&datavencimento='. $inscricaoBoleto->getDataVencimento()->format('d m Y')
            . '&valor='. $inscricaoBoleto->getValor()
            . '&origem=N'
        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $crawler = new Crawler($result);

        if($crawler->filter('status')->first()->text() == 'SUCESSO'){
            $inscricaoBoleto->setUrlBoleto($crawler->filter('urlpagamento')->first()->text());
            $inscricaoBoleto->setLastUpdate(new \DateTime());
            $inscricaoBoleto->setInscricaoFuncamp($crawler->filter('codigoinscricao')->first()->text());
            $inscricaoBoleto->setStatus(0);
            $this->entityManager->persist($inscricaoBoleto);
            $this->entityManager->flush();
            return true;
        }


        return false;

    }

    /**
     * @param InscricaoBoleto $inscricaoBoleto
     */
    public function send(InscricaoBoleto $inscricaoBoleto){
        $eventoAtividade = $inscricaoBoleto->getEventoAtividadeBoleto()->getEventoAtividade();


        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Evento] ' . $eventoAtividade->getEvento()->getNome() . ' Boleto para pagamento')
            ->setFrom($this->mail)
            ->setTo($inscricaoBoleto->getInscricao()->getParticipante()->getEmail())
            ->addCc($eventoAtividade->getEvento()->getEmail())
            ->setReplyTo($eventoAtividade->getEvento()->getEmail())
            ->setBody(
                $this->twigEngine->render('FCMBoletoFuncampBundle:Emails:boleto.html.twig',[
                    'eventoAtividade' => $eventoAtividade,
                    'inscricaoBoleto' => $inscricaoBoleto,
                ]),
                'text/html'
            );

        $this->mailer->send($message);
    }


}