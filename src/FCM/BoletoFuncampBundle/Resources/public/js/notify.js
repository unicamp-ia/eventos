$(document).on("click", ".ajax-wait", function (e) {
    var $element = $(this);
    $($element).html('<img src="/eventos/bundles/fcmevento/images/loading.svg">');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: Routing.generate('ajax_boletofuncamp_notify', {'inscricao' : $($element).attr('inscricao')}),
        data: {
            boleto: $(this).attr('boleto'),
        },
        async: false //you won't need that if nothing in your following code is dependend of the result
    })
        .done(function(response){
            var result = '#ajax_boletofuncamp_notify'+$element.attr('boleto');
            $(result).html(response); //Change the html of the div with the id = "your_div"
        })
        .fail(function(jqXHR, textStatus, errorThrown){
            alert('Error : ' + errorThrown);
        });
});
