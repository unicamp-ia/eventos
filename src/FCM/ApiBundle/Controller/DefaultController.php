<?php

namespace FCM\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('FCMApiBundle:Default:index.html.twig');
    }
}
