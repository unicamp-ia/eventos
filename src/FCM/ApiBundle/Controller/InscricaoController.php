<?php

namespace FCM\ApiBundle\Controller;

use FCM\ApiBundle\Normalizer\InscricaoNormalizer;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\QuestionBundle\Entity\RespostaItem;
use JMS\SerializerBundle\JMSSerializerBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class InscricaoController extends Controller
{
    /**
     *
     *
     * @Rest\Post("/api/cpem/{documento}", name="api_cpem_documento")
     * @Method({"GET"})
     */
    public function cpemAction(Request $request)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();

        $inscricao = $this->getDoctrine()->getRepository('FCMEventoBundle:Inscricao')
            ->createQueryBuilder('a')
            ->join('a.participante','b', 'WITH')
            ->andWhere('a.eventoAtividade = :eventoAtividade')->setParameter('eventoAtividade',1266)
            ->andWhere('b.documento = :documento')->setParameter('documento', $request->get('documento'))
            ->getQuery()->getResult();

        if(!count($inscricao)) {
            $jsonContent = null;
        } else {
            $jsonContent = $serializer->serialize($inscricao, 'json');
        }


        $response = new JsonResponse($jsonContent);
        $response->headers->set('Content-Type', 'application/json');
        // Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     *
     * Pega as informações de um usuário de uma atividade especifica a partir de um documento
     *
     * @Rest\Post("/api/atividadeid/{atividade_id}/documento/{documento}", name="api_userinscricao_documento")
     * @Method({"GET"})
     */
    public function atividadeInscricaoAction(Request $request)
    {
        $inscricao = $this->getDoctrine()->getRepository('FCMEventoBundle:Inscricao')
            ->createQueryBuilder('a')
            ->join('a.participante','b', 'WITH')
            ->andWhere('a.eventoAtividade = :eventoAtividade')->setParameter('eventoAtividade',$request->get('atividade_id'))
            ->andWhere('b.documento = :documento')->setParameter('documento', $request->get('documento'))
            ->getQuery()->getArrayResult();

        $response = new JsonResponse($inscricao);

        return $response;
    }

    /**
     *
     * @Rest\Post("/api/inscricao/{inscricao}", name="api_inscricao_show")
     * @Method({"GET"})
     */
    public function showAction(Request $request, Inscricao $inscricao)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();

        $jsonContent = $serializer->serialize($inscricao, 'json');

        $response = new JsonResponse($jsonContent);
        $response->headers->set('Content-Type', 'application/json');
        // Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     *
     * @Rest\Post("/api/presenca/{id}", name="api_presenca_show")
     * @Method({"GET"})
     */
    public function presencaAction(Request $request, RespostaItem $respostaItem)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();

        if($respostaItem->isAppCheck()){
            $respostaItem->setAppCheck(false);
        } else $respostaItem->setAppCheck(true);

        $this->getDoctrine()->getManager()->persist($respostaItem);
        $this->getDoctrine()->getManager()->flush();

        $jsonContent = $serializer->serialize($respostaItem->getResposta()->getInscricao(), 'json');

        $response = new JsonResponse($jsonContent);
        $response->headers->set('Content-Type', 'application/json');
        // Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("ajax/api/presenca/{id}", name="ajax_api_presenca_show",options={"expose"=true})
     */
    public function ajaxPresencaAction(Request $request, RespostaItem $respostaItem)
    {


        if($respostaItem->isAppCheck()){
            $respostaItem->setAppCheck(false);
        } else $respostaItem->setAppCheck(true);

        $this->getDoctrine()->getManager()->persist($respostaItem);
        $this->getDoctrine()->getManager()->flush();

        $template = $this->render('FCMEventoBundle:Inscricao:ajax.presenca.html.twig', array(
            'respostaItem' => $respostaItem,
        ));

        return new JsonResponse($template->getContent());
    }
}
