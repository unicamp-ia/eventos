<?php

namespace FCM\ApiBundle\Controller;

use FCM\ApiBundle\Normalizer\InscricaoNormalizer;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\QuestionBundle\Entity\RespostaItem;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\SerializerBundle\JMSSerializerBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class EventoController extends FOSRestController
{
    /**
     *
     *
     * @Rest\Get("/api/evento/open", name="api_evento_open")
     * @Method({"GET"})
     */
    public function openAction(Request $request)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();

        $eventos = $this->getDoctrine()->getRepository('FCMEventoBundle:Evento')->byEventoEmInscricao();
        $eventosArr = array();

        /** @var Evento $evento */
        foreach ($eventos as $evento){
            $eventosArr[$evento->getId()] = $this->prepareEvento($evento);
        }

        $jsonContent = $serializer->serialize($eventosArr, 'json');
        $response = new JsonResponse($jsonContent);

        return $response;
    }

    /**
     * @param $evento
     * @return mixed
     */
    protected function prepareEvento($evento){
        $eventoArr['id'] = $evento->getId();
        $eventoArr['nome'] = $evento->getNome();

        /** @var EventoAtividade $eventoAtividade */
        foreach ($evento->getEventoAtividades() as $eventoAtividade){
            if(!isset($eventosArr[$evento->getId()]['inicio']))
                $eventoArr['inicio'] = $eventoAtividade->getDataIniAtividade();

            if(!isset($evetosArr[$evento->getId()]['termino']))
                $eventoArr['termino'] = $eventoAtividade->getDataFimAtividade();
        }

        return $eventoArr;
    }

    /**
     *
     *
     * @Rest\Get("/api/evento/{id}", name="api_evento_show")
     * @Method({"GET"})
     */
    public function showAction(Request $request, Evento $evento)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();

        $eventosArr = $this->prepareEvento($evento);


        $jsonContent = $serializer->serialize($eventosArr, 'json');
        $response = new JsonResponse($jsonContent);

        return $response;
    }
}
