<?php

namespace FCM\ApiBundle\Controller;

use FCM\UserBundle\Entity\User;
use FCM\UserBundle\Entity\Usuario;
use FCM\UserBundle\Security\TokenAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

class UserController extends Controller
{
    /**
     *
     *
     * @Rest\Post("/api/user/login", name="api_user_login")
     * @Method({"GET","POST"})
     */
    public function indexAction(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');

        /** @var User $user */
        $user = $this->get('doctrine')->getRepository('FCMUserBundle:User')
            ->findOneBy(['username'=> $username]);


        $auth = new TokenAuthenticator($this->get('doctrine')->getManager(),$this->get('doctrine')->getManager('perm'),null);

        if($user instanceof User){
            try {
                if($auth->checkCredentials(['username' => $username, 'password' => $password], $user)){
                    $resp['userData'] = [
                        'username' => $user->getUsername(),
                        'name' => $user->getUsuario()->getNome(),
                    ];
                }
            } catch(\Exception $e){
                $resp['error'] = 'Verifique usuário e senha';
            }

        } else {
            $resp['error'] = 'Usuário ' . $username . ' não encontrado.';
        }



        $response = new JsonResponse($resp);
        $response->headers->set('Content-Type', 'application/json');
        // Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
}
