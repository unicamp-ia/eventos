<?
namespace FCM\ApiBundle\Service;

use Doctrine\ORM\EntityManager;
use FCM\UserBundle\Entity\User;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class Serializer
{
    /** @var EntityManager */
    protected $em;

    /** @var \Symfony\Component\Serializer\Serializer  */
    protected $serializer;

    /** @var ObjectNormalizer  */
    protected $normalizer;

    public function __construct(EntityManager $entityManager
        , \Symfony\Component\Serializer\Serializer $serializer, ObjectNormalizer $normalizer)
    {
        $this->em = $entityManager;
        $this->normalizer = $normalizer;
        $this->serializer = $serializer;
    }

    public function serialize($object){



        $this->normalizer = new ObjectNormalizer();
        $this->normalizer->setCircularReferenceLimit(0);
        $this->normalizer->setIgnoredAttributes(['file','upload','thumbnail','File','template',
            'assinatura','Evento','EventoAtividade','User']);

        // Add Circular reference handler
        $this->normalizer->setCircularReferenceHandler(function ($object) {
            return null;

        });

        $normalizers = [$this->normalizer];

        $this->serializer->
        $serializer = new Serializer($normalizers,$this->serializer);

        $inscricaoS = $serializer->serialize($object,'json');
    }
}