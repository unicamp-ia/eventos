<?php
namespace FCM\ApiBundle\Normalizer;

use FCM\EventoBundle\Entity\Inscricao;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * User normalizer
 */
class InscricaoNormalizer implements NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var Inscricao $inscricao */
        $inscricao = $object;
        return [
            'id' => $inscricao->getId(),
            ''

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Inscricao;
    }
}