<?php

namespace FCM\PermissaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permtppermissao
 *
 * @ORM\Table(name="permTpPermissao", indexes={@ORM\Index(name="IDX_B4BA6F5F817BC093", columns={"idSistema"})})
 * @ORM\Entity
 */
class PermTpPermissao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="nivel", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $nivel;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=250, nullable=true)
     */
    private $descricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSistema", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idsistema;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=true)
     */
    private $status;



    /**
     * Set nivel
     *
     * @param integer $nivel
     *
     * @return PermTpPermissao
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return integer
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return PermTpPermissao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set idsistema
     *
     * @param integer $idsistema
     *
     * @return PermTpPermissao
     */
    public function setIdsistema($idsistema)
    {
        $this->idsistema = $idsistema;

        return $this;
    }

    /**
     * Get idsistema
     *
     * @return integer
     */
    public function getIdsistema()
    {
        return $this->idsistema;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return PermTpPermissao
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
