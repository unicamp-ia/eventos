<?php

namespace FCM\PermissaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permlog
 *
 * @ORM\Table(name="permLog")
 * @ORM\Entity
 */
class PermLog
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datahora", type="datetime", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $datahora;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSistema", type="integer", nullable=false)
     */
    private $idsistema;

    /**
     * @var integer
     *
     * @ORM\Column(name="nivel", type="integer", nullable=false)
     */
    private $nivel;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=20, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="userCed", type="string", length=20, nullable=true)
     */
    private $userced;

    /**
     * @var string
     *
     * @ORM\Column(name="acao", type="string", length=50, nullable=true)
     */
    private $acao;



    /**
     * Get datahora
     *
     * @return \DateTime
     */
    public function getDatahora()
    {
        return $this->datahora;
    }

    /**
     * Set idsistema
     *
     * @param integer $idsistema
     *
     * @return PermLog
     */
    public function setIdsistema($idsistema)
    {
        $this->idsistema = $idsistema;

        return $this;
    }

    /**
     * Get idsistema
     *
     * @return integer
     */
    public function getIdsistema()
    {
        return $this->idsistema;
    }

    /**
     * Set nivel
     *
     * @param integer $nivel
     *
     * @return PermLog
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return integer
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return PermLog
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set userced
     *
     * @param string $userced
     *
     * @return PermLog
     */
    public function setUserced($userced)
    {
        $this->userced = $userced;

        return $this;
    }

    /**
     * Get userced
     *
     * @return string
     */
    public function getUserced()
    {
        return $this->userced;
    }

    /**
     * Set acao
     *
     * @param string $acao
     *
     * @return PermLog
     */
    public function setAcao($acao)
    {
        $this->acao = $acao;

        return $this;
    }

    /**
     * Get acao
     *
     * @return string
     */
    public function getAcao()
    {
        return $this->acao;
    }
}
