<?php

namespace FCM\PermissaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permsistema
 *
 * @ORM\Table(name="permSistema")
 * @ORM\Entity
 */
class PermSistema
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=250, nullable=true)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="localSolic", type="string", length=23, nullable=true)
     */
    private $localsolic;

    /**
     * @var string
     *
     * @ORM\Column(name="responsavel", type="string", length=20, nullable=true)
     */
    private $responsavel;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=false)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return PermSistema
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return PermSistema
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set localsolic
     *
     * @param string $localsolic
     *
     * @return PermSistema
     */
    public function setLocalsolic($localsolic)
    {
        $this->localsolic = $localsolic;

        return $this;
    }

    /**
     * Get localsolic
     *
     * @return string
     */
    public function getLocalsolic()
    {
        return $this->localsolic;
    }

    /**
     * Set responsavel
     *
     * @param string $responsavel
     *
     * @return PermSistema
     */
    public function setResponsavel($responsavel)
    {
        $this->responsavel = $responsavel;

        return $this;
    }

    /**
     * Get responsavel
     *
     * @return string
     */
    public function getResponsavel()
    {
        return $this->responsavel;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return PermSistema
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
