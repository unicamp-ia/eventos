<?php

namespace FCM\PermissaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Permpermissao
 *
 * @ORM\Table(name="permPermissao")
 * @ORM\Entity
 */
class PermPermissao implements RoleInterface
{

    public function getRole(){
        return 'ROLE_'.$this->getIdsistema().'_'.$this->getNivel();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=20, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $username;

    /**
     * @var integer
     *
     * @ORM\Column(name="nivel", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $nivel;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSistema", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idsistema;

    /**
     * @var string
     *
     * @ORM\Column(name="userCed", type="string", length=20, nullable=true)
     */
    private $userced;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtFinal", type="date", nullable=true)
     */
    private $dtfinal;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=true)
     */
    private $status;



    /**
     * Set username
     *
     * @param string $username
     *
     * @return PermPermissao
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set nivel
     *
     * @param integer $nivel
     *
     * @return PermPermissao
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return integer
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set idsistema
     *
     * @param integer $idsistema
     *
     * @return PermPermissao
     */
    public function setIdsistema($idsistema)
    {
        $this->idsistema = $idsistema;

        return $this;
    }

    /**
     * Get idsistema
     *
     * @return integer
     */
    public function getIdsistema()
    {
        return $this->idsistema;
    }

    /**
     * Set userced
     *
     * @param string $userced
     *
     * @return PermPermissao
     */
    public function setUserced($userced)
    {
        $this->userced = $userced;

        return $this;
    }

    /**
     * Get userced
     *
     * @return string
     */
    public function getUserced()
    {
        return $this->userced;
    }

    /**
     * Set dtfinal
     *
     * @param \DateTime $dtfinal
     *
     * @return PermPermissao
     */
    public function setDtfinal($dtfinal)
    {
        $this->dtfinal = $dtfinal;

        return $this;
    }

    /**
     * Get dtfinal
     *
     * @return \DateTime
     */
    public function getDtfinal()
    {
        return $this->dtfinal;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return PermPermissao
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
