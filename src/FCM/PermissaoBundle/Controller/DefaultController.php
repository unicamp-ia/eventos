<?php

namespace FCM\PermissaoBundle\Controller;

use FCM\PermissaoBundle\Entity\PermSistema;
use FCM\PermissaoBundle\Entity\PermTpPermissao;
use FCM\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/permissao")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager('perm');

        /** @var User $user */
        $user = $usr= $this->get('security.context')->getToken()->getUser();

        return $this->render('FCMPermissaoBundle:Default:index.html.twig', [
            'page_title' => 'permissao',
        ]);
    }


}
