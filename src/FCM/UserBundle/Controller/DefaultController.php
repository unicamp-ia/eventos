<?php

namespace FCM\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {

        return $this->render('FCMUserBundle:Default:index.html.twig', array(
            'page_title' => 'Intranet FCM'
        ));
    }

    /**
     * @Route("/user/logout")
     */
    public function logoutAction()
    {

        $this->get('request')->getSession()->invalidate();

        unset($_COOKIE['SI_USER_TOKEN']);

        return $this->redirect('/di/user/logout');
    }
}
