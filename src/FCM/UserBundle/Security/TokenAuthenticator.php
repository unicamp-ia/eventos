<?php

namespace FCM\UserBundle\Security;

use Doctrine\Common\Collections\ArrayCollection;
use FCM\EventoBundle\Entity\Participante;
use FCM\EventoBundle\Repository\ParticipanteRepository;
use FCM\EventoBundle\Service\IntranetTasker;
use FCM\UserBundle\Entity\User;
use FCM\UserBundle\Repository\UserRepository;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Ldap\LdapClient;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Security;
use Twig\Template;


class TokenAuthenticator extends AbstractGuardAuthenticator
{
    /** @var EntityManager */
    private $em;

    /** @var EntityManager */
    private $em_perm;

    private $router;

    /** @var Template */
    private $template;

    private $intranetTasker;

    public function __construct(EntityManager $em, $router, IntranetTasker $intranetTasker)
    {
        $this->em = $em;
        $this->em_perm = $em;
        $this->router = $router;
        $this->intranetTasker = $intranetTasker;
    }

    /**
     * Called on every request. Return whatever credentials you want,
     * or null to stop authentication.
     */
    public function getCredentials(Request $request)
    {

        if(!empty($request->query->get('token'))){
            return ['token' => $request->query->get('token'), '_switch_user' => false];
        }

        // Masquerade
        if(!empty($request->query->get('_switch_user'))){
            return ['username' => $request->query->get('_switch_user'), '_switch_user' => true];
        }

        if ($request->getPathInfo() == '/participante/login' && null !== $request->get('pass')) {
            if(is_numeric($request->get('documento')))
                $documento = str_pad($request->get('documento'),11,"0",STR_PAD_LEFT);
            else
                $documento = $request->get('documento');

            return [
                'documento' => $documento,
                'pass' => $request->get('pass'),
                ];
        }

        if ($request->getPathInfo() == '/auth/admin'){
            return [
                'username' => $request->request->get('_username'),
                'password' => $request->request->get('_password'),
            ];
        }

        if ($request->getPathInfo() == '/auth/participante' && null !== $request->get('pass')) {
            return [
                'documento' => $request->get('documento'),
                'pass' => $request->get('pass'),
            ];
        }
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return User
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {

        if(isset($credentials['token'])){
            $user = $this->em->getRepository('FCMUserBundle:User')->findOneBy(['token' => $credentials['token']]);
        }

        if(isset($credentials['_switch_user']) and isset($credentials['username'])){
            $user = $this->em->getRepository('FCMUserBundle:User')->findOneBy(['username' => $credentials['username']]);
            $user->setPermissoes(null, true);
        }

        // Participante
        if(isset($credentials['documento'])){
            return $this->getParticipanteUser($credentials['documento'], $credentials['pass']);
            $user->setPermissoes();
        }


        if(isset($credentials['username']) and isset($credentials['password'])){
            /** @var User $user */
            $user = $this->getAdminUser($credentials['username'], $credentials['password']);

            if($user) $user->setPermissoes();


        }

        if(!$user) return null;

        return $user;
    }

    /**
     * @param $username
     * @param $pass
     * @return User
     */
    protected function getAdminUser($username, $password){

        if($resp = $this->intranetTasker->checkPassowrd($username, $password)){
            // Verifica se existe em Eventos
            /** @var User $user */
            $user = $this->em->getRepository('FCMUserBundle:User')->findOneBy(['username' => $username]);
            $userData = $this->intranetTasker->getUser($username);



            if(!$user instanceof User) {
                /** @var User $user */
                $user = new User();
                $user->setUsername($username);
                $user->setVinculo(User::VINCULO_COLABORADOR);
            }

            $user->setNome($userData->nome);
            $user->setEmail($userData->email);
            $this->em->persist($user);
            $this->em->flush();

            $permissoes = $userData->permissoes;

            foreach ($permissoes as $permissoe){

                // condicao para admin do sistema
                if($permissoe->tipoPermissao->nivel == 0 and $permissoe->tipoPermissao->sistema->id == 17){
                    $user->permissoes['ROLE_17_0'] = 'ROLE_17_0';
                }

                if($permissoe->tipoPermissao->sistema->id == 17){
                    $user->permissoes['ROLE_17_1'] = 'ROLE_17_1';
                }
            }



            // caso nao possua permissao nao autentique
            if (count($permissoes) == 0) {
                throw new CustomUserMessageAuthenticationException('Usuário sem permissão, procure a informática');
                return false;
            }

        } else {
            return false;
        }

        return $user;
    }

    /**
     * @param $documento
     * @param $pass
     * @return User
     */
    protected function getParticipanteUser($documento, $pass){

        $participante = $this->em->getRepository('FCMEventoBundle:Participante')->findOneBy(['documento' => $documento]);

        // tenta com zero a esquerda
        if(!$participante instanceof Participante){
            $participante = $this->em->getRepository('FCMEventoBundle:Participante')->findOneBy(['documento' => '0'.$documento]);
        }

        // tenta com zero a esquerda
        if(!$participante instanceof Participante){
            $participante = $this->em->getRepository('FCMEventoBundle:Participante')->findOneBy(['email' => $documento]);
        }

        if(!$participante instanceof Participante){
            throw new CustomUserMessageAuthenticationException('Participante com documento ou e-mail não encontrado');
        }

        $participante->getUser()->setPermissoes();
        return $participante->getUser();

    }

    /**
     * Realiza bind com a senha do usuario
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if(isset($credentials['_switch_user'])) return true;

        if(isset($credentials['documento'])){
            if(!($user->getParticipante()->checkPass($credentials['pass'])))
                throw new CustomUserMessageAuthenticationException('Participante com senha incorreta');
            else
                return true;
        }

        if($this->checkCredentialsAD($credentials['username'], $credentials['password'])){

            return true;
        }



    }

    /**
     * Verifica senha para usuarios FCM
     * @param $credentials
     * @param UserInterface $user
     * @return bool
     */
    protected function checkCredentialsAD($username, $pass){


        $result = $this->intranetTasker->checkPassowrd($username,$pass);

        if(!$result) {
            throw new CustomUserMessageAuthenticationException('Usuário ou senha incorreta');
            return false;
        }


        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {


        /** @var User $user */
        $user = $token->getUser();

        iF($request->get('_target_path')){
            return new RedirectResponse($request->get('_target_path'));
        }

        $url = $this->router->generate('home');

        if(in_array('ROLE_17_1',$user->getPermissoes())) $url = $this->router->generate('admin');

        if(in_array('ROLE_AVALIADOR',$user->getPermissoes())) $url = $this->router->generate('admin');

        return new RedirectResponse($url);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

        $request->getSession()->getFlashBag()->add('error', $exception->getMessageKey());

        if($request->get('_origin')){
            return new RedirectResponse($request->get('_origin'));
        }

    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {

        $request->getSession()->getFlashBag()->add('info', 'Área restrita, realize o login');
        $url = $this->router->generate('participante_login',['destination' => $request->getRequestUri()]);
        return new RedirectResponse($url);


    }

    public function supportsRememberMe()
    {
        return true;
    }
}