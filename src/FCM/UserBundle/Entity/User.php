<?php

namespace FCM\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 * Na tabela user ficarao somente os usuarios que nao participantes do sistema
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="FCM\UserBundle\Repository\UserRepository")
 * @UniqueEntity(
 *     fields = "email",
 *     errorPath="email",
 *     message="E-mail já cadastrado"
 * )
 */
class User implements UserInterface
{
    public function __construct()
    {
        //$this->permissoes = new ArrayCollection();
        $this->avaliacoes = new ArrayCollection();
        $this->respostas = new ArrayCollection();
    }

    const VINCULO_COLABORADOR = 'Colaborador';
    const VINCULO_PARTICIPANTE = 'Eventos';

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="\FCM\EventoBundle\Entity\Avaliacao", mappedBy="avaliador")
     */
    private $avaliacoes;

    /**
     * @ORM\OneToMany(targetEntity="\FCM\QuestionBundle\Entity\Resposta", mappedBy="createdBy", cascade={"persist","remove"})
     */
    private $respostas;

    /**
     * @ORM\OneToMany(targetEntity="\FCM\EventoBundle\Entity\Evento", mappedBy="createdBy")
     */
    private $eventos;

    /**
     * @ORM\OneToMany(targetEntity="\FCM\EventoBundle\Entity\EventoAdmin", mappedBy="createdBy")
     */
    private $eventoAdminCreatedBys;

    /**
     * @ORM\OneToMany(targetEntity="\FCM\EventoBundle\Entity\EventoAdmin", mappedBy="user")
     */
    private $eventoAdminUsers;

    /**
     * @ORM\OneToOne(targetEntity="\FCM\EventoBundle\Entity\Participante", mappedBy="user")
     */
    private $participante;

    /** @var  ArrayCollection */
    public $permissoes;


    /**
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(name="nome", type="string", nullable=true)
     */
    protected $nome;

    /**
     * @ORM\Column(name="token", type="string", nullable=true)
     */
    protected $token;

    /**
     * @ORM\Column(name="vinculo", type="string", nullable=true)
     */
    protected $vinculo;

    protected $password;


    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getVinculo()
    {
        return $this->vinculo;
    }

    /**
     * @param mixed $vinculo
     */
    public function setVinculo($vinculo)
    {
        $this->vinculo = $vinculo;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken()
    {
        $this->token = md5(uniqid());
    }


    /**
     * @ORM\Column(name="username", type="string", unique=true)
     */
    private $username;


    public function getUsername()
    {
        return $this->username;
    }

    public function getRoles()
    {
        return $this->permissoes;
    }


    public function getSalt()
    {

    }
    public function eraseCredentials()
    {

    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
        return $this;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }


    /**
     * @return ArrayCollection
     */
    public function getPermissoes(){
        return $this->permissoes;
    }

    /**
     * @param ArrayCollection $permissoes
     */
    public function setPermissoes($switched = false){
        $this->permissoes['ROLE_USER'] = 'ROLE_USER';

        if($this->getVinculo() == 'Avaliador') $this->permissoes['ROLE_AVALIADOR'] = 'ROLE_AVALIADOR';

        if($switched) {
            $this->permissoes['ROLE_PREVIOUS_ADMIN'] = 'ROLE_PREVIOUS_ADMIN';
        }

        if($this->getVinculo() == 'Eventos') {
            $this->permissoes['ROLE_PARTICIPANTE'] = 'ROLE_PARTICIPANTE';
        }

/*        if($this->getVinculo() == User::VINCULO_COLABORADOR){
            $this->permissoes['ROLE_17_1'] = 'ROLE_17_1';
        }*/

    }

    /**
     * Set participante
     *
     * @param \FCM\EventoBundle\Entity\Participante $participante
     *
     * @return User
     */
    public function setParticipante(\FCM\EventoBundle\Entity\Participante $participante = null)
    {
        $this->participante = $participante;

        return $this;
    }

    /**
     * Get participante
     *
     * @return \FCM\EventoBundle\Entity\Participante
     */
    public function getParticipante()
    {
        return $this->participante;
    }



    /**
     * Add avaliaco
     *
     * @param \FCM\EventoBundle\Entity\Avaliacao $avaliaco
     *
     * @return User
     */
    public function addAvaliaco(\FCM\EventoBundle\Entity\Avaliacao $avaliaco)
    {
        $this->avaliacoes[] = $avaliaco;

        return $this;
    }

    /**
     * Remove avaliaco
     *
     * @param \FCM\EventoBundle\Entity\Avaliacao $avaliaco
     */
    public function removeAvaliaco(\FCM\EventoBundle\Entity\Avaliacao $avaliaco)
    {
        $this->avaliacoes->removeElement($avaliaco);
    }

    /**
     * Get avaliacoes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAvaliacoes()
    {
        return $this->avaliacoes;
    }

    /**
     * Add evento
     *
     * @param \FCM\EventoBundle\Entity\Evento $evento
     *
     * @return User
     */
    public function addEvento(\FCM\EventoBundle\Entity\Evento $evento)
    {
        $this->eventos[] = $evento;

        return $this;
    }

    /**
     * Remove evento
     *
     * @param \FCM\EventoBundle\Entity\Evento $evento
     */
    public function removeEvento(\FCM\EventoBundle\Entity\Evento $evento)
    {
        $this->eventos->removeElement($evento);
    }

    /**
     * Get eventos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventos()
    {
        return $this->eventos;
    }

    /**
     * Add eventoAdminCreatedBy
     *
     * @param \FCM\EventoBundle\Entity\EventoAdmin $eventoAdminCreatedBy
     *
     * @return User
     */
    public function addEventoAdminCreatedBy(\FCM\EventoBundle\Entity\EventoAdmin $eventoAdminCreatedBy)
    {
        $this->eventoAdminCreatedBys[] = $eventoAdminCreatedBy;

        return $this;
    }

    /**
     * Remove eventoAdminCreatedBy
     *
     * @param \FCM\EventoBundle\Entity\EventoAdmin $eventoAdminCreatedBy
     */
    public function removeEventoAdminCreatedBy(\FCM\EventoBundle\Entity\EventoAdmin $eventoAdminCreatedBy)
    {
        $this->eventoAdminCreatedBys->removeElement($eventoAdminCreatedBy);
    }

    /**
     * Get eventoAdminCreatedBys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventoAdminCreatedBys()
    {
        return $this->eventoAdminCreatedBys;
    }

    /**
     * Add eventoAdminUser
     *
     * @param \FCM\EventoBundle\Entity\EventoAdmin $eventoAdminUser
     *
     * @return User
     */
    public function addEventoAdminUser(\FCM\EventoBundle\Entity\EventoAdmin $eventoAdminUser)
    {
        $this->eventoAdminUsers[] = $eventoAdminUser;

        return $this;
    }

    /**
     * Remove eventoAdminUser
     *
     * @param \FCM\EventoBundle\Entity\EventoAdmin $eventoAdminUser
     */
    public function removeEventoAdminUser(\FCM\EventoBundle\Entity\EventoAdmin $eventoAdminUser)
    {
        $this->eventoAdminUsers->removeElement($eventoAdminUser);
    }

    /**
     * Get eventoAdminUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventoAdminUsers()
    {
        return $this->eventoAdminUsers;
    }

    /**
     * Add resposta
     *
     * @param \FCM\QuestionBundle\Entity\Resposta $resposta
     *
     * @return User
     */
    public function addResposta(\FCM\QuestionBundle\Entity\Resposta $resposta)
    {
        $this->respostas[] = $resposta;

        return $this;
    }

    /**
     * Remove resposta
     *
     * @param \FCM\QuestionBundle\Entity\Resposta $resposta
     */
    public function removeResposta(\FCM\QuestionBundle\Entity\Resposta $resposta)
    {
        $this->respostas->removeElement($resposta);
    }

    /**
     * Get respostas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRespostas()
    {
        return $this->respostas;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}
