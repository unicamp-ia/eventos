<?php

namespace FCM\UserBundle\Entity;

class AD {

    /**
     * @var
     */
    public $address = '143.106.46.4';

    /**
     * @var int
     */
    public $port = 389;

    /**
     * @var
     */
    public $connection;

    /**
     * @var
     */
    public $userbind = 'CN=Zabbix,OU=INFOR,DC=fcm,DC=unicamp,DC=br';

    /**
     * @var
     */
    public $passbind = 'zbx2014FCM';

    function __construct(){
        $this->connect();

    }

    /**
     * Connect Method
     */
    function connect()
    {
        if (!$this->connection) {
            if (!$con = ldap_connect($this->address)) {

            }
            $this->connection = $con;
            ldap_bind($this->connection, $this->userbind, $this->passbind);
            ldap_set_option($con, LDAP_OPT_REFERRALS, 0);
            ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);
        }
    }


    public static function searchUser($user){
        $conn = new AD();
        $basedn = 'DC=fcm,DC=unicamp,DC=br';
        $filter = '(&(objectCategory=CN=Person,CN=Schema,CN=Configuration,DC=fcm,DC=unicamp,DC=br)(sAMAccountName=' . $user . '))';

        ldap_bind($conn->connection, $conn->userbind, $conn->passbind);
        $search = ldap_search($conn->connection, $basedn, $filter);
        $result = ldap_get_entries($conn->connection, $search);

        if ($result['count'] == 0) return false;

        return $result;
    }

    /**
     * Search on AD based on Dn
     * @param $dn
     * @return array|bool
     */
    public static function searchByDn($dn){
        return AD::search('(distinguishedName='.$dn.')');
    }

    /**
     * Search AD entry
     * @param $filter
     * @return array|bool
     */
    public static function search($filter){
        $conn = new AD();
        $basedn = 'DC=fcm,DC=unicamp,DC=br';

        ldap_bind($conn->connection, $conn->userbind, $conn->passbind);

        $search = ldap_search($conn->connection, $basedn, $filter);

        if($search) {
            $result = ldap_get_entries($conn->connection, $search);
            if ($result['count'] == 0) return false;
            unset($result['count']);
            return $result;
        }

        return false;
    }


    /**
     * Crate a LDAP entry
     * @param $attributes
     * @param null $dn
     * @return bool
     */
    public function createLdapEntry($attributes, $dn = NULL) {

        if (isset($attributes['dn'])) {
            $dn = $attributes['dn'];
            unset($attributes['dn']);
        }
        elseif (!$dn) {
            return FALSE;
        }

        $result = ldap_add($this->connection, $dn, $attributes);
        return $result;

        /* @todo Create ldap error exception */
        /*
        if (!$result) {
            $error = "LDAP Server ldap_add(%dn) Error Server ID = %sid, LDAP Err No: %ldap_errno LDAP Err Message: %ldap_err2str ";
            $tokens = array('%dn' => $dn, '%sid' => $this->sid, '%ldap_errno' => ldap_errno($this->connection), '%ldap_err2str' => ldap_err2str(ldap_errno($this->connection)));
            debug(t($error, $tokens));
            watchdog('ldap_server', $error, $tokens, WATCHDOG_ERROR);
        }
        */
    }

    /**
     * Delete a LDAP entry
     * @param $dn
     * @return bool
     */
    public function deleteEntry($dn){
        return @ldap_delete($this->connection, $dn);
    }

    /**
     * Define user to a group
     * @param Usuario $usuario
     * @param UO $uo
     */
    public function addUserToGroup(Usuario $usuario, UO $uo){
        $group_dn = base64_encode($uo->getDn());
        $cmd = "http://araca/funcoes.php?addUsertoGroup=TRUE&username=".$usuario->getUsername()."&groupdn=".$group_dn;
        file_get_contents($cmd);
    }


    /**
     * Define user's expiration date
     * @param Usuario $usuario
     * @param \DateTime $expiration
     */
    public function setExperation(Usuario $usuario, \DateTime $expiration){
        $datetime = base64_encode($expiration->format('d M Y 00:00:00'));
        $cmd = "http://araca/funcoes.php?setUserExpirateDate=TRUE&username=".$usuario->getUsername()."&datetime=".$datetime;
        file_get_contents($cmd);
    }

    /**
     * Utiliza araca para definir senha para usuario
     * @param Usuario $usuario
     * @param $pass
     */
    public function definePass(Usuario $usuario, $pass){
        $pass = base64_encode($pass);
        file_get_contents("http://araca/funcoes.php?changePass=".$usuario->getUsername()."&newpass=".$pass);
    }

    public function enableUser(Usuario $usuario){
        $attr['useraccountcontrol'][0] = 512;

        return $this->modifyLdapEntry($usuario->getDn(), $attr);
    }

    public function modifyLdapEntry($dn, $attributes = array()) {
        $result = ldap_read($this->connection, $dn, 'objectClass=*');

        if (!$result) {
            return FALSE;
        }

        $entries = ldap_get_entries($this->connection, $result);
        if (is_array($entries) && $entries['count'] == 1) {
            $old_attributes =  $entries[0];
        }

        $attributes = $this->removeUnchangedAttributes($attributes, $old_attributes);

        foreach ($attributes as $key => $cur_val) {
            $old_value = FALSE;
            $key_lcase = strtolower($key);
            if (isset($old_attributes[$key_lcase])) {
                if ($old_attributes[$key_lcase]['count'] == 1) {
                    $old_value = $old_attributes[$key_lcase][0];
                }
                else {
                    unset($old_attributes[$key_lcase]['count']);
                    $old_value = $old_attributes[$key_lcase];
                }
            }

            if ($cur_val == '' && $old_value != '') { // remove enpty attributes
                unset($attributes[$key]);
                $result = ldap_mod_del($this->connection, $dn, array($key_lcase => $old_value));
                if (!$result) {
                    return FALSE;
                }
            }
            elseif (is_array($cur_val)) {
                foreach ($cur_val as $mv_key => $mv_cur_val) {
                    if ($mv_cur_val == '') {
                        unset($attributes[$key][$mv_key]); // remove empty values in multivalues attributes
                    }
                    else {
                        $attributes[$key][$mv_key] = $mv_cur_val;
                    }
                }
            }
        }

        if (count($attributes) > 0) {
            $result = ldap_modify($this->connection, $dn, $attributes);
            if (!$result) return FALSE;
        }

        return TRUE;

    }

    protected function removeUnchangedAttributes($new_entry, $old_entry) {

        foreach ($new_entry as $key => $new_val) {
            $old_value = FALSE;
            $key_lcase = strtolower($key);
            if (isset($old_entry[$key_lcase])) {
                if ($old_entry[$key_lcase]['count'] == 1) {
                    $old_value = $old_entry[$key_lcase][0];
                    $old_value_is_scalar = TRUE;
                }
                else {
                    unset($old_entry[$key_lcase]['count']);
                    $old_value = $old_entry[$key_lcase];
                    $old_value_is_scalar = FALSE;
                }
            }

            // identical multivalued attributes
            if (is_array($new_val) && is_array($old_value) && count(array_diff($new_val, $old_value)) == 0) {
                unset($new_entry[$key]);
            }
            elseif (isset($old_value_is_scalar) && $old_value_is_scalar && !is_array($new_val) && strtolower($old_value) == strtolower($new_val)) {
                unset($new_entry[$key]); // don't change values that aren't changing to avoid false permission constraints
            }
        }
        return $new_entry;
    }

    /**
     * @param $LDAPdate
     * @return DateTime
     */
    public static function convertLDAPtoLinux($LDAPdate){
        // Caso esteja indefinido, supor 2099
        $LDAPdate == '9223372036854775807' ? $LDAPdate = '157443336000000000': true;
        $winInterval = round($LDAPdate / 10000000);
        // substract seconds from 1601-01-01 -> 1970-01-01
        $unixTimestamp = ($winInterval - 11644473600);
        // show date/time in local time zone
        $date = new \DateTime();
        $date->setTimestamp($unixTimestamp);
        return $date;
    }

    /**
     * Convert um timestamp linux para formato ldap
     * @param $unix_ts
     * @return int
     */
    public static function convertUnixtoLDAP($unix_ts) {
        $AD2Unix = ( (1970-1601) * 365 -3 + round((1970-1601)/4) ) * 86400;

        $secsAfterADepoch = intval( $AD2Unix + $unix_ts );
        $ldap_ts = $secsAfterADepoch * 10000000;

        return $ldap_ts;
    }

    /**
     * Define senha do usuario
     * @param Usuario $usuario
     * @param $pass
     */
    public static function setPassword(Usuario $usuario, $pass){
        $pass = base64_encode($pass);
        //Para criar a senha e chamado script do araca
        file_get_contents("http://araca/funcoes.php?changePass=".
            $usuario->getUsername()."&newpass=".$pass);
    }
}