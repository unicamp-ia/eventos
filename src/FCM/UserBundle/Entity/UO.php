<?

namespace FCM\UserBundle\Entity;

class UO {
    protected $nome;
    protected $descricao;
    protected $cod_area;
    protected $dn;

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getCodArea()
    {
        return $this->cod_area;
    }

    /**
     * @param mixed $cod_area
     */
    public function setCodArea($cod_area)
    {
        $this->cod_area = $cod_area;
    }

    /**
     * @return mixed
     */
    public function getDn()
    {
        return $this->dn;
    }

    /**
     * @param mixed $dn
     */
    public function setDn($dn)
    {
        $this->dn = $dn;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * Load UO
     * @param $dn
     * @return UO|null
     */
    public static function search($dn){
        $results = AD::searchByDn($dn);
        if(isset($results['0']['name'][0])){

            /** @var UO $uo */
            $uo = new UO;
            $uo->setDescricao($results['0']['description'][0]);
            $uo->setNome($results['0']['name'][0]);
            if(isset($results['0']['postalcode'][0])) $uo->setCodArea($results['0']['postalcode'][0]);
            $uo->setDn($results['0']['dn'][0]);
            return $uo;

        } else {
            return null;
        }
    }


}