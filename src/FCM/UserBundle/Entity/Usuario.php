<?php

namespace FCM\UserBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class Usuario {

    protected $nome;

    protected $primeiro_nome;

    protected $segundo_nome;

    protected $documento;

    protected $matricula;

    /**
     */
    protected $vinculo;

    /**
     */
    protected $ramal;

    /**
     * @Assert\Email
     */
    protected $email;

    /**
     * @Assert\Email
     */
    protected $email_alt;
    protected $alterado_por;
    protected $responsavel;
    protected $observacao;
    protected $permissao;

    protected $cod_area;
    protected $cod_local;

    /**
     */
    protected $celular;

    protected $cod_curso;
    protected $status;
    protected $validade;
    protected $cota;
    protected $criado_em;
    protected $descricao;
    protected $member_of;
    protected $script;
    protected $dn;
    protected $inf;
    protected $situacao;
    protected $departamento;
    protected $token;
    private $username;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken()
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * @param mixed $departamento
     */
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;
    }

    /**
     * @return mixed
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param mixed $situacao
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;
    }

    /**
     * @return null
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * @param null $matricula
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;
    }


    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }


    /**
     * @return null
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param null $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getPrimeiroNome()
    {
        return $this->primeiro_nome;
    }

    /**
     * @param mixed $primeiro_nome
     */
    public function setPrimeiroNome($primeiro_nome)
    {
        $this->primeiro_nome = $primeiro_nome;
    }

    /**
     * @return mixed
     */
    public function getSegundoNome()
    {
        return $this->segundo_nome;
    }

    /**
     * @param mixed $segundo_nome
     */
    public function setSegundoNome($segundo_nome)
    {
        $this->segundo_nome = $segundo_nome;
    }

    /**
     * @return mixed
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * @param mixed $documento
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    /**
     * @return mixed
     */
    public function getVinculo()
    {
        return $this->vinculo;
    }

    /**
     * @param mixed $vinculo
     */
    public function setVinculo($vinculo)
    {
        $this->vinculo = $vinculo;
    }

    /**
     * @return mixed
     */
    public function getRamal()
    {
        return $this->ramal ? $this->ramal : '';
    }

    /**
     * @param mixed $ramal
     */
    public function setRamal($ramal)
    {
        $this->ramal = $ramal;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email ? $this->email : '';
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmailAlt()
    {
        return $this->email_alt ? $this->email_alt : '';
    }

    /**
     * @param mixed $email_alt
     */
    public function setEmailAlt($email_alt)
    {
        $this->email_alt = $email_alt;
    }

    /**
     * @return string
     */
    public function getAlteradoPor()
    {
        return $this->alterado_por ? $this->alterado_por : '';
    }

    /**
     * @param string $alterado_por
     */
    public function setAlteradoPor($alterado_por)
    {
        $this->alterado_por = $alterado_por;
    }

    /**
     * @return Usuario
     */
    public function getResponsavel()
    {
        if(!($this->responsavel instanceof Usuario))
            $this->responsavel = Usuario::load($this->responsavel);

        return $this->responsavel;
    }

    /**
     * @param null $responsavel
     */
    public function setResponsavel($responsavel)
    {
        $this->responsavel = $responsavel;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param string $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return mixed
     */
    public function getPermissao()
    {
        return $this->permissao;
    }

    /**
     * @param mixed $permissao
     */
    public function setPermissao($permissao)
    {
        $this->permissao = $permissao;
    }



    /**
     * @return null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param null $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getValidade()
    {
        return $this->validade;
    }

    /**
     * @param float $validade
     */
    public function setValidade(\DateTime $validade)
    {
        $this->validade = $validade;
    }

    /**
     * @return null
     */
    public function getCota()
    {
        return $this->cota;
    }

    /**
     * @param null $cota
     */
    public function setCota($cota)
    {
        $this->cota = $cota;
    }

    /**
     * @return null
     */
    public function getCriadoEm()
    {
        return $this->criado_em;
    }

    /**
     * @param null $criado_em
     */
    public function setCriadoEm($criado_em)
    {
        $this->criado_em = $criado_em;
    }

    /**
     * @return null
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param null $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getMemberOf()
    {
        return $this->member_of;
    }

    /**
     * @param mixed $member_of
     */
    public function setMemberOf($member_of)
    {
        $this->member_of = $member_of;
    }

    /**
     * @return mixed
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * @param mixed $script
     */
    public function setScript($script)
    {
        $this->script = $script;
    }

    /**
     * @return mixed
     */
    public function getDn()
    {
        return $this->dn;
    }

    /**
     * @param mixed $dn
     */
    public function setDn($dn)
    {
        $this->dn = $dn;
    }



    /**
     * Funcao para quebrar em primeiro nome e segundo
     * @param $nomecompleto
     * @return array prmeiro_nome e segundo_nome
     */
    public static function splitNome($nomecompleto){
        $string_splited = explode(" ", trim($nomecompleto));
        $nome = array(0 => '', 1 => '');
        foreach($string_splited as $palavra){
            if($nome[0] == '') {
                $nome[0] = $palavra;
            } else { //primeiro nome ja preenchido
                $nome[1] .= ' '.$palavra;
            }
        }
        return $nome;
    }

    /**
     * Cria usuario no AD
     * @param $generatePass
     * @param $changepassnextlogon
     * @param UO $uo
     * @return bool
     */
    public function create($generatePass, $changepassnextlogon, UO $uo){

        $script = $uo->getNome().'.bat';

        $this->setDn('CN='.$this->getNome().',OU='.$uo->getNome().',DC=fcm,DC=unicamp,DC=br');

        $att = array(
            'objectclass' => array('0' => 'organizationalPerson',
                '1' => 'person',
                '2' => 'top',
                '3' => 'user'),
            'dn' => $this->getDn(),
            'displayName' => array('0' => $this->getNome()),
            'UserAccountControl' => array('0' => '544'),
            'userPrincipalName' => array('0' =>  $this->getUsername() . '@fcm.unicamp.br'),
            'GivenName' => array('0' => $this->getPrimeiroNome()),
            'sn' => array('0' => $this->getSegundoNome()),
            'name' => array('0' => $this->getNome()),
            'department' => array('0' =>  $uo->getDescricao()),
            'description' => array('0' =>  $this->descricao),
            'samaccountname' => array('0' =>  $this->username),
            'EmployeeID' => array('0' => $this->getDocumento()),
            'EmployeeNumber' => array('0' => $this->matricula),
            'manager' => array('0' => $this->getResponsavel()->getDn()),
            'Company' => array('0' => $this->vinculo),
            'mail' => array('0' => $this->getEmail()),
            'telephoneNumber' => array('0' => $this->ramal),
            'mobile' => array('0' => $this->celular),
            'otherMailbox' => array('0' => $this->email_alt),
            'scriptPath' => array('0' => $script),
            'postalcode' => array('0' => $this->getCodArea()),
            'flags' => array('0' => $this->getCota()), //cota
        );

        $AD = new AD();
        $result = $AD->createLdapEntry($att, $this->getDn());

        if($result){
            $AD->addUserToGroup($this, $this->getUO());
            $AD->setExperation($this, $this->getValidade());

            // Define cota caso tenha
            if($this->getCota() > 0) Usuario::setCota2($this->getUsername(), $this->getCota());
        } else {
            return false;
        }

        if($generatePass) {

            $chars1 = "abcdefghjkmnpqrstuvwxyz";
            $chars2 = "ABCDEFGHJKLMNPQRSTUVWXYZ";
            $number = "23456789";
            $pass = substr(str_shuffle($chars1), 0, 2) . substr(str_shuffle($chars2), 0, 3) . substr(str_shuffle($number), 0, 4);
            $this->setToken($pass);

            $AD->definePass($this, $pass);
            $AD->enableUser($this);

            // Define status para redefinr senha no próximo logon
            if ($changepassnextlogon) $AD->changePassNextLogon($this->username);
        }

        return true;
    }

    /** Obtem informacao do usuario
     * @param $username
     * @return mixed
     */
    public static function load($username)
    {
        $att = AD::searchUser($username);

        if(!$att) return null;

        /** @var Usuario $usuario */
        $usuario = new Usuario();
        $usuario->setUsername($att[0]['samaccountname'][0]);

        // Condicao para usuario valido
        if(isset($att[0]['displayname'][0])) $usuario->setNome($att[0]['displayname'][0]);
        else return null;

        if(isset($att[0]['employeenumber'][0])) $usuario->setMatricula($att[0]['employeenumber'][0]);
        if(isset($att[0]['givenname'][0])) $usuario->setPrimeiroNome($att[0]['givenname'][0]);
        if(isset($att[0]['sn'][0])) $usuario->setSegundoNome($att[0]['sn'][0]);
        if(isset($att[0]['department'][0])) $usuario->setDepartamento($att[0]['department'][0]);
        if(isset($att[0]['employeeid'][0])) $usuario->setDocumento($att[0]['employeeid'][0]);
        if(isset($att[0]['company'][0])) $usuario->setVinculo($att[0]['company'][0]);
        if(isset($att[0]['telephonenumber'][0])) $usuario->setRamal($att[0]['telephonenumber'][0]);
        if(isset($att[0]['mail'][0])) $usuario->setEmail($att[0]['mail'][0]);
        if(isset($att[0]['othermailbox'][0])) $usuario->setEmailAlt($att[0]['othermailbox'][0]);
        if(isset($att[0]['description'][0])) $usuario->setDescricao($att[0]['description'][0]);


        $usuario->setDn($att[0]['dn']);
        $usuario->setValidade(AD::convertLDAPtoLinux($att[0]['accountexpires'][0]));

        if(($att[0]['pwdlastset'][0] == 0) && ($att[0]['useraccountcontrol'][0] == '512'))$status = 999;
        else $status = $att[0]['useraccountcontrol'][0];
        $usuario->setStatus($status);


        if(isset($att[0]['manager'][0])) {
            if ($att[0]['manager'][0] == $att[0]['dn'])
                $usuario->setResponsavel($usuario);
            else {
                $result = AD::searchByDn($att[0]['manager'][0]);
                $usuario->setResponsavel(Usuario::load($result[0]['samaccountname'][0]));
            }
        }

        return $usuario;
    }

    /**
     * @Assert\Callback(groups={"new_visitante"})
     */
    public function validate(ExecutionContextInterface $context, $payload = null)
    {
        // Verifica documento
        $result = AD::search('(&(objectCategory=CN=Person,CN=Schema,CN=Configuration,DC=fcm,DC=unicamp,DC=br)
                    (employeeID='.$this->getDocumento().')(!(sAMAccountName='.$this->getUsername().')))');

        if($result){
            $context->buildViolation('Documento já associado ao usuário '. $result[0]['samaccountname'][0])
                ->atPath('documento')
                ->addViolation();
        }
    }

    public static function valCPF($cpf){
        $result = true;
        if(!is_numeric($cpf)) return false;

        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            $result = false;
        }

        else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
            $result = false;
        } else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    $result = false;
                }
            }
        }

        if(!$result){
            return false;
        }

        return $result;
    }

    /**
     * Valida CPF
     * @Assert\Callback(groups={"cpf"})
     */
    public function validateCPF(ExecutionContextInterface $context, $payload = null){
        $result = true;
        $cpf = str_pad($this->getDocumento(), 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            $result = false;
        }

        else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
            $result = false;
        } else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    $result = false;
                }
            }
        }

        if(!$result){
            $context->buildViolation('CPF inválido')
                ->atPath('documento')
                ->addViolation();
        }
    }

    /**
     * @return bool
     */
    public function delete(){
        $ad = new AD();
        return $ad->deleteEntry($this->getDn());
    }



}