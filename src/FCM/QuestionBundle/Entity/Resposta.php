<?php

namespace FCM\QuestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\UserBundle\Entity\User;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Resposta
 *
 * @ORM\Table(name="resposta")
 * @ORM\Entity(repositoryClass="FCM\QuestionBundle\Repository\RespostaRepository")
 * @Vich\Uploadable
 * @Serializer\ExclusionPolicy("all")
 */
class Resposta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    public function __construct()
    {
        $this->respostaItens = new ArrayCollection();
    }

    /**
     * @ORM\ManyToOne(targetEntity="\FCM\EventoBundle\Entity\Inscricao", inversedBy="respostas")
     * @ORM\JoinColumn(name="inscricao_id", referencedColumnName="id")
     */
    private $inscricao;

    /**
     * @ORM\ManyToOne(targetEntity="Questionario", inversedBy="respostas")
     * @ORM\JoinColumn(name="questionario_id", referencedColumnName="id")
     */
    private $questionario;

    /**
     * @ORM\OneToMany(targetEntity="RespostaItem", mappedBy="resposta", cascade={"remove","persist"})
     * @Assert\Valid
     * @Serializer\Expose()
     */
    private $respostaItens;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     *
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="\FCM\UserBundle\Entity\User", inversedBy="respostas")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="deposito", fileNameProperty="depositoName")
     * @Serializer\Expose()
     * @var File
     */
    private $deposito;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     *
     * @var string
     */
    private $depositoName;

    /**
     * @var bool
     *
     * @ORM\Column(name="pago", type="boolean", nullable=true)
     * @Serializer\Expose()
     */
    private $pago;

    /**
     * @var bool
     *
     * @ORM\Column(name="notified_pago", type="boolean", nullable=true)
     */
    private $notifiedPago;

    /**
     * @var bool
     *
     * @ORM\Column(name="approved", type="boolean")
     */
    private $approved = false;

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->approved;
    }

    public function isApprovedLabel(){
        if($this->isApproved()) return 'Sim';
        return 'Não';
    }

    /**
     * @param bool $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

    /**
     * @return bool
     */
    public function isNotifiedPago()
    {
        return $this->notifiedPago;
    }

    /**
     * @param bool $notifiedPago
     */
    public function setNotifiedPago($notifiedPago)
    {
        $this->notifiedPago = $notifiedPago;
    }

    /**
     * @return bool
     */
    public function isPago()
    {
        return $this->pago;
    }

    /**
     * @param bool $pago
     */
    public function setPago($pago)
    {
        $this->pago = $pago;
    }

    /**
     * @return string
     */
    public function isPagoLabel(){
        if($this->pago) return 'SIM';
        return 'NÃO';
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return Evento
     */
    public function setDeposito(File $deposito = null)
    {
        $this->deposito = $deposito;

        if ($deposito) {
            $this->setCreated(new \DateTime());
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getDeposito()
    {
        return $this->deposito;
    }

    /**
     * @param string $depositoName
     *
     * @return Evento
     */
    public function setDepositoName($depositoName)
    {
        $this->depositoName = $depositoName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDepositoName()
    {
        return $this->depositoName;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set inscricao
     *
     * @param \FCM\Eventobundle\Inscricao $inscricao
     *
     * @return Resposta
     */
    public function setInscricao(Inscricao $inscricao = null)
    {
        $this->inscricao = $inscricao;

        return $this;
    }

    /**
     * Get inscricao
     *
     * @return Inscricao
     */
    public function getInscricao()
    {
        return $this->inscricao;
    }



    /**
     * Add respostaIten
     *
     * @param \FCM\QuestionBundle\Entity\RespostaItem $respostaIten
     *
     * @return Resposta
     */
    public function addRespostaIten(\FCM\QuestionBundle\Entity\RespostaItem $respostaIten)
    {
        $this->respostaItens[] = $respostaIten;

        return $this;
    }

    /**
     * Remove respostaIten
     *
     * @param \FCM\QuestionBundle\Entity\RespostaItem $respostaIten
     */
    public function removeRespostaIten(\FCM\QuestionBundle\Entity\RespostaItem $respostaIten)
    {
        $this->respostaItens->removeElement($respostaIten);
    }

    /**
     * Get respostaItens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRespostaItens()
    {
        return $this->respostaItens;
    }

    /**
     * Set questionario
     *
     * @param \FCM\QuestionBundle\Entity\Questionario $questionario
     *
     * @return Resposta
     */
    public function setQuestionario(\FCM\QuestionBundle\Entity\Questionario $questionario = null)
    {
        $this->questionario = $questionario;

        return $this;
    }

    /**
     * Get questionario
     *
     * @return \FCM\QuestionBundle\Entity\Questionario
     */
    public function getQuestionario()
    {
        return $this->questionario;
    }

    /**
     * Get pago
     *
     * @return boolean
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * Get notifiedPago
     *
     * @return boolean
     */
    public function getNotifiedPago()
    {
        return $this->notifiedPago;
    }

    /**
     * Retorna quantas vezes um trabalho foi avaliado
     */
    public function getNumAvaliacao(){
        /** @var Inscricao $inscricao */
        $inscricao = $this->getInscricao();

        /** @var ArrayCollection $avaliacoes */
        $avaliacoes = $inscricao->getRespostaType(Questionario::TYPE_AVALIADOR);

        // Nao avaliado
        if(!$avaliacoes->count()) return 0;

        return $avaliacoes->count();
    }

    /**
     * Retorna status de um trabalho
     * @param User|null $user
     * @return int
     */
    public function getAvaliacaoStatus(User $user = null){
        /** @var Inscricao $inscricao */
        $inscricao = $this->getInscricao();

        /** @var ArrayCollection $avaliacoes */
        $avaliacoes = $inscricao->getRespostaType(Questionario::TYPE_AVALIADOR);

        // Nao avaliado
        if(!$avaliacoes->count()) return 0;

        if($user instanceof User) {
            /** @var Resposta $avaliacoe */
            foreach ($avaliacoes as $avaliacoe) {
                if ($avaliacoe->getCreatedBy()->getUsername() == $user->getUsername()) return 2;
            }
        }

        // Possivel avaliar
        if($avaliacoes->count() < $inscricao->getEventoAtividade()->getQuestionarioAvaliador()->getMax())
            return 1;

        // Atingiu limite de avaliacoes
        if($avaliacoes->count() >= $inscricao->getEventoAtividade()->getQuestionarioAvaliador()->getMax())
            return 2;
    }




}
