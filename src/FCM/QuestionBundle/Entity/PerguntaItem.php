<?php

namespace FCM\QuestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * PerguntaItem
 *
 * @ORM\Table(name="pergunta_item")
 * @ORM\Entity(repositoryClass="FCM\QuestionBundle\Repository\PerguntaItemRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class PerguntaItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="FCM\QuestionBundle\Entity\Pergunta", inversedBy="perguntaItens")
     * @ORM\JoinColumn(name="pergunta_id", referencedColumnName="id")
     */
    private $pergunta;

    /**
     * @ORM\OneToMany(targetEntity="FCM\QuestionBundle\Entity\RespostaItem", mappedBy="perguntaItem",
     *     cascade={"remove","persist"})
     */
    private $respostaItens;

    /**
     * @var string
     *
     * @ORM\Column(name="text_inscricao", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    private $textInscricao;

    /**
     * @var string
     *
     * @ORM\Column(name="text_certificado", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    private $textCertificado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ini_exibicao", type="date", nullable=true)
     */
    private $dataIniExibicao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_fim_exibicao", type="date", nullable=true)
     */
    private $dataFimExibicao;

    /**
     * @var int
     *
     * @ORM\Column(name="lim_escolha", type="smallint", nullable=true)
     */
    private $limEscolha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_lim_deposito", type="datetime", nullable=true)
     */
    private $dataLimDeposito;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2, nullable=true)
     * @Serializer\Expose()
     *
     */
    private $valor;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set textInscricao
     *
     * @param string $textInscricao
     *
     * @return PerguntaItem
     */
    public function setTextInscricao($textInscricao)
    {
        $this->textInscricao = $textInscricao;

        return $this;
    }

    /**
     * Get textInscricao
     *
     * @return string
     */
    public function getTextInscricao()
    {
        return $this->textInscricao;
    }

    /**
     * Set textCertificado
     *
     * @param string $textCertificado
     *
     * @return PerguntaItem
     */
    public function setTextCertificado($textCertificado)
    {
        $this->textCertificado = $textCertificado;

        return $this;
    }

    /**
     * Get textCertificado
     *
     * @return string
     */
    public function getTextCertificado()
    {
        return $this->textCertificado;
    }

    /**
     * Set dataIniExibicao
     *
     * @param \DateTime $dataIniExibicao
     *
     * @return PerguntaItem
     */
    public function setDataIniExibicao($dataIniExibicao)
    {
        $this->dataIniExibicao = $dataIniExibicao;

        return $this;
    }

    /**
     * Get dataIniExibicao
     *
     * @return \DateTime
     */
    public function getDataIniExibicao()
    {
        return $this->dataIniExibicao;
    }

    /**
     * Set dataFimExibicao
     *
     * @param \DateTime $dataFimExibicao
     *
     * @return PerguntaItem
     */
    public function setDataFimExibicao($dataFimExibicao)
    {
        $this->dataFimExibicao = $dataFimExibicao;

        return $this;
    }

    /**
     * Get dataFimExibicao
     *
     * @return \DateTime
     */
    public function getDataFimExibicao()
    {
        return $this->dataFimExibicao;
    }

    /**
     * Set limEscolha
     *
     * @param integer $limEscolha
     *
     * @return PerguntaItem
     */
    public function setLimEscolha($limEscolha)
    {
        $this->limEscolha = $limEscolha;

        return $this;
    }

    /**
     * Get limEscolha
     *
     * @return int
     */
    public function getLimEscolha()
    {
        return $this->limEscolha;
    }

    /**
     * Set valor
     *
     * @param string $valor
     *
     * @return PerguntaItem
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->respostaItens = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set pergunta
     *
     * @param \FCM\QuestionBundle\Entity\Pergunta $pergunta
     *
     * @return PerguntaItem
     */
    public function setPergunta(\FCM\QuestionBundle\Entity\Pergunta $pergunta = null)
    {
        $this->pergunta = $pergunta;

        return $this;
    }

    /**
     * Get pergunta
     *
     * @return \FCM\QuestionBundle\Entity\Pergunta
     */
    public function getPergunta()
    {
        return $this->pergunta;
    }

    /**
     * Add respostaIten
     *
     * @param \FCM\QuestionBundle\Entity\RespostaItem $respostaIten
     *
     * @return PerguntaItem
     */
    public function addRespostaIten(\FCM\QuestionBundle\Entity\RespostaItem $respostaIten)
    {
        $this->respostaItens[] = $respostaIten;

        return $this;
    }

    /**
     * Remove respostaIten
     *
     * @param \FCM\QuestionBundle\Entity\RespostaItem $respostaIten
     */
    public function removeRespostaIten(\FCM\QuestionBundle\Entity\RespostaItem $respostaIten)
    {
        $this->respostaItens->removeElement($respostaIten);
    }

    /**
     * Get respostaItens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRespostaItens()
    {
        return $this->respostaItens;
    }

    /**
     * Set dataLimDeposito
     *
     * @param \DateTime $dataLimDeposito
     *
     * @return Pergunta
     */
    public function setDataLimDeposito($dataLimDeposito)
    {
        $this->dataLimDeposito = $dataLimDeposito;

        return $this;
    }

    /**
     * Get dataLimDeposito
     *
     * @return \DateTime
     */
    public function getDataLimDeposito()
    {
        return $this->dataLimDeposito;
    }

    public function getPagos(){
        $pagos = 0;

        /** @var RespostaItem $respostaIten */
        foreach ($this->getRespostaItens() as $respostaIten){
            if($respostaIten->getResposta()->getPago()) $pagos++;
        }

        return $pagos;
    }
}
