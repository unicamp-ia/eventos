<?php

namespace FCM\QuestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Pergunta
 *
 * @ORM\Table(name="pergunta")
 * @ORM\Entity(repositoryClass="FCM\QuestionBundle\Repository\PerguntaRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class Pergunta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    public function __construct()
    {
        $this->respostas = new ArrayCollection();
        $this->perguntaItens = new ArrayCollection();

    }

    /**
     * @ORM\OneToMany(targetEntity="FCM\QuestionBundle\Entity\PerguntaItem", mappedBy="pergunta", cascade={"remove","persist"})
     */
    private $perguntaItens;


    /**
     * @ORM\ManyToOne(targetEntity="Questionario", inversedBy="perguntas")
     * @ORM\JoinColumn(name="questionario_id", referencedColumnName="id")
     */
    private $questionario;

    /**
     * @ORM\OneToMany(targetEntity="RespostaItem", mappedBy="pergunta", cascade={"remove", "persist"})
     */
    private $respostaItens;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="text")
     * @Serializer\Expose()
     */
    private $nome;

    /**
     * @var bool
     *
     * @ORM\Column(name="required", type="boolean")
     */
    private $required;

    /**
     * @var int
     *
     * @ORM\Column(name="delta", type="smallint")
     */
    private $delta = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="check_app", type="boolean")
     * @Serializer\Expose()
     */
    private $checkApp;

    /**
     * @return bool
     */
    public function isCheckApp()
    {
        return $this->checkApp;
    }

    /**
     * @param bool $checkApp
     */
    public function setCheckApp($checkApp)
    {
        $this->checkApp = $checkApp;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="text")
     * @Serializer\Expose()
     */
    private $type;



    /**
     * @var string
     *
     * @ORM\Column(name="options", type="text", nullable=true)
     */
    private $options;

    /**
     * @var string
     *
     * @ORM\Column(name="placeholder", type="text", nullable=true)
     */
    private $placeholder;

    /**
     * @var int
     *
     * @ORM\Column(name="anonimo", type="boolean", options={"default" = 0}, nullable=true)
     */
    private $anonimo;

    /**
     * Exibe pergunta aos avaliadores
     * @var int
     *
     * @ORM\Column(name="show_avaliacao", type="boolean", options={"default" = 0}, nullable=true)
     */
    private $showAvaliacao;

    /**
     * Exibe pergunta aos avaliadores
     * @var int
     *
     * @ORM\Column(name="show_avaliacao_lista", type="boolean", options={"default" = 0}, nullable=true)
     */
    private $showAvaliacaoLista;

    /**
     * @return int
     */
    public function getShowAvaliacaoLista()
    {
        return $this->showAvaliacaoLista;
    }

    /**
     * @param int $showAvaliacaoLista
     */
    public function setShowAvaliacaoLista($showAvaliacaoLista)
    {
        $this->showAvaliacaoLista = $showAvaliacaoLista;
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Pergunta
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set required
     *
     * @param boolean $required
     *
     * @return Pergunta
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get required
     *
     * @return bool
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set delta
     *
     * @param integer $delta
     *
     * @return Pergunta
     */
    public function setDelta($delta)
    {
        $this->delta = $delta;

        return $this;
    }

    /**
     * Get delta
     *
     * @return int
     */
    public function getDelta()
    {
        return $this->delta;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Pergunta
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public static function getTypeChoices(){
        return [
            'Symfony\Component\Form\Extension\Core\Type\TextType' => 'Texto',
            'Symfony\Component\Form\Extension\Core\Type\TextareaType' => 'Caixa de texto',
            'Symfony\Component\Form\Extension\Core\Type\ChoiceType' => 'Caixa de seleção (único)',
            'Symfony\Component\Form\Extension\Core\Type\CheckBoxType' => 'Caixa de seleção (muitos)',
            // 'Symfony\Component\Form\Extension\Core\Type\NumberType' => 'Número',
            'Vich\UploaderBundle\Form\Type\VichFileType' => 'Arquivo',
        ];
    }

    /**
     * Set options
     *
     * @param string $options
     *
     * @return Pergunta
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    public function getPerguntaItensArr(){
        $items = array();
        /** @var PerguntaItem $perguntaIten */
        foreach ($this->getPerguntaItens() as $perguntaIten){
            $items[$perguntaIten->getTextInscricao()] = $perguntaIten->getTextInscricao();
        }

        return $items;
    }

    /**
     * Prepara as opcoes
     * @return array Retorna array do tipo valor|label
     */
    public function getOptionsPrepared($isInscricao = false, $isNew = false){
        $choices = array();

        $today = new \DateTime();

        if($this->getPerguntaItens()->count()){
            $itens = $this->getPerguntaItens();

            /** @var PerguntaItem $iten */
            foreach ($itens as $iten){
                $label = $iten->getTextInscricao();

                if($isInscricao and $iten->getValor() > 0){
                    $label = $label . ' (R$' . str_replace('.',',',$iten->getValor()) . ')';
                }


                // Somente data inicial de exibicao
                if($isNew and $isInscricao and $iten->getDataIniExibicao() and !$iten->getDataFimExibicao()){
                    // Nao exibir caso ainda nao esteja na data inicial de exibicao
                    if($today < $iten->getDataIniExibicao()) continue;
                }

                // Somente data final de exibicao
                if($isNew and $isInscricao and $iten->getDataFimExibicao() and !$iten->getDataIniExibicao()){
                    if($today > $iten->getDataFimExibicao()) continue;
                }

                // Com data inicial e final de exibicao
                if($isNew and $isInscricao and $iten->getDataFimExibicao() and $iten->getDataIniExibicao()){
                    if($today < $iten->getDataIniExibicao() or $today > $iten->getDataFimExibicao()) continue;
                }


                // Verfica se possui limite de escolha, quando atingindo o limite a opcao nao eh exibida ao participante
                if($iten->getLimEscolha()){

                    $inscricoes = 0;
                    /** @var RespostaItem $respostaIten */
                    foreach ($this->getRespostaItens() as $respostaIten){

                        if($respostaIten->getPerguntaItem() instanceof PerguntaItem
                            and $respostaIten->getPerguntaItem()->getId() == $iten->getId()){
                            $inscricoes++;
                        }
                    }

                    if($inscricoes > $iten->getLimEscolha()){
                        $label = $label . ' (esgotado)';
                    } else {
                        $restantes = $iten->getLimEscolha() - $inscricoes;

                        $label = $label . ' ('. $restantes . ' vagas restantes)';
                    }
                }

                $choices[$iten->getTextInscricao()] = $label;

            }

            return $choices;
        }


        $options_arr = preg_split("/\\r\\n|\\r|\\n/",$this->getOptions());

        foreach ($options_arr as $row){
            // Separa os valores em chave|valor
            if($pos = strpos($row,'|')){
                // Para inscricao o modelo eh chave|chave
                if($isInscricao){
                    $choices[substr($row,0,$pos)] = substr($row,0,$pos);
                } else {
                    // Para certificado modelo eh chave|valor
                    $choices[substr($row,0,$pos)] = substr($row,$pos+1);
                }

            } else {
                $choices[$row] = $row;
            }
        }

        return $choices;
    }

    /**
     * Set placeholder
     *
     * @param string $placeholder
     *
     * @return Pergunta
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * Get placeholder
     *
     * @return string
     */
    public function getPlaceholder()
    {
        return $this->placeholder;
    }


    /**
     * Set questionario
     *
     * @param \FCM\QuestionBundle\Entity\Questionario $questionario
     *
     * @return Pergunta
     */
    public function setQuestionario(\FCM\QuestionBundle\Entity\Questionario $questionario = null)
    {
        $this->questionario = $questionario;

        return $this;
    }

    /**
     * Get questionario
     *
     * @return \FCM\QuestionBundle\Entity\Questionario
     */
    public function getQuestionario()
    {
        return $this->questionario;
    }



    /**
     * Add respostaIten
     *
     * @param \FCM\QuestionBundle\Entity\RespostaItem $respostaIten
     *
     * @return Pergunta
     */
    public function addRespostaIten(\FCM\QuestionBundle\Entity\RespostaItem $respostaIten)
    {
        $this->respostaItens[] = $respostaIten;

        return $this;
    }

    /**
     * Remove respostaIten
     *
     * @param \FCM\QuestionBundle\Entity\RespostaItem $respostaIten
     */
    public function removeRespostaIten(\FCM\QuestionBundle\Entity\RespostaItem $respostaIten)
    {
        $this->respostaItens->removeElement($respostaIten);
    }

    /**
     * Get respostaItens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRespostaItens()
    {
        return $this->respostaItens;
    }

    /**
     * Retorna o título do tipo da pergunta conforme o tipo da pergunta
     */
    public function getTypeString()
    {
        switch ($this->getType()) {

            case 'Symfony\Component\Form\Extension\Core\Type\TextType':
                return 'Pergunta e resposta (texto)';
                break;

            case 'Symfony\Component\Form\Extension\Core\Type\TextareaType':
                return 'Pergunta e resposta (area de texto)';
                break;

            case 'Symfony\Component\Form\Extension\Core\Type\ChoiceType':
                return 'Checkbox (uma escolha)';
                break;

            case 'Symfony\Component\Form\Extension\Core\Type\CheckBoxType':
                return 'Checkbox (multiplas escolhas)';
                break;

            case 'Symfony\Component\Form\Extension\Core\Type\NumberType':
                return  'Número';
                break;

            case 'Vich\UploaderBundle\Form\Type\VichFileType':
                return 'Upload';
                break;

            default:
                return 'Texto (nada definido)';
                break;
        }
    }

    /**
     * Retorna o numero de mesma ocorrencia das respostas
     *
     * @var $order Se for true, ordena por tamanho, maior para menor
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSameAnswers($order = true)
    {
        $grouped_answers = array();

        //Foreach das respostas
        foreach ($this->getRespostaItens()->getValues() as $entry) {
            //Pega todas as respostas (inclusive duplicadas)
            $grouped_answers[$entry->getId()] = [
                'resposta' => $entry->getText(), //Resposta do usuario
                'username' => $entry->getResposta()->getInscricao()->getParticipante()->getNome() //Nome de quem respondeu
            ];
        }

        //Monta um array que contem o a resposta e os nomes de quem respondeu
        foreach ($grouped_answers as $key => $value) {
            //Monta o array com o nome das pessoas que escolheram a resposta
            $newArr[$value['resposta']][] = $value['username'];
        }

        //Ordena array do menor para o maior
        asort($newArr);

        //Inverte a ordem do array: Maior para menor mantendo a key
        return array_reverse($newArr, true);
    }

    /**
     * Define um padrão de escala proporcional baseado nos valores passados por parametros
     *
     * @param float $quantidade valor do coeficiente
     * @param array $respostas todos os valores
     * @param float|int $max_limit limite maximo de escala
     * @param float|int $min_limit limite minimo de escala
     *
     * @return float/int número proporcional aos limites
     */
    public function setSizeScale($quantidade, $respostas, $max_limit = 32, $min_limit = 11)
    {
        //Verifica qual o maior valor das respostas
        $maior = 0;
        foreach ($respostas as $key => $value) {
            $maior = count($value) > $maior ? $value : $maior;
        }

        //Variação do maior e menor valor, dividido pela maior quantidade do array
        $multiplier = ($max_limit - $min_limit) / count($maior);

        //Monta a equação para ser resolvida pelo eval posteriomente
        //f(x) = [(x) . $multiplier] + limite minimo
        $nums = "((". $quantidade .") * ". $multiplier .") + " . $min_limit;

        //Resolve a equação acima
        eval("\$nums = $nums;");

        return $nums;
    }

    /**
     * Gets the value of anonimo.
     *
     * @return int
     */
    public function getAnonimo()
    {
        return $this->anonimo;
    }

    /**
     * Sets the value of anonimo.
     *
     * @param int $anonimo the anonimo
     *
     * @return self
     */
    public function setAnonimo($anonimo)
    {
        $this->anonimo = $anonimo;

        return $this;
    }



    /**
     * Add perguntaIten
     *
     * @param \FCM\QuestionBundle\Entity\PerguntaItem $perguntaIten
     *
     * @return Pergunta
     */
    public function addPerguntaIten(\FCM\QuestionBundle\Entity\PerguntaItem $perguntaIten)
    {
        $this->perguntaItens[] = $perguntaIten;

        return $this;
    }

    /**
     * Remove perguntaIten
     *
     * @param \FCM\QuestionBundle\Entity\PerguntaItem $perguntaIten
     */
    public function removePerguntaIten(\FCM\QuestionBundle\Entity\PerguntaItem $perguntaIten)
    {
        $this->perguntaItens->removeElement($perguntaIten);
    }

    /**
     * Get perguntaItens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPerguntaItens()
    {
        return $this->perguntaItens;
    }

    /**
     * Set showAvaliacao
     *
     * @param boolean $showAvaliacao
     *
     * @return Pergunta
     */
    public function setShowAvaliacao($showAvaliacao)
    {
        $this->showAvaliacao = $showAvaliacao;

        return $this;
    }

    /**
     * Get showAvaliacao
     *
     * @return boolean
     */
    public function getShowAvaliacao()
    {
        return $this->showAvaliacao;
    }

    public function isChoiceType(){
        if($this->type == 'Symfony\Component\Form\Extension\Core\Type\ChoiceType') return true;
    }

    public function getPercentChoices(){
        $total = 0;

        $result = array();

        /** @var PerguntaItem $perguntaIten */
        foreach ($this->getPerguntaItens() as $perguntaIten){
            $result[$perguntaIten->getId()]['total'] = $perguntaIten->getRespostaItens()->count();
            $result[$perguntaIten->getId()]['percent'] = $perguntaIten->getRespostaItens()->count();
        }

        return $result;
    }


}
