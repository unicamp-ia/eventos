<?php

namespace FCM\QuestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RespostaItens
 *
 * @ORM\Table(name="resposta_item")
 * @ORM\Entity(repositoryClass="FCM\QuestionBundle\Repository\RespostaItensRepository")
 * @Vich\Uploadable
 */
class RespostaItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="FCM\QuestionBundle\Entity\PerguntaItem", inversedBy="respostaItens")
     * @ORM\JoinColumn(name="pergunta_item_id", referencedColumnName="id")
     */
    private $perguntaItem;

    /**
     * @ORM\ManyToOne(targetEntity="Resposta", inversedBy="respostaItens",cascade={"persist"})
     * @ORM\JoinColumn(name="resposta_id", referencedColumnName="id")
     */
    private $resposta;

    /**
     * @ORM\ManyToOne(targetEntity="Pergunta", inversedBy="respostaItens")
     * @ORM\JoinColumn(name="pergunta_id", referencedColumnName="id")
     */
    private $pergunta;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="fileName")
     *
     * @var File
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     *
     * @var string
     */
    private $fileName;

    /**
     * @ORM\Column(name="is_anonino", type="boolean", nullable=true, options={"default" = 0})
     *
     * @var string
     */
    private $is_anonimo;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valor;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return RespostaItem
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set pergunta
     *
     * @param \FCM\QuestionBundle\Entity\Pergunta $pergunta
     *
     * @return Resposta
     */
    public function setPergunta(\FCM\QuestionBundle\Entity\Pergunta $pergunta = null)
    {
        $this->pergunta = $pergunta;

        return $this;
    }

    /**
     * Get pergunta
     *
     * @return \FCM\QuestionBundle\Entity\Pergunta
     */
    public function getPergunta()
    {
        return $this->pergunta;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return Resposta
     */
    public function setFile($file = null)
    {
        $this->file = $file;

        if ( $file ) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            // $this->setCreated(new \DateTime('now'));
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $fileName
     *
     * @return Resposta
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set resposta
     *
     * @param \FCM\QuestionBundle\Entity\Resposta $resposta
     *
     * @return RespostaItem
     */
    public function setResposta(\FCM\QuestionBundle\Entity\Resposta $resposta = null)
    {
        $this->resposta = $resposta;

        return $this;
    }

    /**
     * Get resposta
     *
     * @return \FCM\QuestionBundle\Entity\Resposta
     */
    public function getResposta()
    {
        return $this->resposta;
    }

    /**
     * Gets the value of is_anonimo.
     *
     * @return string
     */
    public function getIsAnonimo()
    {
        return $this->is_anonimo;
    }

    /**
     * Sets the value of is_anonimo.
     *
     * @param string $is_anonimo the is anonimo
     *
     * @return self
     */
    public function setIsAnonimo($is_anonimo)
    {
        $this->is_anonimo = $is_anonimo;

        return $this;
    }

    /**
     * Retorna as respostas tratadas
     * @return string
     */
    public function getRespostaPrepared(){
        switch ($this->getPergunta()->getType()){
            // Trata para selecao multipla, prepara resposta do tipo "OPCAO 1","OPCAO 2"
            case 'Symfony\Component\Form\Extension\Core\Type\CheckBoxType':
                $options = $this->getPergunta()->getOptionsPrepared();

                $val = array();
                foreach (unserialize($this->getText()) as $resposta){
                    $val[] = $options[$resposta];
                }

                return implode(',',$val);
                break;

            // Selecao UNICA
            case 'Symfony\Component\Form\Extension\Core\Type\ChoiceType':
                $choices = $this->getPergunta()->getOptionsPrepared();
                return $choices[$this->getText()];
                break;

            default:
                return $this->getText();
                break;
        }
    }

    /**
     * Set valor
     *
     * @param string $valor
     *
     * @return RespostaItem
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set perguntaItem
     *
     * @param \FCM\QuestionBundle\Entity\PerguntaItem $perguntaItem
     *
     * @return RespostaItem
     */
    public function setPerguntaItem(\FCM\QuestionBundle\Entity\PerguntaItem $perguntaItem = null)
    {
        $this->perguntaItem = $perguntaItem;

        return $this;
    }

    /**
     * Get perguntaItem
     *
     * @return \FCM\QuestionBundle\Entity\PerguntaItem
     */
    public function getPerguntaItem()
    {
        return $this->perguntaItem;
    }

    /**
     * @return bool
     */
    public function validateCPEM()
    {


        if($this->getResposta()->getQuestionario()->getEventoAtividade()->getEvento()->getId() == 135
            and $this->getPergunta()->getId() == 96){
            /** @var RespostaItem $respostaItem */

            if(substr($this->getText(),0,11) == 'Sócio ABEM'){
                $url = 'http://abem-educmed.org.br/api-status/api.php?cpf=' . $this->getResposta()->getInscricao()->getParticipante()->getDocumento();

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                $result = curl_exec($ch);
                $result = json_decode($result);


                if($result->status != 'Ativo'){
                    return false;
                }
            }
        }
        return true;
    }
}
