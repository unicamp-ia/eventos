<?php

namespace FCM\QuestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FCM\EventoBundle\Entity\EventoAtividade;

/**
 * Questionario
 *
 * @ORM\Table(name="questionario")
 * @ORM\Entity(repositoryClass="FCM\QuestionBundle\Repository\QuestionarioRepository")

 */
class Questionario
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\FCM\EventoBundle\Entity\EventoAtividade", inversedBy="questionarios")
     * @ORM\JoinColumn(name="evento_atividade_id", referencedColumnName="id")
     */
    private $eventoAtividade;


    public function __construct()
    {
        $this->perguntas = new ArrayCollection();
        $this->respostas = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="Pergunta", mappedBy="questionario", cascade={"remove","persist"})
     * @ORM\OrderBy({"delta" = "asc"})
     */
    private $perguntas;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=1000, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_exibicao", type="string", length=1000, nullable=true)
     */
    private $nomeExibicao;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", nullable=true)
     */
    private $descricao;

    const TYPE_PARTICIPANTE = 0;
    const TYPE_AVALIADOR = 1;
    const TYPE_FEEDBACK = 2;

    /**
     * @var int
     * 0 - Participante ao increver
     * 1 - Avaliador, ao ver trabalho
     * 2 - Participante ao final do evento
     * @ORM\Column(name="type", type="smallint", nullable=false)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ini_avaliacao", type="datetime", nullable=true)
     */
    private $dataIniAvaliacao;

    /**
     * Indica se o feedback da atividade vai ser obrigatorio
     * @var int
     *
     * @ORM\Column(name="feedback", type="boolean", nullable=true)
     */
    private $feedback;

    /**
     * Indica se o feedback da atividade vai ser obrigatorio
     * @var int
     *
     * @ORM\Column(name="deposito", type="boolean", nullable=false)
     */
    private $deposito;

    /**
     * Se true indica que deve ser aprovado
     * @var int
     *
     * @ORM\Column(name="aprove_required", type="boolean", nullable=false)
     */
    private $aproveRequired = false;





    /**
     * @return int
     */
    public function getDeposito()
    {
        return $this->deposito;

    }

    /**
     * @param int $deposito
     */
    public function setDeposito($deposito)
    {
        $this->deposito = $deposito;
    }

    /**
     * @return boolean
     */
    public function getFeedback()
    {
        return $this->feedback == 0 ? false : true;
    }

    /**
     * @param int $feedback
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * @return \DateTime
     */
    public function getDataIniAvaliacao()
    {
        return $this->dataIniAvaliacao;
    }

    /**
     * @param \DateTime $dataIniAvaliacao
     */
    public function setDataIniAvaliacao($dataIniAvaliacao)
    {
        $this->dataIniAvaliacao = $dataIniAvaliacao;
    }

    /**
     * @return \DateTime
     */
    public function getDataFimAvaliacao()
    {
        return $this->dataFimAvaliacao;
    }

    /**
     * @param \DateTime $dataFimAvaliacao
     */
    public function setDataFimAvaliacao($dataFimAvaliacao)
    {
        $this->dataFimAvaliacao = $dataFimAvaliacao;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax($max)
    {
        $this->max = $max;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_fim_avaliacao", type="datetime", nullable=true)
     */
    private $dataFimAvaliacao;

    /**
     * @var int
     * @todo: validar numero maximo de inscricoes
     * type 0: numero maximo de inscrições
     * type 1: numero maximo de avaliacoes
     * @todo: validar numero maximo de feedbacks
     * type 2: numero maximo de feedbacks
     * @ORM\Column(name="max", type="integer", nullable=true)
     */
    private $max;

    /**
     * @ORM\OneToMany(targetEntity="Resposta", mappedBy="questionario", cascade={"remove"})
     */
    private $respostas;

    /**
     * Orientacao exibida no ato da inscricao
     * @var string
     *
     * @ORM\Column(name="orientacao_inscricao", type="text", nullable=true)
     */
    private $orietacaoInscricao;

    /**
     * Orientacao enviada ao e-mail no ato da inscricao
     * @var string
     *
     * @ORM\Column(name="orientacao_confimacao", type="text", nullable=true)
     */
    private $orientacaoConfirmacao;

    /**
     * Orentacao enviada qando confirmado pagamento
     * @var string
     *
     * @ORM\Column(name="orientacao_pagamento", type="text", nullable=true)
     */
    private $orientacaoPagamento;

    /**
     * Orentacao enviada qando confirmado pagamento
     * @var string
     *
     * @ORM\Column(name="orientacao_aprovado", type="text", nullable=true)
     */
    private $orientacaoAprovado;


    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }



    /**
     * @return array
     */
    public static function getTypeChoices(){
        return [
            0 => 'Participante ao se increver',
            1 => 'Avaliador ao ver trabalho',
            2 => 'Participante ao final do evento',

        ];
    }

    /**
     * Retorna opçoes para o feedback do form
     * @return array
     */
    public static function getFeedbackChoices(){
        return [
            0 => 'Não, o participante pode emitir o certificado sem o questionário',
            1 => 'Sim, é necessário responder para emitir o certificado',
        ];
    }

    /**
     * @return string
     */
    public function getTypeLabel(){
        switch ($this->getType()){
            case 0: return 'Participante ao se increver'; break;
            case 1: return 'Avaliador, ao ver trabalho'; break;
            case 2: return 'Participante ao final do evento'; break;
        }
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Questionario
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set nomeExibicao
     *
     * @param string $nomeExibicao
     *
     * @return Questionario
     */
    public function setNomeExibicao($nomeExibicao)
    {
        $this->nomeExibicao = $nomeExibicao;

        return $this;
    }

    /**
     * Get nomeExibicao
     *
     * @return string
     */
    public function getNomeExibicao()
    {
        return $this->nomeExibicao;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Questionario
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }



    /**
     * Set eventoAtividade
     *
     * @param \FCM\Eventobundle\EventoAtividade $eventoAtividade
     *
     * @return Questionario
     */
    public function setEventoAtividade(EventoAtividade $eventoAtividade = null)
    {
        $this->eventoAtividade = $eventoAtividade;

        return $this;
    }

    /**
     * Get eventoAtividade
     *
     * @return EventoAtividade
     */
    public function getEventoAtividade()
    {
        return $this->eventoAtividade;
    }

    /**
     * Add pergunta
     *
     * @param \FCM\QuestionBundle\Entity\Pergunta $pergunta
     *
     * @return Questionario
     */
    public function addPergunta(\FCM\QuestionBundle\Entity\Pergunta $pergunta)
    {
        $this->perguntas[] = $pergunta;

        return $this;
    }

    /**
     * Remove pergunta
     *
     * @param \FCM\QuestionBundle\Entity\Pergunta $pergunta
     */
    public function removePergunta(\FCM\QuestionBundle\Entity\Pergunta $pergunta)
    {
        $this->perguntas->removeElement($pergunta);
    }

    /**
     * Get perguntas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPerguntas()
    {
        return $this->perguntas;
    }

    /**
     * Add resposta
     *
     * @param \FCM\QuestionBundle\Entity\Resposta $resposta
     *
     * @return Questionario
     */
    public function addResposta(\FCM\QuestionBundle\Entity\Resposta $resposta)
    {
        $this->respostas[] = $resposta;

        return $this;
    }

    /**
     * Remove resposta
     *
     * @param \FCM\QuestionBundle\Entity\Resposta $resposta
     */
    public function removeResposta(\FCM\QuestionBundle\Entity\Resposta $resposta)
    {
        $this->respostas->removeElement($resposta);
    }

    /**
     * Get respostas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRespostas()
    {
        return $this->respostas;
    }

    /**
     * Set orietacaoInscricao
     *
     * @param string $orietacaoInscricao
     *
     * @return Pergunta
     */
    public function setOrietacaoInscricao($orietacaoInscricao)
    {
        $this->orietacaoInscricao = $orietacaoInscricao;

        return $this;
    }

    /**
     * Get orietacaoInscricao
     *
     * @return string
     */
    public function getOrietacaoInscricao()
    {
        return $this->orietacaoInscricao;
    }

    /**
     * Set orietacaoConfirmacao
     *
     * @param string $orientacaoConfirmacao
     *
     * @return Pergunta
     */
    public function setOrientacaoConfirmacao($orientacaoConfirmacao)
    {
        $this->orientacaoConfirmacao = $orientacaoConfirmacao;

        return $this;
    }

    /**
     * Get orietacaoConfirmacao
     *
     * @return string
     */
    public function getOrientacaoConfirmacao()
    {
        return $this->orientacaoConfirmacao;
    }

    /**
     * Set orietacaoPagamento
     *
     * @param string $orientacaoPagamento
     *
     * @return Pergunta
     */
    public function setOrientacaoPagamento($orientacaoPagamento)
    {
        $this->orientacaoPagamento = $orientacaoPagamento;

        return $this;
    }

    /**
     * Get orietacaoPagamento
     *
     * @return string
     */
    public function getOrientacaoPagamento()
    {
        return $this->orientacaoPagamento;
    }

    /**
     * @return string
     */
    public function getOrientacaoAprovado()
    {
        return $this->orientacaoAprovado;
    }

    /**
     * @param string $orientacaoAprovado
     */
    public function setOrientacaoAprovado($orientacaoAprovado)
    {
        $this->orientacaoAprovado = $orientacaoAprovado;
    }

    /**
     * @return int
     */
    public function getAproveRequired()
    {
        return $this->aproveRequired;
    }

    /**
     * @param int $aproveRequired
     */
    public function setAproveRequired($aproveRequired)
    {
        $this->aproveRequired = $aproveRequired;
    }

}
