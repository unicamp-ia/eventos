<?php

namespace FCM\QuestionBundle\Controller;

use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\Avaliacao;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\QuestionBundle\Controller\answerFeedback;
use FCM\QuestionBundle\Entity\Pergunta;
use FCM\QuestionBundle\Entity\Questionario;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\QuestionBundle\Entity\RespostaItem;
use FCM\QuestionBundle\Form\RespostaType;
use FCM\UserBundle\Entity\User;
USE Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Console\Input\Input;

/**
 * Respostum controller.
 *
 * @Route("questionario/{questionario}")
 */
class RespostaController extends Controller
{


    /**
     * Procura participante para inscricao manual
     *
     * @Route("/ajax/resposta/{resposta}", name="ajax_question_resposta_approve", options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function ajaxApproveAction(Request $request, Resposta $resposta){

        $em = $this->getDoctrine()->getManager();

        if($resposta->isApproved()) $resposta->setApproved(false);
        else {
            $resposta->setApproved(true);
            $this->sendApprove($resposta);
        }

        $em->persist($resposta);
        $em->flush();

        $result = $this->render('FCMQuestionBundle:Resposta:ajax.approve.html.twig', [
            'resposta' => $resposta,
            'showLabel' => $request->get('showLabel'),
        ]);

        return new JsonResponse($result->getContent());

    }

    /**
     * Envia e-mail quando um trabalho eh aprovado
     * @param Resposta $resposta
     */
    public function sendApprove(Resposta $resposta){
        $em = $this->getDoctrine()->getManager();

        $questionarioAvaliacao = $em->getRepository('FCMQuestionBundle:Questionario')->findOneBy([
            'eventoAtividade' => $resposta->getInscricao()->getEventoAtividade()->getId(),
            'type' => 1
            ]);

        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Eventos] ' . $resposta->getInscricao()->getEventoAtividade()->getEvento()->getNome(). ' -  '.
                $resposta->getInscricao()->getEventoAtividade()->getNome() . ' Trabalho aprovado')
            ->setFrom($this->getParameter('mail'))
            ->setTo($resposta->getInscricao()->getParticipante()->getEmail())
            ->setReplyTo($resposta->getInscricao()->getEventoAtividade()->getEvento()->getEmail())
            ->setCc($resposta->getInscricao()->getEventoAtividade()->getEvento()->getEmail())
            ->setBody(
                $this->renderView('FCMQuestionBundle:Emails:approve.html.twig',[
                    'questionarioAvaliacao' => $questionarioAvaliacao,
                    'resposta' => $resposta
                ]),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }

    /**
     * Lists all respostum entities.
     *
     * @Route("/", name="questionario_resposta_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, Questionario $questionario)
    {
        $em = $this->getDoctrine()->getManager();

        $respostas = $em->getRepository('FCMQuestionBundle:Resposta')->findAll();

        return $this->render('FCMQuestionBundle:Resposta:edit.html.twig', array(
            'questionario' => $questionario,
            'respostas' => $respostas,
        ));
    }

    /**
     * Mostra as resposta de um questionário
     *
     * @Route("/respostas", name="questionario_respostas_render")
     * @Method("GET")
     */
    public function respostasShow(Request $request, Questionario $questionario)
    {
        $em = $this->getDoctrine()->getManager();

        $perguntas = $questionario->getPerguntas()->getValues();
        $respostas = $questionario->getRespostas()->getValues();

        return $this->render('FCMQuestionBundle:Feedback:index.html.twig', array(
            'questionario' => $questionario,
            'perguntas' => $perguntas,
        ));
    }

    /**
     * Lista os trabalhos para avaliacao
     *
     * @Route("/avaliacao", name="questionario_avaliacao")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function avaliacaoAction(Request $request, Questionario $questionario)
    {
        $eventoAtividade = $questionario->getEventoAtividade();

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();


        return $this->render('FCMQuestionBundle:Resposta:avaliacao.html.twig', array(
            'page_title' => 'Avaliar ' . $eventoAtividade->getNome(),
            'eventoAtividade' => $eventoAtividade,
            'admin' => $eventoAtividade->getEvento()->isAdmin($user),
            'questionario' => $eventoAtividade->getQuestionarioAvaliador(),
            'submissoes' => $eventoAtividade->getQuestionarioInscricao(),
            'back' => empty($request->get('back')) ? $this->generateUrl('admin') : $request->get('back')

        ));
    }

    /**
     * @Route("/export", name="questionario_export")
     * @param Questionario $questionario
     */
    public function printTrabalhosAction(Questionario $questionario){
        /** @var \PHPExcel $phpExcelObject */
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        /** @var EventoAtividade $eventoAtividade */
        $eventoAtividade = $questionario->getEventoAtividade();

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1',$eventoAtividade->getEvento()->getNome())
            ->setCellValue('A2',$eventoAtividade->getNome())
            ->setCellValue('A4','Trabalho')
            ->setCellValue('B4','Nome')
            ->setCellValue('C4','E-mail');

        $nextCell = 'C';


        /**
         * Preenche coluna das perguntas
         * @var Questionario $questionario
         */
        if($questionario = $eventoAtividade->getQuestionarioAvaliador()){
            /** @var Pergunta $pergunta */
            foreach($questionario->getPerguntas() as $pergunta){
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue(++$nextCell.'4',$pergunta->getNome());
            }
        }

        $row = 5;

        /** @var Inscricao $inscricao */
        foreach($eventoAtividade->getInscricoes() as $inscricao){
            $nextCell = 'A';

            $phpExcelObject->getActiveSheet()->setCellValue($nextCell.$row,$inscricao->getParticipante()->getDocumento());


            $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell.$row,$inscricao->getParticipante()->getNome());
            $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell.$row,$inscricao->getParticipante()->getEmail());
            /** @var InscricaoToken $inscricaoToken */
            foreach($inscricao->getInscricaoTokens() as $inscricaoToken){
                $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell.$row,$inscricaoToken->getVal());
            }


            /** @var Resposta $resposta */
            foreach($inscricao->getRespostaType(1) as $resposta){

                /** @var RespostaItem $respostaItem */
                foreach($resposta->getRespostaItens() as $respostaItem){
                    $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell.$row, $respostaItem->getText());
                }
            }

            $row++;
        }

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'lista-de-presenca.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;

    }

    /**
     * Creates a new respostum entity.
     *
     * @Route("/inscricao/{inscricao}/resposta/new", name="questionario_inscricao_resposta_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Questionario $questionario, Inscricao $inscricao)
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if($questionario->getType() == 1){
            $back = $this->generateUrl('questionario_avaliacao', ['questionario' => $questionario->getId(),]);
            $respostas = $inscricao->getRespostaType(Questionario::TYPE_AVALIADOR);
            /** @var Resposta $resposta */
            foreach ($respostas as $resposta){
                if($resposta->getCreatedBy()->getUsername() == $user->getUsername()){
                    $this->addFlash('success','Avaliação já preenchida');
                    return new RedirectResponse($back);
                }
            }
        }


        $resposta = new Resposta();
        $resposta->setQuestionario($questionario);
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        foreach($questionario->getPerguntas() as $pergunta){
            $ri = new RespostaItem();
            $ri->setResposta($resposta);
            $ri->setPergunta($pergunta);
            $resposta->addRespostaIten($ri);
        }

        $form = $this->createForm(RespostaType::class, $resposta,['submit' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $resposta->setCreated(new \DateTime());
            $resposta->setCreatedBy($user = $this->get('security.token_storage')->getToken()->getUser());
            $resposta->setInscricao($inscricao);

            /** @var RespostaItem $respostaItem */
            foreach($resposta->getRespostaItens() as $respostaItem){
                if(!$respostaItem->getFile()) $resposta->removeRespostaIten($respostaItem);
            }

            foreach($request->get('resposta')['respostaItens'] as $item){
                foreach($item as $perguntaId => $resp){
                    /** @var Pergunta $pergunta */
                    $pergunta = $em->getRepository('FCMQuestionBundle:Pergunta')->find($perguntaId);

                    if($pergunta->getType() == 'Symfony\Component\Form\Extension\Core\Type\CheckBoxType'){
                        foreach($resp as $resp_option){
                            $ri = new RespostaItem();
                            $ri->setPergunta($pergunta);
                            $ri->setResposta($resposta);
                            $ri->setText($resp_option);
                            $resposta->addRespostaIten($ri);
                            $em->persist($ri);
                        }
                    } else {
                        $ri = new RespostaItem();
                        $ri->setPergunta($pergunta);
                        $ri->setResposta($resposta);
                        $ri->setText($resp);
                        $resposta->addRespostaIten($ri);
                        $em->persist($ri);
                    }
                }
            }

            $em->persist($resposta);

            $this->addFlash('success','Preenchimento efetuado com sucesso');
            $em->flush($resposta);

            return new RedirectResponse($back);
        }

        return $this->render('FCMQuestionBundle:Resposta:edit.html.twig', array(
            'page_title' => $questionario->getNome(),
            'page_title_desc' => $questionario->getEventoAtividade()->getEvento()->getNome() . ' - '
                . $questionario->getEventoAtividade()->getNome(),
            'questionario' => $questionario,
            'inscricao' => $inscricao,
            'resposta' => $resposta,
            'form' => $form->createView(),
            'back' => $back
        ));
    }

    /**
     * Finds and displays a respostum entity.
     *
     * @Route("/inscricao/{inscricao}/resposta/show", name="questionario_inscricao_resposta_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Questionario $questionario, Inscricao $inscricao)
    {
        if($questionario->getType() == 1){
            $back = $this->generateUrl('questionario_avaliacao', ['questionario' => $questionario->getId(),]);
        } else {
            $back = $this->generateUrl('home');
        }

        return $this->render('FCMQuestionBundle:Resposta:show.html.twig', array(
            'page_title' => $inscricao->getEventoAtividade()->getEvento()->getNome()
                . ' - ' . $inscricao->getEventoAtividade()->getNome(),
            'questionario' => $questionario,
            'inscricao' => $inscricao,
            'back' => $back
        ));
    }

    /**
     * Finds and displays a respostum entity.
     *
     * @Route("/inscricao/{inscricao}/avaliacao/show", name="questionario_inscricao_avaliacao_show")
     * @Method("GET")
     */
    public function showAvalicaoAction(Request $request, Questionario $questionario, Inscricao $inscricao)
    {
        $back = $this->generateUrl('questionario_avaliacao', ['questionario' => $questionario->getId(),]);

        return $this->render('FCMQuestionBundle:Resposta:show.avaliacao.html.twig', array(
            'page_title' => $inscricao->getEventoAtividade()->getEvento()->getNome()
                . ' - ' . $inscricao->getEventoAtividade()->getNome(),
            'questionario' => $questionario,
            'resposta' => $inscricao->getRespostaType(0)->first(),
            'inscricao' => $inscricao,
            'back' => $back
        ));
    }



    /**
     * Mostra o formulario de resposta de feedback pro usuario
     *
     * @Route("/inscricao/{inscricao}/resposta", name="questionario_user_resposta")
     * @Method({"GET", "POST"})
     */
    public function answerAction(Request $request, Questionario $questionario, Inscricao $inscricao)
    {
        $resposta = new Resposta();

        foreach($questionario->getPerguntas() as $pergunta){
             $ri = new RespostaItem();
             $ri->setResposta($resposta);
             $ri->setPergunta($pergunta);
             $resposta->addRespostaIten($ri);
        }

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm('FCM\QuestionBundle\Form\RespostaType', $resposta);

        $form->handleRequest($request);

        if($form->isSubmitted()){
            $resposta = new Resposta();

            $resposta->setCreated(new \DateTime());
            $resposta->setQuestionario($questionario);
            $resposta->setCreatedBy($user = $this->get('security.token_storage')->getToken()->getUser());
            $resposta->setInscricao($inscricao);

            //Upload de arquivos
            if( !empty($request->files->get('resposta')['respostaItens']) )
                foreach($request->files->get('resposta')['respostaItens'] as $perguntaId => $item){
                    foreach($item as $resp){
                        /** @var Pergunta $pergunta */
                        $pergunta = $em->getRepository('FCMQuestionBundle:Pergunta')->find($perguntaId);

                        $ri->setPergunta($pergunta);
                        $ri->setResposta($resposta);
                        $ri->setFile($resp['file']);
                        $resposta->addRespostaIten($ri);
                        $em->persist($ri);
                    }
                }

            foreach($request->get('resposta')['respostaItens'] as $perguntaId => $item){
                foreach($item as $perguntaId => $resp){

                    if($perguntaId == 'is_anonimo')
                        break;

                    /** @var Pergunta $pergunta */
                    $pergunta = $em->getRepository('FCMQuestionBundle:Pergunta')->find($perguntaId);

                    if($pergunta->getType() == 'Symfony\Component\Form\Extension\Core\Type\CheckBoxType'){
                        foreach($resp as $resp_option){
                            $ri = new RespostaItem();
                            $ri->setPergunta($pergunta);
                            $ri->setResposta($resposta);
                            $ri->setText($resp_option);
                            $ri->setIsAnonimo(isset($item['is_anonimo']) ? $item['is_anonimo'] : 0);
                            $resposta->addRespostaIten($ri);
                            $em->persist($ri);
                        }
                    } else {
                        $ri = new RespostaItem();
                        $ri->setPergunta($pergunta);
                        $ri->setResposta($resposta);
                        $ri->setText($resp);
                        $ri->setIsAnonimo(isset($item['is_anonimo']) ? $item['is_anonimo'] : 0);
                        $resposta->addRespostaIten($ri);
                        $em->persist($ri);
                    }
                }
            }
            $em->persist($resposta);
            $em->flush($resposta);

            $this->addFlash('success','Questionário de feedback da atividade '. $questionario->getEventoAtividade()->getNome() .' respondido!');

            //Após responder o questionário, redireiona o usuário para a pagina inicial
            return $this->redirect($this->generateUrl("home"));
        }

        return $this->render('FCMQuestionBundle:Resposta:answer.html.twig', array(
            'page_title' => 'Questionário: ' . $questionario->getNome(),
            'page_title_desc' => 'Questionário de feedback | ' . $questionario->getEventoAtividade()->getEvento()->getNome() . ' - '
            . $questionario->getEventoAtividade()->getNome(),
            'form' => $form->createView(),
            'back' => $this->generateUrl('home')
            )
        );
    }
}
