<?php

namespace FCM\QuestionBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\QuestionBundle\Entity\Pergunta;
use FCM\QuestionBundle\Entity\PerguntaItem;
use FCM\QuestionBundle\Entity\Questionario;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\QuestionBundle\Entity\RespostaItem;
use FCM\QuestionBundle\Form\PerguntaType;
use FCM\QuestionBundle\Form\QuestionarioFillType;
use FCM\QuestionBundle\Form\QuestionarioType;
use FCM\QuestionBundle\Form\RespostaType;
use FCM\QuestionBundle\Repository\QuestionarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Questionario controller.
 *
 * @Route("")
 */
class PerguntaController extends Controller
{



    /**
     * Creates a new questionario entity.
     *
     * @Route("admin/eventoAtividade/{eventoAtividade}/questionario/{questionario}/pergunta/new", name="admin_evento_atividade_questionario_pergunta_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, EventoAtividade $eventoAtividade, Questionario $questionario)
    {
        $pergunta = new Pergunta();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(PerguntaType::class, $pergunta,[
            'hasAvaliacao' => $eventoAtividade->getQuestionarioAvaliador(),
        ]);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $pergunta->setQuestionario($questionario);

            /** @var PerguntaItem $iten */
            foreach ($pergunta->getPerguntaItens() as $iten){
                $iten->setPergunta($pergunta);
                $em->persist($iten);
            }

            $em->persist($pergunta);

            $em->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso');

            return $this->redirectToRoute('admin_evento_atividade_questionario_edit', array(
                'evento' => $eventoAtividade->getEvento()->getId(),
                'eventoAtividade' => $eventoAtividade->getId(),
                'id' => $questionario->getId(),
            ));
        }

        return $this->render('FCMQuestionBundle:Pergunta:edit.html.twig', array(
            'page_title' => 'Incluir questionário do tipo ' . $questionario->getTypeLabel(),

            'back' => $this->generateUrl('admin_evento_atividade_questionario_edit', [
                'evento' => $eventoAtividade->getEvento()->getId(),
                'eventoAtividade' => $eventoAtividade->getId(),
                'id' => $questionario->getId(),
            ]),
            'questionario' => $questionario,
            'form' => $form->createView(),
        ));
    }

    /**
     * Exclui pergunta
     *
     * @Route("admin/eventoAtividade/{eventoAtividade}/questionario/{questionario}/pergunta/{id}/delete", name="admin_evento_atividade_questionario_pergunta_delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request, Questionario $questionario, EventoAtividade $eventoAtividade,
                               Pergunta $pergunta)
    {

        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($pergunta);

            $em->flush();

            $this->addFlash('success','Exclusão efetuada com sucesso');

            return $this->redirectToRoute('admin_evento_atividade_questionario_edit', array(
                'evento' => $questionario->getEventoAtividade()->getEvento()->getId(),
                'eventoAtividade' => $eventoAtividade->getId(),
                'id' => $questionario->getId(),
            ));
        }


        return $this->render('FCMQuestionBundle:Pergunta:delete.html.twig', array(
            'pergunta' => $pergunta,
            'form' => $form->createView(),
            'cancel_route' => $this->generateUrl('admin_evento_atividade_questionario_edit', array(
                'evento' => $questionario->getEventoAtividade()->getEvento()->getId(),
                'eventoAtividade' => $eventoAtividade->getId(),
                'id' => $questionario->getId(),
            )),
        ));

    }


    /**
     * Displays a form to edit an existing questionario entity.
     *
     * @Route("admin/eventoAtividade/{eventoAtividade}/questionario/{questionario}/pergunta/{id}/edit", name="admin_evento_atividade_questionario_pergunta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Questionario $questionario, EventoAtividade $eventoAtividade,
                               Pergunta $pergunta)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(PerguntaType::class, $pergunta,[
          'hasAvaliacao' => $eventoAtividade->getQuestionarioAvaliador(),
        ]);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            /** @var PerguntaItem $iten */
            foreach ($pergunta->getPerguntaItens() as $iten){
                $iten->setPergunta($pergunta);
                $em->persist($iten);
            }

            $em->persist($pergunta);

            $this->addFlash('success','Alteração efetuada com sucesso');

            $em->flush();

            return $this->redirectToRoute('admin_evento_atividade_questionario_edit', array(
                'id' => $questionario->getId(),
                'eventoAtividade' => $eventoAtividade->getId(),
                'evento' => $eventoAtividade->getEvento()->getId(),
                ));

        }

        return $this->render('FCMQuestionBundle:Pergunta:edit.html.twig', array(
            'questionario' => $questionario,
            'page_title' => 'Editar pergunta: ' . $pergunta->getNome(),
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_evento_atividade_questionario_edit', [
                        'id' => $questionario->getId(),
                        'eventoAtividade' => $eventoAtividade->getId(),
                        'evento' => $eventoAtividade->getEvento()->getId(),
            ]),
        ));
    }
}
