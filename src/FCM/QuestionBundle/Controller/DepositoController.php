<?php

namespace FCM\QuestionBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\QuestionBundle\Entity\Pergunta;
use FCM\QuestionBundle\Entity\PerguntaItem;
use FCM\QuestionBundle\Entity\Questionario;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\QuestionBundle\Entity\RespostaItem;
use FCM\QuestionBundle\Form\QuestionarioFillType;
use FCM\QuestionBundle\Form\QuestionarioType;
use FCM\QuestionBundle\Form\RespostaType;
use FCM\QuestionBundle\Repository\QuestionarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Questionario controller.
 *
 */
class DepositoController extends Controller
{

    /**
     *
     *
     * @Route("/admin/evento/{evento}/eventoAtividade/{eventoAtividade}/deposito", name="admin_evento_evento_atividade_deposito_index")
     * @Method("GET")
     */
    public function indexAction(EventoAtividade $eventoAtividade)
    {
        return $this->render('FCMQuestionBundle:Deposito:index.html.twig',[
            'page_title' => 'Depósitos',
            'page_title_desc' => 'desc',
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'inscricoes' => $eventoAtividade->getInscricoes(),
            'presentes' => 0,
            'questionarios' => null,
            'back' => $this->generateUrl('admin_evento_atividade_edit',[
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId(),
            ])
        ]);
    }

    /**
     *
     *
     * @Route("/evento/{evento}/eventoAtividade/{eventoAtividade}/{inscricao}/deposito", name="admin_evento_evento_atividade_inscricao_deposito_edit")
     * @Method("GET")
     */
    public function editAction(EventoAtividade $eventoAtividade, Inscricao $inscricao)
    {
        return $this->render('FCMQuestionBundle:Deposito:edit.html.twig',[
            'page_title' => 'Depósito',
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'inscricao' => $inscricao,

        ]);
    }
}