<?php

namespace FCM\QuestionBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\QuestionBundle\Entity\Pergunta;
use FCM\QuestionBundle\Entity\PerguntaItem;
use FCM\QuestionBundle\Entity\Questionario;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\QuestionBundle\Entity\RespostaItem;
use FCM\QuestionBundle\Form\QuestionarioFillType;
use FCM\QuestionBundle\Form\QuestionarioType;
use FCM\QuestionBundle\Form\RespostaType;
use FCM\QuestionBundle\Repository\QuestionarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Questionario controller.
 *
 * @Route("admin/eventoAtividade/{eventoAtividade}/questionario")
 */
class QuestionarioController extends Controller
{

    /**
     * Lists all questionario entities.
     *
     * @Route("/", name="admin_evento_atividade_questionario_index")
     * @Method("GET")
     */
    public function indexAction(EventoAtividade $eventoAtividade)
    {
        $action_links = array();

        $em = $this->getDoctrine()->getManager();

        /** @var QuestionarioRepository $q_repo */
        $q_repo = $em->getRepository('FCMQuestionBundle:Questionario');

        $questionarios = $q_repo->findBy(['eventoAtividade' => $eventoAtividade->getId()]);


        /** @var Questionario $questionario */
        if(!$eventoAtividade->getQuestionarioInscricao()){
            $action_links[] = [
                'url' => $this->generateUrl('admin_evento_atividade_questionario_new', [
                    'eventoAtividade' => $eventoAtividade->getId(), 'type' => 0]),
                'label' => 'Incluir Questionário no ato da inscrição'
            ];
        }

        /** @var Questionario $questionario */
        if(!$eventoAtividade->getQuestionarioAvaliador()){
            $action_links[] = [
                'url' => $this->generateUrl('admin_evento_atividade_questionario_new', [
                    'eventoAtividade' => $eventoAtividade->getId(), 'type' => 1]),
                'label' => 'Incluir questionário para avaliação de projetos',
            ];
        }

        /** @var Questionario $questionario */
        if(!$eventoAtividade->getQuestionarioPosInscricao()){
            $action_links[] = [
                'url' => $this->generateUrl('admin_evento_atividade_questionario_new', [
                    'eventoAtividade' => $eventoAtividade->getId(), 'type' => 2]),
                'label' => 'Incluir questionário após a atividade',
            ];
        }

        return $this->render('FCMQuestionBundle:Questionario:index.html.twig', array(
            'page_title' => 'Questionários',
            'page_title_desc' => $eventoAtividade->getEvento()->getNome() . ' - ' . $eventoAtividade->getNome(),
            'questionarios' => $questionarios,
            'action_links' => $action_links,
            'back' => $this->generateUrl('admin_evento_atividade_edit',[
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId()
            ])
        ));
    }

    /**
     * Creates a new questionario entity.
     *
     * @Route("/new", name="admin_evento_atividade_questionario_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, EventoAtividade $eventoAtividade)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $questionario = new Questionario();
        $questionario->setMax(1);


        // Define type if defined
        if($request->query->has('type')) $questionario->setType($request->query->get('type'));

        $questionario->setEventoAtividade($eventoAtividade);
        $questionario->setDeposito(false);
        $em->persist($questionario);
        $em->flush();

        $form = $this->createForm('FCM\QuestionBundle\Form\QuestionarioType', $questionario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            /** @var Pergunta $pergunta */
            foreach($questionario->getPerguntas() as $pergunta){
                $pergunta->setQuestionario($questionario);

                /** @var PerguntaItem $iten */
                foreach ($pergunta->getPerguntaItens() as $iten){
                    $iten->setPergunta($pergunta);
                    $em->persist($iten);
                }

                $em->persist($pergunta);
            }

            $questionario->setEventoAtividade($eventoAtividade);
            $em->persist($questionario);
            $em->flush($questionario);

            $this->addFlash('success','Inclusão efetuada com sucesso');

            return $this->redirectToRoute('admin_evento_atividade_edit', array(
                'evento' => $questionario->getEventoAtividade()->getEvento()->getId(),
                'id' => $questionario->getEventoAtividade()->getId(),
            ));
        }

        return $this->render('FCMQuestionBundle:Questionario:edit.html.twig', array(
            'page_title' => 'Incluir questionário do tipo ' . $questionario->getTypeLabel(),
            'page_title_desc' => $eventoAtividade->getEvento()->getNome() . ' - ' . $eventoAtividade->getNome(),
            'back' => $this->generateUrl('admin_evento_atividade_edit', [
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId()
            ]),
            'questionario' => $questionario,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a questionario entity.
     *
     * @Route("/{id}", name="admin_evento_atividade_questionario_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Questionario $questionario)
    {
        $resposta = new Resposta();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        foreach($questionario->getPerguntas() as $pergunta){
            $ri = new RespostaItem();
            $ri->setResposta($resposta);
            $ri->setPergunta($pergunta);
            $resposta->addRespostaIten($ri);
        }

        $form = $this->createForm(RespostaType::class, $resposta,['submit' => false]);
        $form->handleRequest($request);

        return $this->render('FCMQuestionBundle:Questionario:show.html.twig', array(
            'q' => $questionario,
            'evento' => $questionario->getEventoAtividade()->getEvento(),
            'page_title' => 'Questionário: ' . $questionario->getNome(),
            'page_title_desc' => $questionario->getEventoAtividade()->getEvento()->getNome() . ' - '
            . $questionario->getEventoAtividade()->getNome(),
            'form' => $form->createView(),
            'action_links' => [
                'edit' => [
                    'url' => $this->generateUrl('admin_evento_atividade_questionario_edit', [
                        'eventoAtividade' => $questionario->getEventoAtividade()->getId(),
                        'id' => $questionario->getId(),
                    ]),
                    'label' => 'Editar'
                ]
            ],
            'back' => $this->generateUrl('admin_evento_atividade_questionario_index', [
                'eventoAtividade' => $questionario->getEventoAtividade()->getId()
            ])
        ));
    }

    /**
     * Displays a form to edit an existing questionario entity.
     *
     * @Route("/{id}/edit", name="admin_evento_atividade_questionario_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Questionario $questionario, EventoAtividade $eventoAtividade)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();


        $form = $this->createForm(QuestionarioType::class, $questionario,[
          'hasAvaliacao' => $eventoAtividade->getQuestionarioAvaliador(),
        ]);

        $perguntas_old = new ArrayCollection();
        foreach($questionario->getPerguntas() as $pergunta){
            $perguntas_old->add($pergunta);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var Pergunta $pergunta */
            foreach ($perguntas_old as $pergunta) {
                if(false === $questionario->getPerguntas()->contains($pergunta) ){
                    $questionario->getPerguntas()->removeElement($pergunta);
                    $em->persist($pergunta);
                    $em->remove($pergunta);

                }
            }

            /** @var Pergunta $pergunta */
            foreach($questionario->getPerguntas() as $pergunta){
                $pergunta->setQuestionario($questionario);

                /** @var PerguntaItem $iten */
                foreach ($pergunta->getPerguntaItens() as $iten){
                    $iten->setPergunta($pergunta);
                    $em->persist($iten);
                }

                $em->persist($pergunta);
            }

            $questionario->setEventoAtividade($eventoAtividade);

            $this->addFlash('success','Alteração efetuada com sucesso');


            $em->flush();

            return $this->redirectToRoute('admin_evento_atividade_edit', array(
                'evento' => $questionario->getEventoAtividade()->getEvento()->getId(),
                'id' => $questionario->getEventoAtividade()->getId()
                ));
        }

        $questionario_form = $this->getQuestionarioForm($eventoAtividade, $questionario);

        return $this->render('FCMQuestionBundle:Questionario:edit.html.twig', array(
            'questionario' => $questionario,
            'page_title' => 'Editar questionário: ' . $questionario->getTypeLabel(),
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'page_title_desc' => $questionario->getEventoAtividade()->getNome(),
            'form' => $form->createView(),
            'formQuestionario' => $questionario_form->createView(),
            'back' => $this->generateUrl('admin_evento_atividade_edit', [
                        'id' => $questionario->getEventoAtividade()->getId(),
                        'evento' => $eventoAtividade->getEvento()->getId(),
            ]),
        ));
    }

    /**
     * @param EventoAtividade $eventoAtividade
     * @param EntityManager $em
     * @return \Symfony\Component\Form\Form
     */
    protected function getQuestionarioForm(EventoAtividade $eventoAtividade, Questionario $questionario = null){
        $resposta = new Resposta();

        if(!$questionario){
            $questionario = $eventoAtividade->getQuestionarioInscricao();
        };

        if($questionario) {

            $resposta = new Resposta();

            // Verifica se cada pergunta ja possui uma resposta
            /** @var Pergunta $pergunta */
            foreach ($questionario->getPerguntas() as $pergunta) {
                if(!$ri = $this->getDoctrine()->getRepository('FCMQuestionBundle:RespostaItem')
                    ->findOneBy(['resposta' => $resposta->getId(),'pergunta' => $pergunta->getId()])){
                    $ri = new RespostaItem();
                    $ri->setResposta($resposta);
                    $ri->setPergunta($pergunta);
                    $resposta->addRespostaIten($ri);
                }
            }
        }

        $isAdmin = !$this->get('security.authorization_checker')->isGranted('ROLE_PARTICIPANTE');


        $formQuestionario = $this->createForm(RespostaType::class, $resposta, [
            'submit' => true,'deposito' => false, 'is_admin' => $isAdmin, 'translator' => $this->get('translator'),
        ]);
        return $formQuestionario;
    }

    /**
     * Faz o download de um file
     *
     * @Route("/download/{file}", name="download_questionario_file")
     * @Method({"GET", "POST"})
    */
    public function downloadAction($file)
    {
        $request = $this->get('request');

        $response = new Response();

        //set headers
        $response->headers->set('Content-Type', 'mime/type');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$file->getNome());

        $response->setContent($file);

        return $response;
    }
}
