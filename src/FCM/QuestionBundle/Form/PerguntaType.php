<?php

namespace FCM\QuestionBundle\Form;

use FCM\QuestionBundle\Entity\Pergunta;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PerguntaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class, [
                'label' => 'Título da pergunta',
                'required' => true,
            ])
            ->add('required', CheckboxType::class, [
                'label' => 'Obrigatório',
                'required' => false,
            ])

            ->add('checkApp', CheckboxType::class, [
                'label' => 'Checar item pelo app?',
                'required' => false,
            ])

            ->add('delta', NumberType::class, [
                'label' => 'Ordem',
                'required' => false,
                'data' => 0,
                'attr' => array(
                    'placeholder' => 'Insira um valor, quanto menor, a pergunta virá primeiro',
                    'class' => 'ordem'
                ),
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Tipo do campo',
                'choices' => array_flip(Pergunta::getTypeChoices()),
                'required' => true,
                'attr' => array('class' => 'select-tipo'),
            ])
            ->add('options', TextareaType::class, [
                'label' => 'Opções',
                'required' => false,
                'attr' => array('placeholder' => 'Insira as opções uma por linha sem espaços, você também pode inserir no modelo Inscricao|Certificado',
                    'class' => 'select-options'
                ),
            ])
            ->add('placeholder',TextType::class, [
                'label' => 'Texto de ajuda',
                'required' => false,
                'attr' => array('placeholder' => 'Insira um texto de ajuda como este'),
            ])
            ->add('perguntaItens', CollectionType::class,[
                'label' => 'Opções',
                'prototype' => true,
                'prototype_name' => 'perguntaItem',
                'entry_type' => PerguntaItemType::class,
                'allow_add'    => true,
                'allow_delete' => true,
                'attr' => [
                    'class' => 'perguntaitem-collection'
                ]
            ])
        ;

        if($options['type'] == 2){
          $builder->add('anonimo', CheckboxType::class, [
              'label' => 'Resposta anonima?',
              'required' => false,
          ]);
        }

        if($options['hasAvaliacao']){
              $builder->add('showAvaliacao', CheckboxType::class, [
                'label' => 'Exibir aos avaliadores',
                'required' => false,
          ]);
            $builder->add('showAvaliacaoLista', CheckboxType::class, [
                'label' => 'Exibir aos avaliadores na lista',
                'required' => false,
            ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\QuestionBundle\Entity\Pergunta',
            'type' => 0,
            'hasAvaliacao' => false,
        ));
    }

        public function getBlockPrefix()
        {
            return 'PerguntaType';
        }


}
