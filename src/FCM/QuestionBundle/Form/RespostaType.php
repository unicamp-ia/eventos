<?php

namespace FCM\QuestionBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class RespostaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('respostaItens', CollectionType::class, [
            'label' => 'Respostas',
            'entry_type' => RespostaItemType::class,
            'allow_add'    => false,
            'allow_delete' => false,
            'entry_options' => ['resposta' => $options['data'], 'isNew' => $options['isNew']],
            'by_reference' => false,
        ]);


        if($options['submit']) {
            $builder->add('submit', SubmitType::class, [
                'label' => 'Enviar',
                'attr' => array('class' => 'btn-primary')
            ]);
        }

        if($options['deposito']){
            /*
             * habilita deposito na edicao e
             * PARTICIPANTE: se estiver vazio OU nao pago
             * ADMIN: sempre habilitado
             */
            $builder->add('deposito',VichFileType::class,[
                'label' => 'Comprovante de depósito',
                'required' => false,

            ]);

            /* exibido quando ADMIN */
            if($options['is_admin']){
                $builder->add('pago',CheckboxType::class,[
                    'label' => 'Depósito confirmado?',
                    'required' => false,
                    'disabled' => !$options['is_admin']
                ]);
            }
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\QuestionBundle\Entity\Resposta',
            'submit' => true,
            'allow_extra_fields' => true,
            'deposito' => false,
            'is_admin' => false,
            'isNew' => false,
            'translator' => null,
        ));
    }
}