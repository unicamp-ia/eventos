<?php

namespace FCM\QuestionBundle\Form;

use FCM\QuestionBundle\Entity\Questionario;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionarioType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Questionario $qustionario */
        $qustionario = $options['data'];

        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'Quando o questionário é aplicado?',
                'choices' => array_flip(Questionario::getTypeChoices()),
                'required' => true,
                'disabled' => true,
            ]);


        switch($qustionario->getType()) {
            // Inscricao/Depósito
            case 0:
                $label_max = 'Número máximo de inscrições por participante';
                $builder->add('orientacao_pagamento', CKEditorType::class, [
                    'label' => 'Instrução após pagamento confirmado',
                    'required' => false,
                ]);
                $builder->add('deposito', CheckboxType::class, [
                    'label' => 'Habilitar campo para anexar comprovante de depósito?',
                    'required' => false,
                ])
                ->add('descricao', CKEditorType::class, [
                    'label' => 'Instrução durante a inscrição',
                    'required' => false,
                ])
                ->add('orientacao_confirmacao', CKEditorType::class, [
                    'required' => false,
                ]);
                break;
            // Apresentacao de trabalhos
            case 1:
                $builder->add('aproveRequired', CheckboxType::class, [
                    'label' => 'Trabahos necessitam ser aprovados',
                    'required' => false,
                ]);
                $builder->add('orientacaoAprovado', CKEditorType::class, [
                    'label' => 'Instrução enviada ao participante quando aprovado sua inscrição/trabalho',
                    'required' => false,
                ]);

                $label_max = 'Número máximo de avaliações que uma trabalho pode receber, deixe em branco para ILIMITADO';
                $builder->add('dataIniAvaliacao', DateTimeType::class, [
                    'label' => 'Data do início das avaliações',
                    'required' => false,
                    'attr' => array('class'=>'form_date_picker_date'),
                    'widget' => 'single_text',
                    'html5' => false,
                    'format' => 'dd/MM/yyyy',
                ])
                    ->add('dataFimAvaliacao', DateTimeType::class, [
                        'label' => 'Data final das avaliações',

                        'required' => false,
                        'attr' => array('class'=>'form_date_picker_date'),
                        'widget' => 'single_text',
                        'html5' => false,
                        'format' => 'dd/MM/yyyy',
                        ]);
                break;

            case 2:

                $label_max = 'Número máximo de avaliações do evento que um participante poderá fazer';
                $builder->add('feedback', ChoiceType::class, [
                    'label' => 'Questionário obrigatorio?',
                    'choices' => array_flip(Questionario::getFeedbackChoices()),
                    'required' => true,
                    'disabled' => false,
                ]);

                break;
        }

        //Opção de max não esta disponivel para questionario do tipo pós inscrição
        if ($qustionario->getType() != 2) {
            $builder->add('max', TextType::class, [
                'label' => $label_max,
                'required' => false,
            ]);
        }




    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\QuestionBundle\Entity\Questionario',
            'avaliacao' => false,
            'hasAvaliacao' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fcm_questionbundle_questionario';
    }


}
