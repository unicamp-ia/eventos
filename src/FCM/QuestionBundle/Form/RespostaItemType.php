<?php

namespace FCM\QuestionBundle\Form;

use FCM\QuestionBundle\Entity\Pergunta;
use FCM\QuestionBundle\Entity\Questionario;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\QuestionBundle\Entity\RespostaItem;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class RespostaItemType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Resposta $resposta */
        $resposta = $options['resposta'];

        /** @var RespostaItem $respostaItem */
        $respostaItem = $resposta->getRespostaItens()->get($builder->getName());

        $pergunta = $respostaItem->getPergunta();

        switch ($pergunta->getType()) {
            case 'Symfony\Component\Form\Extension\Core\Type\TextareaType':
                $builder->add(  $pergunta->getId(), CKEditorType::class, [
                    'label' => $pergunta->getNome(),
                    'required' => $pergunta->getRequired(),
                    'config_name' => 'basic',
                    'mapped' => false,
                    'attr' => array(
                        'placeholder' => $pergunta->getPlaceholder(),
                        'cols' => '5', 'rows' => '20'
                    ),
                    'data' => $respostaItem instanceof RespostaItem ? $respostaItem->getText() : '',
                ]);
                break;
            case 'Symfony\Component\Form\Extension\Core\Type\ChoiceType' :
                $choices = $pergunta->getOptionsPrepared(true, $options['isNew']);

                $builder->add($pergunta->getId(), $pergunta->getType(), [
                    'label' => $pergunta->getNome(),
                    'required' => $pergunta->getRequired(),
                    'mapped' => false,
                    'expanded' => true,
                    'attr' => array('placeholder' => $pergunta->getPlaceholder()),
                    'data' => $respostaItem instanceof RespostaItem ? $respostaItem->getText() : array(),
                    'choices' => array_flip($choices),
                    'placeholder' => 'Clique aqui para escolher uma opção'
                ]);
                break;
            case 'Symfony\Component\Form\Extension\Core\Type\CheckBoxType' :
                $choices = $pergunta->getOptionsPrepared(true,$options['isNew']);

                if($respostaItem instanceof RespostaItem){
                    $data = unserialize($respostaItem->getText());
                } else $data = array();


                $builder->add($pergunta->getId(), ChoiceType::class, [
                    'label' => $pergunta->getNome(),
                    'required' => $pergunta->getRequired(),
                    'mapped' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'attr' => array('placeholder' => $pergunta->getPlaceholder()),
                    'data' => $data ? $data : array(),
                    'choices' => array_flip($choices),
                ]);
                break;
            case 'Symfony\Component\Form\Extension\Core\Type\TextType' :
                $builder->add($pergunta->getId(), $pergunta->getType(), [
                    'label' => $pergunta->getNome(),
                    'required' => $pergunta->getRequired(),
                    'mapped' => false,
                    'attr' => array('placeholder' => $pergunta->getPlaceholder(),),
                    'data' => $respostaItem instanceof RespostaItem ? $respostaItem->getText() : '',

                ]);
                break;
            case 'Vich\UploaderBundle\Form\Type\VichFileType' :

                $builder->add('file', $pergunta->getType(), [
                    'mapped' => false,
                    'label' => $pergunta->getNome(),
                    'data_class' => null,
                    'required' => $pergunta->getRequired(),
                    'data' => $respostaItem instanceof RespostaItem ? $respostaItem->getFile() : null,
                ]);
                break;
            case 'Symfony\Component\Form\Extension\Core\Type\NumberType':
                $builder->add($pergunta->getId(), NumberType::class, [
                    'label' => $pergunta->getNome(),
                    'required' => $pergunta->getRequired(),
                    'mapped' => false,
                    'attr' => array('placeholder' => $pergunta->getPlaceholder(),),
                    'data' => $respostaItem instanceof RespostaItem ? $respostaItem->getText() : '',
                ]);
                break;
        }

        //Anonimamente
        if ($pergunta->getAnonimo()) {
            $builder->add('is_anonimo', 'checkbox', [
                'label' => 'Responder anonimamente?',
                'required' => false,
                'data' => false,
                'mapped' => false,
            ]);
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\QuestionBundle\Entity\RespostaItem',
            'resposta' => 'FCM\QuestionBundle\Entity\Resposta',
            'hideavaliacao' => true,
            'allow_extra_fields' => true,
            'isNew' => false,
        ));
    }


}
