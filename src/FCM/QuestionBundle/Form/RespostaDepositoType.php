<?php

namespace FCM\QuestionBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class RespostaDepositoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        if($options['submit']) {
            $builder->add('submit', SubmitType::class, [
                'label' => 'Enviar',
                'attr' => array('class' => 'btn-primary')
            ]);
        }


        $builder->add('deposito',VichFileType::class,[
            'label' => 'Comprovante de depósito',
            'required' => false,

        ]);


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\QuestionBundle\Entity\Resposta',
            'submit' => true,
            'allow_extra_fields' => true,
            'deposito' => false,
            'is_admin' => false,
            'translator' => null,
        ));
    }
}