<?php

namespace FCM\QuestionBundle\Form;

use FCM\QuestionBundle\Entity\Questionario;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionarioType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Questionario $qustionario */
        $qustionario = $options['data'];

        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'Quando o questionário é aplicado?',
                'choices' => array_flip(Questionario::getTypeChoices()),
                'required' => true,
                'disabled' => true,
            ])

            ->add('perguntas', CollectionType::class, [
                'label' => 'Perguntas',
                'options' => ['hasAvaliacao' => $options['hasAvaliacao']],
                'entry_type' => PerguntaType::class,
                'allow_add'    => true,
                'allow_delete' => true,
                'prototype' => true,
                'attr' => [
                    'class' => 'my-selector'
                ],
            ]);


        switch($qustionario->getType()) {
            case 0:
             $label_max = 'Número máximo de inscrições, deixe em branco para ILIMITADO';
             $builder->add('descricao', CKEditorType::class, [
                 'label' => 'Descrição/Instrução do questionário',
                 'required' => false,
             ]);
             break;
            case 1: $label_max = 'Número máximo de avaliações que uma trabalho pode receber, deixe em branco para ILIMITADO'; break;
            case 2: $label_max = 'Número máximo de avaliações do evento que um usuário poderá fazer, deixe em branco para ILIMITADO'; break;

        }
        $builder->add('max', TextType::class, [
            'label' => $label_max,
            'required' => false,
        ]);

        // Somente para feedback or not informed
        switch ($qustionario->getType()){
            case 1:
            $builder->add('dataIniAvaliacao', DatetimeType::class, [
                    'label' => 'Data do início das avaliações',

                    'required' => false,
                'attr' => array('class'=>'form_date_picker_date'),
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                ])
                ->add('dataFimAvaliacao', DatetimeType::class, [
                    'label' => 'Data final das avaliações',
                    'required' => false,
                    'attr' => array('class'=>'form_date_picker_date'),
                    'widget' => 'single_text',
                    'html5' => false,
                    'format' => 'dd/MM/yyyy',
                    ]);
            break;
            case 2:
                $builder->add('feedback', ChoiceType::class, [
                'label' => 'Questionário obrigatorio?',
                'choices' => array_flip(Questionario::getFeedbackChoices()),
                'required' => true,
                'disabled' => false,
            ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\QuestionBundle\Entity\Questionario',
            'hasAvaliacao' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fcm_questionbundle_questionario';
    }


}
