<?php

namespace FCM\QuestionBundle\Form;

use FCM\QuestionBundle\Entity\Pergunta;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PerguntaItemType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('textInscricao', TextType::class, [
                'label' => 'Opção ao inscrever',
                'required' => true,
            ])
            ->add('textCertificado', TextType::class, [
                'label' => 'Opção exibida no certificado',
                'required' => true,
            ])

            ->add('limEscolha', NumberType::class, [
                'label' => 'Limitar máx. seleção',
                'required' => false,
                'attr' => array(
                    //'placeholder' => 'Atingindo esse valor não será mais possível aos participantes selecionarem',
                ),
            ])
            ->add('valor', NumberType::class, [
                'label' => 'Valor associado ao campo',
                'required' => false,
                'attr' => array(
                    //'placeholder' => 'Uma vez selecionado o valor será acrescentado a inscrição',
                ),
            ])
            ->add('dataIniExibicao', DateTimeType::class, [
                'label' => 'Data do início de exibição',
                'required' => false,
                'attr' => array('class'=>'form_date_picker_date'),
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                ])
            ->add('dataFimExibicao', DateTimeType::class, [
                'label' => 'Data do fim da exibição',
                'required' => false,
                'attr' => array('class'=>'form_date_picker_date'),
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                ])
            ->add('dataLimDeposito', DateTimeType::class, [
                'label' => 'Data limite para depósitos',
                'required' => false,
                'attr' => array('class'=>'form_date_picker_date'),
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
            ]);
            ;

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\QuestionBundle\Entity\PerguntaItem',

        ));
    }

    public function getBlockPrefix()
    {
        return 'PerguntaItemType';
    }


}
