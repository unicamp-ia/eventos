<?php

namespace FCM\QuestionBundle\Naming ;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\QuestionBundle\Entity\Pergunta;
use FCM\QuestionBundle\Entity\PerguntaItem;
use FCM\QuestionBundle\Entity\Questionario;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\QuestionBundle\Entity\RespostaItem;
use FCM\QuestionBundle\Form\QuestionarioFillType;
use FCM\QuestionBundle\Form\QuestionarioType;
use FCM\QuestionBundle\Form\RespostaType;
use FCM\QuestionBundle\Repository\QuestionarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;
use Vich\UploaderBundle\Naming\UniqidNamer;

/**
 * Questionario controller.
 *
 */
class RespostaItemNamer implements NamerInterface
{

    public function name($object, PropertyMapping $mapping){

    }
}