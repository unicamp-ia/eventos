<?php

namespace FCM\LabelBundle\Controller;

use Endroid\QrCode\Factory\QrCodeFactory;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\LabelBundle\Entity\Etiqueta;
use Mpdf\Mpdf;
use Mpdf\Tag\Ins;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Etiquetum controller.
 *
 * @Route("admin/eventoAtividade/{eventoAtividade}/etiqueta")
 */
class EtiquetaController extends Controller
{
    /**
     * Lists all etiquetum entities.
     *
     * @Route("/", name="admin_eventoAtividade_etiqueta_index")
     * @Method("GET")
     */
    public function indexAction(EventoAtividade $eventoAtividade)
    {
        // Caso nao tenha etiqueta redirecionar
        if(!$eventoAtividade->getEtiqueta())
            return $this->redirectToRoute('admin_eventoAtividade_etiqueta_edit', array(
                'eventoAtividade' => $eventoAtividade->getId()));

        $em = $this->getDoctrine()->getManager();

        $etiquetas = $em->getRepository('FCMLabelBundle:Etiqueta')->findAll();

        return $this->render('FCMLabelBundle:Etiqueta:index.html.twig', array(
            'etiquetas' => $etiquetas,
        ));
    }


    /**
     * Finds and displays a etiquetum entity.
     *
     * @Route("/show", name="admin_eventoAtividade_etiqueta_show")
     * @Method("GET")
     */
    public function showAction(Etiqueta $etiqueta, EventoAtividade $eventoAtividade)
    {


        return $this->render('FCMLabelBundle:Etiqueta:show.html.twig', array(
            'page_title' => 'Etiqueta ' . $eventoAtividade->getNome(),
            'etiqueta' => $etiqueta,
            'back' => $this->generateUrl('admin_eventoAtividade_etiqueta_edit',['eventoAtividade' => $eventoAtividade->getId()]),
        ));
    }

    /**
     * Displays a form to edit an existing etiquetum entity.
     *
     * @Route("/edit", name="admin_eventoAtividade_etiqueta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, EventoAtividade $eventoAtividade)
    {

        if($eventoAtividade->getEtiqueta()) $etiqueta = $eventoAtividade->getEtiqueta();
        else $etiqueta = new Etiqueta();

        $form = $this->createForm('FCM\LabelBundle\Form\EtiquetaType', $etiqueta,['eventoAtividade' => $eventoAtividade]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $etiqueta->setEventoAtividade($eventoAtividade);
            $em->persist($etiqueta);
            $em->flush();

            $this->addFlash('success', 'Etiqueta salva com sucesso');
            return $this->redirectToRoute('admin_eventoAtividade_etiqueta_edit', array(
                'id' => $etiqueta->getId(), 'eventoAtividade' => $eventoAtividade->getId(),
            ));
        }

        return $this->render('FCMLabelBundle:Etiqueta:edit.html.twig', array(
            'page_title' => 'Editar etiqueta ' . $eventoAtividade->getNome(),
            'etiqueta' => $etiqueta,
            'perguntas' => $eventoAtividade->getQuestionarioInscricao() ? $eventoAtividade->getQuestionarioInscricao()->getPerguntas() : array(),
            'action_links' => [
                'show' => [
                    'url' => $this->generateUrl('admin_eventoAtividade_etiqueta_print',['eventoAtividade' => $eventoAtividade->getId()]),
                    'label' => 'Visualizar',
                ],

            ],
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'back' => $this->generateUrl('admin_evento_atividade_edit',[
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId()]),
            'form' => $form->createView(),

        ));
    }

    /**
     * Gera folha contendo as inscricoes
     * @Route("/print", name="admin_eventoAtividade_etiqueta_print",options={"expose"=true})
     */
    public function printAction(Request $request, EventoAtividade $eventoAtividade)
    {

        // Imprime somente uma ou todas as inscricoes
        if($request->get('inscricao')){
            $pos = $request->get('pos');

            $inscricao = $this->getDoctrine()->getManager()->getRepository('FCMEventoBundle:Inscricao')
                ->find($request->get('inscricao'));

            // primeira posicao
           if($pos == 1) {

               $inscricoes = array($inscricao,null);

           } else {
                // step para imprimir na n posicao
               for($i = 1;$i < $pos; $i++){
                   $inscricoes[] = null;
               }

               $inscricoes[] = $inscricao;
           }


        } else {
            $inscricoes = $eventoAtividade->getInscricoes();
        }

        $html =  $this->renderView('FCMLabelBundle:Etiqueta:print.html.twig', array(
            'eventoAtividade' => $eventoAtividade,
            'etiqueta' => $eventoAtividade->getEtiqueta(),
            'inscricoes' => $inscricoes,
        ));

        $mpdf = new Mpdf([
            'mode' => 'utf-8',
        ]);

        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }

    /**
     * @Route("/print/{inscricao}", name="admin_eventoAtividade_etiqueta_printItem")
     */
    public function printItemAction(Inscricao $inscricao, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $corpo = $inscricao->getEventoAtividade()->getEtiqueta()->getBody();

        $replace_arr = [
            '[user.nome]' => $inscricao->getParticipante()->getNome(),
            '[user.documento]' => $inscricao->getParticipante()->getDocumento(),
            '[evento.nome]' => $inscricao->getEventoAtividade()->getEvento()->getNome(),
            '[evento.local]' => $inscricao->getEventoAtividade()->getEvento()->getLocal(),
            '[evento.data]' => $inscricao->getEventoAtividade()->getDataIniAtividade()->format('d M Y'),
            '[evento.duracao]' => $inscricao->getEventoAtividade()->getDataIniAtividade()
                ->diff($inscricao->getEventoAtividade()->getDataFimAtividade())->h,
        ];


        /** @var Resposta $resposta */
        foreach ($inscricao->getRespostas() as $resposta) {
            /** @var RespostaItem $respostaIten */
            foreach ($resposta->getRespostaItens() as $respostaIten)
            {
                $resposta = $respostaIten->getRespostaPrepared();
                $replace_arr['[pergunta.'.$respostaIten->getPergunta()->getId().']'] = $resposta;
            }
        }


        foreach($replace_arr as $key => $row){
            $corpo = str_replace($key, $row, $corpo);
        }

        /** @var QrCodeFactory $factory */
        $factory = $this->get('endroid.qrcode.factory');

        $imagePath = '/site/seventos/web/qrcode/' . $inscricao->getId();

        $factory->create($inscricao->getId(),['size' => 70])->writeFile($imagePath);



        return $this->render('FCMLabelBundle:Etiqueta:print.item.html.twig', array(
            'etiqueta' => $inscricao->getEventoAtividade()->getEtiqueta(),
            'inscricao' => $inscricao,
            'index' => $request->get('index'),
            'image_path' => $imagePath,
            'corpo' => $corpo,
        ));



    }

    /**
     * Deletes a etiquetum entity.
     *
     * @Route("/{id}/delete", name="admin_eventoAtividade_etiqueta_delete")
     * @Method({"POST","GET"})
     */
    public function deleteAction(Request $request, Etiqueta $etiquetum)
    {
        return $this->createFormBuilder()
            ->getForm()
            ;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($etiquetum);
            $em->flush();
        }

        return $this->redirectToRoute('admin_eventoAtividade_etiqueta_index',[

        ]);
    }


}
