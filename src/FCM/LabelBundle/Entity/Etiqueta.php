<?php

namespace FCM\LabelBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Etiqueta
 *
 * @ORM\Table(name="etiqueta")
 * @ORM\Entity(repositoryClass="FCM\LabelBundle\Repository\EtiquetaRepository")
 */
class Etiqueta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @ORM\OneToOne(targetEntity="\FCM\EventoBundle\Entity\EventoAtividade", inversedBy="etiqueta")
     * @ORM\JoinColumn(name="evento_atividade_id", referencedColumnName="id")
     */
    private $eventoAtividade;



    /**
     * @var bool
     *
     * @ORM\Column(name="qrcode", type="boolean")
     */
    private $qrcode;

    /**
     * @var bool
     *
     * @ORM\Column(name="eventoNome", type="boolean")
     */
    private $eventoNome;

    /**
     * @var bool
     *
     * @ORM\Column(name="atividadeNome", type="boolean")
     */
    private $atividadeNome;

    /**
     * @var string
     *
     * @ORM\Column(name="header", type="text", nullable=true)
     */
    private $body;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qrcode
     *
     * @param boolean $qrcode
     *
     * @return Etiqueta
     */
    public function setQrcode($qrcode)
    {
        $this->qrcode = $qrcode;

        return $this;
    }

    /**
     * Get qrcode
     *
     * @return bool
     */
    public function getQrcode()
    {
        return $this->qrcode;
    }

    /**
     * Set eventoNome
     *
     * @param boolean $eventoNome
     *
     * @return Etiqueta
     */
    public function setEventoNome($eventoNome)
    {
        $this->eventoNome = $eventoNome;

        return $this;
    }

    /**
     * Get eventoNome
     *
     * @return bool
     */
    public function getEventoNome()
    {
        return $this->eventoNome;
    }

    /**
     * Set atividadeNome
     *
     * @param boolean $atividadeNome
     *
     * @return Etiqueta
     */
    public function setAtividadeNome($atividadeNome)
    {
        $this->atividadeNome = $atividadeNome;

        return $this;
    }

    /**
     * Get atividadeNome
     *
     * @return bool
     */
    public function getAtividadeNome()
    {
        return $this->atividadeNome;
    }

    /**
     * Set header
     *
     * @param string $body
     *
     * @return Etiqueta
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get header
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }



    /**
     * Set eventoAtiviidade
     *
     * @param \FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade
     *
     * @return Etiqueta
     */
    public function setEventoAtividade(\FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade = null)
    {
        $this->eventoAtividade = $eventoAtividade;

        return $this;
    }

    /**
     * Get eventoAtiviidade
     *
     * @return \FCM\EventoBundle\Entity\EventoAtividade
     */
    public function getEventoAtividade()
    {
        return $this->eventoAtividade;
    }
}
