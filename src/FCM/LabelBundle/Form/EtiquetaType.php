<?php

namespace FCM\LabelBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EtiquetaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('qrcode', CheckboxType::class, [
                'label' => 'Exibir QRCode?',
                'required' => false,
            ])
            ->add('eventoNome', CheckboxType::class,[
                'label' => 'Exibir nome do evento na etiqueta?',
                'required' => false,
            ])
            ->add('atividadeNome', CheckboxType::class, [
                'label' => 'Exibir nome da atividade na etiqueta?',
                'required' => false,
            ])
            ->add('body', CKEditorType::class, [
                'label' => 'Corpo',
                'required' => false,
            ])

        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\LabelBundle\Entity\Etiqueta',
            'eventoAtividade' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'EtiquetaType';
    }


}
