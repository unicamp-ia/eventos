<?php

namespace FCM\EventoBundle\Repository;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\Participante;
use FCM\UserBundle\Entity\User;

/**
 * EventoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EventoRepository extends \Doctrine\ORM\EntityRepository
{
    public function byUser(User $user){
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('e')
            ->from('FCMEventoBundle:Evento', 'e')
            ->where('e.createdBy = :u')
            ->setParameters(['u' => $user->getId()])
            ->orderBy('e.created','DESC')
            ->getQuery();

        $query2 = $this->getEntityManager()->createQueryBuilder()
            ->select('e')
            ->from('FCMEventoBundle:Evento', 'e')
            ->leftjoin('FCMEventoBundle:EventoAdmin', 'ea', 'WITH', 'ea.evento = e.id')
            ->where('ea.user = :u')
            ->setParameters(['u' => $user->getId()])
            ->getQuery();

        return array_merge($query->getResult(),$query2->getResult());
    }

    /**
     * Return evento em periodo de inscricao e ordenado pelo inicio da atividade
     * @return array
     */
    public function byEventoEmInscricao(){
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('e')
            ->from('FCMEventoBundle:Evento', 'e')
            ->leftjoin('FCMEventoBundle:EventoAtividade', 'ea', 'WITH', 'ea.evento = e.id')
            ->where('ea.dataIniInscricao <= CURRENT_DATE()')
            ->andWhere('ea.dataFimInscricao >= CURRENT_DATE()')
            ->orderBy('ea.dataIniAtividade', 'ASC')
            ->getQuery();

        return $query->getResult();
    }

    public function findEventoAtividadeOrderInscricao(Evento $evento){
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('ea')
            ->from('FCMEventoBundle:Evento', 'e')
            ->leftjoin('FCMEventoBundle:EventoAtividade', 'ea', 'WITH', 'ea.evento = e.id')
            ->where('e.id = :evento')
            ->orderBy('ea.dataFimInscricao','DESC')
            ->setParameter('evento', $evento->getId())
            ->getQuery();

        return $query->getResult();
    }

    public function byParticipante(Participante $participante){
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('e')
            ->from('FCMEventoBundle:Evento', 'e')
            ->leftjoin('FCMEventoBundle:EventoAtividade', 'ea', 'WITH', 'ea.evento = e.id')
            ->leftjoin('FCMEventoBundle:Inscricao', 'i', 'WITH', 'i.eventoAtividade = ea.id')
            ->where('i.participante = :p')
            ->setParameters(['p' => $participante->getId()])
            ->getQuery();
        return $query->getResult();
    }

    public function byEmPeriodoDeAvaliacao(){
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('e')
            ->from('FCMEventoBundle:Evento', 'e')
            ->leftjoin('FCMEventoBundle:EventoAtividade', 'ea', 'WITH', 'ea.evento = e.id')
            ->leftjoin('FCMQuestionBundle:Questionario', 'q', 'WITH', 'q.eventoAtividade = ea.id')
            ->where('q.dataIniAvaliacao <= :curr')
            ->andWhere('q.dataFimAvaliacao >= :curr')
            ->setParameters(['curr' => new \DateTime()])
            ->getQuery();
        return $query->getResult();
    }
}
