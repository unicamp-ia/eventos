<?php

namespace FCM\EventoBundle\Controller;

use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\InscricaoToken;
use FCM\QuestionBundle\Entity\Pergunta;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\QuestionBundle\Entity\RespostaItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;

class CertificadoController extends Controller
{
    /**
     * @Route("/certificado/{token}", name="certificado_show")
     */
    public function showAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Inscricao $inscricao */
        $inscricao = $em->getRepository('FCMEventoBundle:Inscricao')->findOneBy(['token' => $token]);

        $inscricao->setEmitiu(true);
        $em->persist($inscricao);
        $em->flush($inscricao);

        /** @var File $template */
        if($inscricao->getEventoAtividade()->getEvento()->getTemplate() instanceof File) {
            $template = $inscricao->getEventoAtividade()->getEvento()->getTemplate()->getPathname();
        } else $template = null;

        $assinatura = null;
        if($inscricao->getEventoAtividade()->getEvento()->getAssinatura() instanceof File) {
            /** @var File $assinatura */
            $assinatura = $this->cropSignature($inscricao->getEventoAtividade()->getEvento()->getAssinatura());
        }

        $corpo = $inscricao->getEventoAtividade()->getModelo();

        $replace_arr = [
            '[user.nome]' => $inscricao->getParticipante()->getNome(),
            '[user.documento]' => $inscricao->getParticipante()->getDocumento(),
            '[evento.nome]' => $inscricao->getEventoAtividade()->getEvento()->getNome(),
            '[evento.local]' => $inscricao->getEventoAtividade()->getEvento()->getLocal(),
            '[evento.data]' => $inscricao->getEventoAtividade()->getDataIniAtividade()->format('d M Y'),
            '[evento.duracao]' => $inscricao->getEventoAtividade()->getDataIniAtividade()
        ->diff($inscricao->getEventoAtividade()->getDataFimAtividade())->h,
        ];

        /** @var InscricaoToken $inscricaoToken */
        foreach ($inscricao->getInscricaoTokens() as $inscricaoToken) {
            $replace_arr['[token.'.$inscricaoToken->getAtividadeToken()->getToken()->getId().']'] = $inscricaoToken->getVal();
        }

        /** @var Resposta $resposta */
        foreach ($inscricao->getRespostas() as $resposta) {
            /** @var RespostaItem $respostaIten */
            foreach ($resposta->getRespostaItens() as $respostaIten)
            {

                if($respostaIten->getPerguntaItem()){
                    $resposta = strip_tags(trim($respostaIten->getPerguntaItem()->getTextCertificado()));
                } else {
                    $resposta = strip_tags(trim($respostaIten->getRespostaPrepared()))  ;
                }

                $replace_arr['[pergunta.'.$respostaIten->getPergunta()->getId().']'] = $resposta;
            }
        }


        // Substitui as tags pelas respostas
        foreach($replace_arr as $pergunta => $row){

            $posPerguntaCorpo = strpos($corpo, $pergunta);

            // Se pergunta contem no corpo do certificado, caracter anterior eh virgula e nao
            // possui resposta: retira virgula

            if($posPerguntaCorpo and substr($corpo,$posPerguntaCorpo -2,1) == ';'){
                if($replace_arr[$pergunta] == ""){
                    $corpo = substr_replace($corpo,'',$posPerguntaCorpo -2,1);
                }


            }

            $corpo = str_replace($pergunta, $row, $corpo);
        }


        $html =  $this->renderView('FCMEventoBundle:Certificado:print.html.twig', array(
            'fundo' => $template,
            'eventoAtividade' => $inscricao->getEventoAtividade(),
            'assinatura' => $assinatura,
            'inscricao' => $inscricao,
            'corpo' => $corpo,
            'link' => $this->generateUrl('certificado_show',['token' => $token])
        ));



        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4-L'
        ]);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }



    /**
     * This function will receive the full signature and crop (in a rectangle format)
     * only the signature part.
     *
     * Crop rectangle area
     *  x1     => 150
     *  y1     => 800
     *  width  => 1455
     *  height => 181
     *
     * and the DPIs are:
     *  150 DPI
     *  Horizontal => 1754px
     *  Vertical   => 1240px
     *
     */
    function cropSignature(File $file){
        //DPI size constant
        $dpi150x = 1754;
        $dpi150y = 1240;

        //Creates a new copy of the original image converted in the correct format
        //(JPG or PNG)
        if($file->getExtension() == 'jpg')
            $image = imagecreatefromjpeg($file->getPathname());

        if($file->getExtension() == 'PNG')
            $image = imagecreatefrompng($file->getPathname());


        //Load the image size of x and y
        $size = getimagesize($file->getPathname());
        $x = $size[0];
        $y = $size[1];

        //Check if the image is on portrait. Case true: rotate to landscape
        if ($x < $y) {
            $image = imagerotate($image, 270, 0 );
        }

        // Resize the image to A4 DPI 150
        $image = imagescale($image, $dpi150x, $dpi150y,  IMG_BICUBIC_FIXED);

        //Crop 146
        $to_crop_array = array('x' =>150 , 'y' => 800, 'width' => 1455, 'height'=> 181);

        $image = imagecrop($image, $to_crop_array);

        //Output the img
        $path_2 = str_replace('.jpg','_2.jpg',$file->getPathname());
        imagePng($image, $path_2);

        // Release (the kraken) memory
        imageDestroy($image);

        return $path_2;
    }

}
