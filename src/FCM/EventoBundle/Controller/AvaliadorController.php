<?php

namespace FCM\EventoBundle\Controller;

use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Form\AvaliadorType;
use FCM\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class AvaliadorController extends Controller
{
    /**
     * @Route("/admin/evento/{evento}/avaliador", name="admin_evento_avaliador_index")
     */
    public function indexAction(Evento $evento)
    {

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('FCMUserBundle:User')->findBy(['vinculo' => 'Avaliador']);

        return $this->render('FCMEventoBundle:Avaliador:index.html.twig', array(
            'page_title' => 'Avaliador',
            'users' => $users,
            'evento' => $evento,
            'action_links' => [
                'new' => [
                    'label' => 'Incluir avaliador',
                    'url' => $this->generateUrl('admin_avaliador_new', ['evento' => $evento->getId()])
                ],
            ],
            'back' => $this->generateUrl('home'),
        ));
    }

    /**
     * @Route("/admin/evento/{evento}/avaliador/new", name="admin_avaliador_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Evento $evento)
    {
        $user = new User();
        $form = $this->createForm(AvaliadorType::class, $user);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted()) {

            if($userCheck = $em->getRepository('FCMUserBundle:User')->findOneBy(['email' => $user->getEmail()])){
                $user = $userCheck;
            }

            $user->setUsername($user->getEmail());
            $user->setVinculo('Avaliador');
            $user->setToken();

            $em->persist($user);
            $em->flush();

            $this->sendNotify($user, $evento);

            $this->addFlash('success', 'Avaliador incluído com sucesso');

            return $this->redirectToRoute('admin_evento_avaliador_index', ['evento' => $evento->getId()]);
        }

        return $this->render('FCMEventoBundle:Avaliador:edit.html.twig', array(
            'page_title' => 'Incluir avaliador',
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_evento_avaliador_index', ['evento' => $evento->getId()]),
        ));
    }

    /**
     * Envia e-mail para avaliadr
     * @param User $user
     */
    public function sendNotify(User $user, Evento $evento){

        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Eventos] Solicitação para avaliação de trabalhos')
            ->setFrom($this->getParameter('mail'))
            ->setTo($user->getEmail())
            ->setReplyTo($evento->getEmail())
            ->setCc($evento->getEmail())
            ->setBody(
                $this->renderView('FCMEventoBundle:Emails:avaliador.html.twig',[
                    'user' => $user,
                    'evento' => $evento,
                    'link' => $this->generateUrl('admin', ['token' => $user->getToken()],true)
                ]),
                'text/html'
            );

        $this->get('mailer')->send($message);

    }

    /**
     * @Route("/admin/avaliador/{id}/edit", name="admin_avaliador_edit")
     */
    public function editAction()
    {
        return $this->render('FCMEventoBundle:Avaliador:edit.html.twig', array(
            'page_title' => 'Incluir avaliador',
            'back' => $this->generateUrl('admin_avaliador_index'),
        ));
    }

}
