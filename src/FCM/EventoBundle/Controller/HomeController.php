<?php

namespace FCM\EventoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Repository\EventoRepository;
use FCM\UserBundle\Entity\User;
use FCM\UserBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function homeAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EventoRepository $eventos_repo */
        $eventos_repo = $em->getRepository('FCMEventoBundle:Evento');

        /** @var array $eventos */
        $eventos = $eventos_repo->byEventoEmInscricao();

        $session = $this->get('session');

        /** @var Evento $evento */
        foreach($eventos as $key => $evento){
            $show_evento = false;
            /** @var EventoAtividade $eventoAtividade */
            foreach($evento->getEventoAtividades() as $eventoAtividade){
                if($eventoAtividade->isInscricaoPossible($session)) $show_evento = true;
            }

            if(!$show_evento) unset($eventos[$key]);
        }

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        // Se usuario eh participante verifica dcumento
        if($user instanceof User and $user->getVinculo() == 'Eventos'){
            $participante = $user->getParticipante();

            if(!$user->getParticipante()->getDocumento()){
                $this->addFlash('error','Documento não encontrado, complete seu cadastro');
                return $this->redirectToRoute('participante_edit',['id' => $participante->getId()]);

            }
        }

        if($eventos){
            $eventos = array_chunk($eventos, ceil(count($eventos) / 3));
        }


        return $this->render('FCMEventoBundle:Home:home.html.twig', array(
            'eventos' => $eventos,
            'user' => $user,
        ));
    }

    /**
     * @Route("/participante/dashboard", name="dashboard")
     */
    public function dashboardAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EventoRepository $eventos_repo */
        $eventos_repo = $em->getRepository('FCMEventoBundle:Evento');

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $eventos = $eventos_repo->byParticipante($user->getParticipante());



        $eventos = array_chunk($eventos, ceil(count($eventos) / 3));


        return $this->render('FCMEventoBundle:Home:home.html.twig', array(
            'eventos' => $eventos,
            'user' => $user,
        ));
    }

    /**
     * @Route("/about/help", name="help")
     */
    public function helpAction()
    {
        return $this->render('FCMEventoBundle:Home:help.html.twig', array());
    }

}
