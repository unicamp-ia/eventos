<?php

namespace FCM\EventoBundle\Controller;

use FCM\EventoBundle\Entity\Participante;
use FCM\EventoBundle\Form\ResetPassType;
use FCM\EventoBundle\Repository\ParticipanteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ResetPassController extends Controller
{
    /**
     * Solicita e-mail
     * @Route("/login/pass/request", name="resetpass_request")
     * @Method({"GET", "POST"})
     */
    public function requestAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var ParticipanteRepository $part_repo */
        $part_repo = $em->getRepository('FCMEventoBundle:Participante');

        /** @var Form $form */
        $form = $this->createForm(ResetPassType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $participante = $part_repo->findOneBy(['email' => $request->get('reset_pass')['email']]);


            if(!$participante) {

                $participante = $part_repo->findOneBy(['documento' => $request->get('reset_pass')['documento']]);
            }

            if($participante instanceof Participante){
                $this->addFlash('success', 'Participante encontrado');
                return $this->redirectToRoute('resetpass_request_show', ['id' => $participante->getId()]);
            } else {
                $request->getSession()->getFlashBag()->add('error', 'E-mail ou documento não encontrado');
            }
        }

        return $this->render('FCMEventoBundle:ResetPass:request.html.twig',[
            'page_title' => 'Redefinir senha',
            'form' => $form->createView(),
            'back' => $this->generateUrl('participante_login'),
        ]);
    }

    /**
     * Solicita e-mail
     * @Route("/login/pass/request/{id}", name="resetpass_request_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Participante $participante)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Form $form */
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $participante->setResetPass($this->getTokenResetPage($participante));
            $em->persist($participante);
            $em->flush();
            $this->sendResetAction($participante, $participante->getEmail());
            $request->getSession()->getFlashBag()->add('success', 'Instruções de redefinição de senha enviada para seu e-mail');
        }

        return $this->render('FCMEventoBundle:ResetPass:request.step.html.twig',[
            'page_title' => 'Redefinir senha de ' . $participante->getNome(),
            'form' => $form->createView(),
            'participante' => $participante,
            'back' => $this->generateUrl('resetpass_request'),
        ]);
    }

    /**
     * Solicita e-mail
     * @Route("/login/pass/request/{id}/step2", name="resetpass_request_show_step2")
     * @Method({"GET", "POST"})
     */
    public function step2Action(Request $request, Participante $participante)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();


        /** @var Form $form */
        $form = $this->createForm(ResetPassType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $participante->setResetPass($this->getTokenResetPage($participante));
            $em->persist($participante);
            $em->flush();
            $this->sendResetAction($participante, $request->get('reset_pass')['email']);
            $request->getSession()->getFlashBag()->add('success', 'Instruções de redefinição de senha enviada para seu e-mail');
            return $this->redirectToRoute('home');

        }

        return $this->render('FCMEventoBundle:ResetPass:request.step2.html.twig',[
            'page_title' => 'Redefinir senha de ' . $participante->getNome(),
            'form' => $form->createView(),
            'participante' => $participante,
            'back' => $this->generateUrl('resetpass_request'),
        ]);
    }

    /**
     * @param Participante $participante
     */
    public function sendResetAction(Participante $participante, $mail){
        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Eventos] Link para redefinição de senha')
            ->setFrom($this->getParameter('mail'))
            ->setTo($participante->getEmail())

            ->setBody(
                $this->renderView('FCMEventoBundle:Emails:reset.html.twig',[
                    'participante' => $participante,
                    'link' => $this->generateUrl('resetpass_reset', [
                        'token' => $this->getTokenResetPage($participante),
                        'mail' => $mail,
                    ], UrlGeneratorInterface::ABSOLUTE_URL)
                ]),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }

    /**
     * @param Participante $participante
     * @return string
     */
    public function getTokenResetPage(Participante $participante){
        return md5('Reset'.$participante->getDocumento().$participante->getEmail().date('d-m-Y'));
    }

    /**
     *
     * @Route("/login/pass/reset/{token}", name="resetpass_reset")
     * @Method({"GET", "POST"})
     */
    public function resetAction(Request $request, $token)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var ParticipanteRepository $part_repo */
        $part_repo = $em->getRepository('FCMEventoBundle:Participante');

        $participante = $part_repo->findOneBy(['resetPass' => $token]);



        $form = $this->createFormBuilder()
            ->add('pass', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Senha'),
                'second_options' => array('label' => 'Confirme a senha'),
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mail',
                'disabled' => true,
                'data' => $request->get('mail')
            ])
            ->getForm();

        $form->handleRequest($request);

        if(!($participante instanceof Participante)){
            $request->getSession()->getFlashBag()->add('error', 'Link não encontrado para redefinição de senha, solicite novamente.');
            return $this->redirectToRoute('participante_login');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $translator = $this->get('translator');
            // Check complexidade
            if (!preg_match_all('$\S*(?=\S{8,})(?=\S*[a-zA-Z])(?=\S*[\d])$', $form->get('pass')->getViewData()['first'])){
                $request->getSession()->getFlashBag()->add('error', $translator->trans('errosenha'));
            } else {

                $participante->setPass($request->get('form')['pass']['first']);
                $participante->setResetPass(null);

                $em->persist($participante);
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Senha alterada com sucesso');
                return $this->redirectToRoute('participante_login');
            }
        }
        return $this->render('FCMEventoBundle:ResetPass:reset.html.twig',[
            'participante' => $participante,
            'page_title' => 'Redefinição de senha',
            'form' => $form->createView(),
        ]);
    }
}
