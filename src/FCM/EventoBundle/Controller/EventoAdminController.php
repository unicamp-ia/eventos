<?php

namespace FCM\EventoBundle\Controller;

use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAdmin;
use FCM\EventoBundle\Repository\EventoAdminRepository;
use FCM\EventoBundle\Service\IntranetTasker;
use FCM\UserBundle\Entity\AD;
use FCM\UserBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\User;

/**
 * Eventoadmin controller.
 *
 * @Route("")
 */
class EventoAdminController extends Controller
{
    private $intranetTasker;


    /**
     * Lists all eventoAdmin entities.
     *
     * @Route("/admin/evento/{evento}/admin", name="admin_evento_admin_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request, Evento $evento)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var EventoAdminRepository $ea_repo */
        $ea_repo = $em->getRepository('FCMEventoBundle:EventoAdmin');
        $eas = $ea_repo->findBy(['evento' => $evento->getId()]);

        $eventoAdmin = new Eventoadmin();
        $form = $this->createForm('FCM\EventoBundle\Form\EventoAdminType', $eventoAdmin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user_ids = $request->get('evento_admin')['user'];
            foreach($user_ids as $user_id){
                $user = $em->getRepository('FCMUserBundle:User')->find($user_id);

                $eventoAdmin = new EventoAdmin();
                $eventoAdmin->setUser($user);
                $eventoAdmin->setEvento($evento);
                $eventoAdmin->setCreatedBy($user);
                $em->persist($eventoAdmin);
            }

            $em->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso');
            return $this->redirectToRoute('admin_evento_admin_index', array('evento' => $evento->getId()));
        }

        return $this->render('FCMEventoBundle:EventoAdmin:index.html.twig', array(
            'page_title' => $evento->getNome() . ' - Administradores do evento',
            'page_title_desc' => 'Inclua outros usuários para administrar o evento',
            'form' => $form->createView(),
            'eas' => $eas,
            'evento' => $evento,
            'back' => $this->generateUrl('admin_evento_edit', ['id' => $evento->getId()])
        ));
    }


    /**
     *
     * @Route("/getUser", name="admin_evento_admin_getUser")
     * @Method("GET")
     */
    public function getUserAction(Request $request)
    {
        $resp = array();
        $ads = array();

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $this->intranetTasker = $this->get('fcm.intranet_tasker');
        $userIntranet = $this->intranetTasker->getUser($request->get('q'));

        if(!$userIntranet) return new JsonResponse(null);

        $user = $em->getRepository('FCMUserBundle:User')->findOneBy(['username' => $userIntranet->username]);

        if(!$user instanceof \FCM\UserBundle\Entity\User) {
            $user = new \FCM\UserBundle\Entity\User();
            $user->setUsername($userIntranet->username);
            $user->setNome($userIntranet->nome);
            $user->setEmail($userIntranet->email);
            $user->setVinculo(\FCM\UserBundle\Entity\User::VINCULO_COLABORADOR);
            $em->persist($user);
            $em->flush();
        }

        $resp[] = [
            'id' => $user->getId(),
            'text' => $userIntranet->nome,
        ];

        return new JsonResponse($resp);
    }

    /**
     *
     *
     * @Route("/admin/evento/{evento}/admin/{id}/delete", name="admin_evento_admin_delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request, Evento $evento, EventoAdmin $eventoAdmin)
    {

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $em->remove($eventoAdmin);
        $em->flush();

        $this->addFlash('success','Exclusão efetuada com sucesso');
        return $this->redirectToRoute('admin_evento_admin_index', array('evento' => $evento->getId()));



    }
}
