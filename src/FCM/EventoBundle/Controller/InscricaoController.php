<?php

namespace FCM\EventoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto;
use FCM\EventoBundle\Entity\AtividadeToken;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\InscricaoToken;
use FCM\EventoBundle\Entity\Participante;
use FCM\EventoBundle\Entity\Token;
use FCM\EventoBundle\Form\InscricaoType;
use FCM\EventoBundle\Repository\TokenRepository;
use FCM\QuestionBundle\Entity\Pergunta;
use FCM\QuestionBundle\Entity\PerguntaItem;
use FCM\QuestionBundle\Entity\Questionario;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\QuestionBundle\Entity\RespostaItem;
use FCM\QuestionBundle\Form\RespostaDepositoType;
use FCM\QuestionBundle\Form\RespostaType;
use FCM\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Inscricao controller.
 *
 * @Route("evento/{evento}")
 */
class InscricaoController extends Controller
{
    /** @var  Resposta */
    private $resposta;
    /**
     * Lists all token entities.
     *
     * @Route("/atividade", name="evento_atividade_index")

     * @Method("GET")
     */
    public function indexAction(Request $request, Evento $evento)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /** @var ArrayCollection $eventoAtividades */
        $eventoAtividades = $em->getRepository('FCMEventoBundle:Evento')->findEventoAtividadeOrderInscricao($evento);

        return $this->render('FCMEventoBundle:Inscricao:index.html.twig', array(
            'participante' => $user instanceof User ? $user->getParticipante() : null,
            'page_title' => $evento->getNome(). ' - Inscrição',
            'session' => $this->get('session'),
            'evento' => $evento,
            'eventoAtividades' => $eventoAtividades
        ));
    }

    /**
     * Creates a new inscricao entity.
     *
     * @Route("/atividade/{eventoAtividade}/inscricao/{inscricao}/confirm", name="evento_inscricao_confirm")
     * @Method({"GET", "POST"})
     */
    public function confirmAction(Request $request, EventoAtividade $eventoAtividade, Inscricao $inscricao)
    {
        if($this->get('security.authorization_checker')->isGranted('ROLE_PARTICIPANTE')){
            $back = $this->generateUrl('evento_atividade_index', ['evento' => $eventoAtividade->getEvento()->getId()]);
        } else {
            $back = $this->generateUrl('admin_evento_atividade_presenca_index', [
                'evento' => $eventoAtividade->getEvento()->getId(),
                'eventoAtividade' => $eventoAtividade->getId()
            ]);
        }

        $translator = $this->get('translator');

        $resposta = $inscricao->getRespostaType(Questionario::TYPE_PARTICIPANTE)->first();

        $em = $this->getDoctrine()->getManager();

        $valores = $em->getRepository('FCMQuestionBundle:RespostaItem')->createQueryBuilder('a')
            ->select('a')
            ->leftJoin('FCMQuestionBundle:Resposta','b', 'WITH','a.resposta = b.id')
            ->andWhere('b.inscricao = :inscricao')->setParameter('inscricao', $inscricao->getId())
            ->andWhere('a.valor IS NOT NULL')
            ->getQuery()->getResult();

        return $this->render('FCMEventoBundle:Inscricao:confirm.html.twig',[
            'page_title' => $translator->trans('inscricaosalva'),
            'inscricao' => $inscricao,
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'valores' => $valores,
            'questionario' => $resposta instanceof Resposta ? $resposta->getQuestionario() : false,
            'back' => $back,
        ]);
    }
    /**
     * Creates a new inscricao entity.
     *
     * @Route("/atividade/{eventoAtividade}/inscricao/new", name="evento_inscricao_new")
     * @Security("has_role('ROLE_USER')")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, EventoAtividade $eventoAtividade)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $inscricao = new Inscricao();
        $inscricao->setCreated(new \DateTime());

        // Inscricao manual
        if(is_null($request->get('participante'))){
            /** @var User $user */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $participante = $user->getParticipante();
        } else {
            /** @var Participante $participante */
            $participante = $em->getRepository('FCMEventoBundle:Participante')->find($request->get('participante'));
        }

        $inscricao->setEventoAtividade($eventoAtividade);
        $inscricao->setParticipante($participante);


        /** @var Questionario $questionario */
        $form = $this->getQuestionarioForm($eventoAtividade, $em,null,false,true);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $inscricao->setCreated(new \DateTime());
            $inscricao->setEmitiu(0);
            $inscricao->setPresenca(false);
            $inscricao->setLiberado(false);
            $em->persist($inscricao);


            if(isset($request->get('resposta')['respostaItens'])){

                foreach ($request->get('resposta')['respostaItens'] as $resposta){
                    foreach ($resposta as $perguntaId => $valor){
                        /** @var Pergunta $pergunta */
                        $pergunta = $em->getRepository('FCMQuestionBundle:Pergunta')->find($perguntaId);
                        $questionarios[$pergunta->getQuestionario()->getId()] = $pergunta->getQuestionario();
                    }
                }


                foreach ($questionarios as $questionario){
                    if(!$this->saveResposta($request,$em,$inscricao, $questionario)){
                        return $this->render('FCMEventoBundle:Inscricao:edit.html.twig', array(
                            'page_title' => $eventoAtividade->getEvento()->getNome() .' - '. $eventoAtividade->getNome(),
                            'inscricao' => $inscricao,
                            'atividade' => $eventoAtividade->getAtividade(),
                            'form' => $form->createView(),
                            'questionario' => isset($questionario) ? $questionario : null,
                            'back' => $this->generateUrl('evento_atividade_index', ['evento' => $eventoAtividade->getEvento()->getId()]),
                        ));
                    }

                }

            }

            // Gera boleto funcamp se selecionado
            if($eventoAtividade->getAtividadeBoleto() instanceof EventoAtividadeBoleto
                and $eventoAtividade->getAtividadeBoleto()->getAuto()){


                // Busca respostas com valores associados
                $itens = $em->getRepository('FCMQuestionBundle:RespostaItem')->createQueryBuilder('a')
                    ->select('a')
                    ->leftJoin('FCMQuestionBundle:Resposta','b', 'WITH','a.resposta = b.id')
                    ->andWhere('b.inscricao = :inscricao')->setParameter('inscricao', $inscricao->getId())
                    ->andWhere('a.valor > 0')
                    ->getQuery()->getResult();



                $valor = 0;

                /** @var RespostaItem $item */
                foreach ($itens as $item){
                    $valor += $item->getValor();
                }

                if($valor == 0 ){
                    $valor = $eventoAtividade->getAtividadeBoleto()->getValorPadrao();
                }

                if($inscricaoBoleto = $this->get('fcm_boleto_funcamp.boleto_tasker')->newBoleto($inscricao,
                    $valor)){
                    $inscricao->setBoleto($inscricaoBoleto);
                    $this->addFlash('success','O boleto para pagamento foi enviado para seu e-mail');
                }


            }

            $em->flush();
            $this->sendInscricao($inscricao);
            $this->addFlash('success','Inscrição efetuada com sucesso, um e-mail de confirmação foi enviado');

            return $this->redirectToRoute('evento_inscricao_confirm', array(
                'evento' => $inscricao->getEventoAtividade()->getEvento()->getId(),
                'eventoAtividade' => $inscricao->getEventoAtividade()->getId(),
                'inscricao' => $inscricao->getId(),
            ));
        }

        return $this->render('FCMEventoBundle:Inscricao:edit.html.twig', array(
            'page_title' => $eventoAtividade->getEvento()->getNome() .' - '. $eventoAtividade->getNome(),
            'inscricao' => $inscricao,
            'evento' => $eventoAtividade->getEvento(),
            'atividade' => $eventoAtividade->getAtividade(),
            'form' => $form->createView(),
            'questionario' => isset($questionario) ? $questionario : null,
            'back' => $this->generateUrl('evento_atividade_index', ['evento' => $eventoAtividade->getEvento()->getId()]),
        ));
    }

    /**
     * @param EventoAtividade $eventoAtividade
     * @param EntityManager $em
     * @return \Symfony\Component\Form\Form
     */
    protected function getQuestionarioForm(EventoAtividade $eventoAtividade,
                                           EntityManager $em,
                                           Inscricao $inscricao = null,
                                           $depositoEnabled = false, $isNew = false){
        $resposta = new Resposta();

        if($questionario = $eventoAtividade->getQuestionarioInscricao()) {
            /** @var Resposta $resposta */
            if($inscricao instanceof Inscricao and $resposta = $inscricao->getRespostas()->first()){

            } else {
                $resposta = new Resposta();
            }

            // Verifica se cada pergunta ja possui uma resposta
            /** @var Pergunta $pergunta */
            foreach ($questionario->getPerguntas() as $pergunta) {
                if(!$ri = $this->getDoctrine()->getRepository('FCMQuestionBundle:RespostaItem')
                ->findOneBy(['resposta' => $resposta->getId(),'pergunta' => $pergunta->getId()])){
                    $ri = new RespostaItem();
                    $ri->setResposta($resposta);
                    $ri->setPergunta($pergunta);
                    $resposta->addRespostaIten($ri);
                }
            }


        }

        $isAdmin = !$this->get('security.authorization_checker')->isGranted('ROLE_PARTICIPANTE');

        $formQuestionario = $this->createForm(RespostaType::class, $resposta, [
            'submit' => true,'deposito' => $depositoEnabled, 'is_admin' => $isAdmin, 'translator' => $this->get('translator'),'isNew' => $isNew,
        ]);
        return $formQuestionario;
    }

    /**
     * @param Request $request
     * @param EntityManager $em
     * @param Inscricao $inscricao
     * @param Questionario $questionario
     * @return Resposta|mixed|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function saveResposta(Request $request, EntityManager $em, Inscricao $inscricao,
                                    Questionario $questionario){


        if(!isset($request->get('resposta')['respostaItens'])) return;

        if(!$resposta = $inscricao->getRespostaType(0)->first()){
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $resposta = new Resposta();
            $resposta->setCreated(new \DateTime());
            $resposta->setInscricao($inscricao);
            $resposta->setCreatedBy($user);
            $resposta->setQuestionario($questionario);
        }

        /** @var RespostaItem $respostaItem */
        foreach($resposta->getRespostaItens() as $respostaItem){

            // Exclui respostas com excecao das que contiver arquivos
            if(!$respostaItem->getFile() instanceof File) {
                $resposta->getRespostaItens()->removeElement($respostaItem);
                $em->persist($respostaItem);
                $em->remove($respostaItem);
            }
        }


        // Pago e nao notificado

        if(isset($request->get('resposta')['pago']) and $request->get('resposta')['pago'] and !$resposta->isNotifiedPago()){
            $this->sendPago($inscricao);
            $resposta->setNotifiedPago(true);
        }

        $respostaItens = $request->get('resposta')['respostaItens'];

        foreach ($request->files->getIterator() as $fileBag) {
            if (isset($fileBag['respostaItens'])){
                foreach ($fileBag['respostaItens'] as $file) {
                    $respostaItens[] = $file['file'];
                }
            }
        }


        foreach($respostaItens as $item){
            foreach($item as $perguntaId => $resp){
                if($perguntaId == 'is_anonimo')
                    break;

                // Se arquivo procurar id da pergunta
                if($perguntaId == 'file'){
                    foreach ($questionario->getPerguntas() as $pergunta){
                        if($pergunta->getType() == 'Vich\UploaderBundle\Form\Type\VichFileType'){
                            $perguntaId = $pergunta->getId();
                        }
                    }
                }

                /** @var Pergunta $pergunta */
                $pergunta = $em->getRepository('FCMQuestionBundle:Pergunta')->find($perguntaId);

                // Preencha respostas somente de um dado questionario
                if($pergunta->getQuestionario()->getId() != $questionario->getId()) break;

                if($pergunta->getType() == 'Symfony\Component\Form\Extension\Core\Type\CheckBoxType'){
                    $resp = serialize($resp);
                }


                $ri = new RespostaItem();
                $ri->setPergunta($pergunta);
                $ri->setResposta($resposta);
                $ri->setText($resp);
                $ri->setIsAnonimo(isset($item['is_anonimo']) ? $item['is_anonimo'] : 0);
                $resposta->addRespostaIten($ri);

                if($pergunta->getType() == 'Vich\UploaderBundle\Form\Type\VichFileType'){
                    $ri->setFile($item['file']);
                    $ri->setText('');
                }


                // Liga resposta com perguntaItemcalc
                /** @var PerguntaItem $perguntaIten */
                foreach ($pergunta->getPerguntaItens() as $perguntaIten){
                    if($perguntaIten->getTextInscricao() == $resp){
                        $ri->setPerguntaItem($perguntaIten);
                        $ri->setValor($perguntaIten->getValor());
                    }
                }

                // validacao CPEM
                if(!$ri->validateCPEM() and true === $this->get('security.authorization_checker')->isGranted('ROLE_PARTICIPANTE')) {
                    $this->addFlash('error','Verifique seu status ao CPEM. Para continuar selecione tipo de inscrição para não sócios.');
                    return false;
                }

                if(!$ri->validateLimEscolha() and true === $this->get('security.authorization_checker')->isGranted('ROLE_PARTICIPANTE')){
                    $this->addFlash('error','Você selecionou uma opção esgotada');
                    return false;
                }

                $em->persist($ri);
            }
        }


        $em->persist($resposta);
        $em->flush();

        return $resposta;
    }


    /**
     * Displays a form to edit an existing inscricao entity.
     *
     * @Route("/atividade/{eventoAtividade}/inscricao/{id}/edit/deposito", name="evento_inscricao_edit_deposito")
     * @Method({"GET", "POST"})
     */
    public function editDepositoAction(Request $request, EventoAtividade $eventoAtividade, Inscricao $inscricao)
    {

        if(!empty($request->get('back'))) $back = $request->get('back');
        else $back = $this->generateUrl('evento_atividade_index',['evento' => $eventoAtividade->getEvento()->getId()]);


        $em = $this->getDoctrine()->getManager();
        $resposta = $inscricao->getRespostaType(0)->first();
        $form = $this->createForm(RespostaDepositoType::class, $resposta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($inscricao);
            $em->flush();

            $this->addFlash('success','Inscrição atualizada com sucesso');

            if(!empty($request->get('back'))) return new RedirectResponse($back);
            else return $this->redirectToRoute('evento_atividade_index', array('evento' => $inscricao->getEventoAtividade()->getEvento()->getId()));
        }


        return $this->render('FCMQuestionBundle:Resposta:edit.deposito.html.twig', array(
            'page_title' => 'Informar depósito',
            'inscricao' => $inscricao,
            'deposito' => $eventoAtividade->getQuestionarioInscricao(),
            'delete' => true,
            'evento' => $eventoAtividade->getEvento(),
            'form' => $form->createView(),
            'questionario' => isset($questionario) ? $questionario : null,
            'atividade' => $eventoAtividade->getAtividade(),
            'back' => $back,
        ));
    }

    /**
     * Displays a form to edit an existing inscricao entity.
     *
     * @Route("/atividade/{eventoAtividade}/inscricao/{id}/edit", name="evento_inscricao_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, EventoAtividade $eventoAtividade, Inscricao $inscricao)
    {

        if(!empty($request->get('back'))) $back = $request->get('back');
        else $back = $this->generateUrl('evento_atividade_index',['evento' => $eventoAtividade->getEvento()->getId()]);


        $em = $this->getDoctrine()->getManager();

        $form = $this->getQuestionarioForm($eventoAtividade, $em, $inscricao, true);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if(isset($request->get('resposta')['respostaItens'])){
                foreach ($request->get('resposta')['respostaItens'] as $resposta){
                    foreach ($resposta as $perguntaId => $valor){
                        /** @var Pergunta $pergunta */
                        $pergunta = $em->getRepository('FCMQuestionBundle:Pergunta')->find($perguntaId);
                        $questionarios[$pergunta->getQuestionario()->getId()] = $pergunta->getQuestionario();
                    }
                }

                foreach ($questionarios as $questionario){
                    if(!$this->saveResposta($request, $em, $inscricao, $questionario)){
                        return $this->render('FCMEventoBundle:Inscricao:edit.html.twig', array(
                            'page_title' => 'Editar inscrição',
                            'inscricao' => $inscricao,
                            'deposito' => $eventoAtividade->getQuestionarioInscricao(),
                            'delete' => true,
                            'form' => $form->createView(),
                            'questionario' => isset($questionario) ? $questionario : null,
                            'atividade' => $eventoAtividade->getAtividade(),
                            'back' => $back,
                        ));
                    }

                }

            }

            $em->persist($inscricao);
            $em->flush();

            $this->sendInscricao($inscricao);
            $this->addFlash('success','Inscrição atualizada com sucesso');

            if(!empty($request->get('back'))) return new RedirectResponse($back);
            else return $this->redirectToRoute('evento_atividade_index', array('evento' => $inscricao->getEventoAtividade()->getEvento()->getId()));
        }

        $valores = $em->getRepository('FCMQuestionBundle:RespostaItem')->createQueryBuilder('a')
            ->select('a')
            ->leftJoin('FCMQuestionBundle:Resposta','b', 'WITH','a.resposta = b.id')
            ->andWhere('b.inscricao = :inscricao')->setParameter('inscricao', $inscricao->getId())
            ->andWhere('a.valor IS NOT NULL')
            ->getQuery()->getResult();


        return $this->render('FCMEventoBundle:Inscricao:edit.html.twig', array(
            'page_title' => 'Editar inscrição',
            'inscricao' => $inscricao,
            'deposito' => $eventoAtividade->getQuestionarioInscricao(),
            'delete' => true,
            'evento' => $eventoAtividade->getEvento(),
            'form' => $form->createView(),
            'valores' => $valores,
            'questionario' => isset($questionario) ? $questionario : null,
            'atividade' => $eventoAtividade->getAtividade(),
            'back' => $back,
        ));
    }

    /**
     * Confirm the delete
     * @Route("/atividade/{eventoAtividade}/inscricao/{id}/delete/confirm", name="evento_inscricao_confirmdelete")
     */
    public function deleteConfirmAction(Request $request, Inscricao $inscricao, EventoAtividade $eventoAtividade)
    {
        $form = $this->createFormBuilder()
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($inscricao);
            $em->flush();

            $this->sendCancelamento($inscricao);
            $this->addFlash('success','Cancelamento efetuado com sucesso');

            return $this->redirectToRoute('evento_atividade_index', [
                'evento' => $eventoAtividade->getEvento()->getId()
            ]);
        }

        return $this->render('FCMEventoBundle:Inscricao:delete-confirm.html.twig', array(
            'page_title' => $eventoAtividade->getEvento()->getNome() . ' - '  . $eventoAtividade->getNome() . ': Cancelar Inscrição',
            'title' => $eventoAtividade->getNome(),
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'form' => $form->createView(),
            'cancel_route' => $this->generateUrl('evento_inscricao_edit', [
                'evento' => $eventoAtividade->getEvento()->getId(),
                'eventoAtividade' => $eventoAtividade->getId(),
                'id' => $inscricao->getId()
            ])
        ));
    }



    /**
     * @param Inscricao $inscricao
     */
    public function sendCancelamento(Inscricao $inscricao){
        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Eventos] Inscrição cancelada com sucesso')
            ->setFrom($this->getParameter('mail'))
            ->setTo($inscricao->getParticipante()->getEmail())
            ->setReplyTo($inscricao->getEventoAtividade()->getEvento()->getEmail())
            ->setCc($inscricao->getEventoAtividade()->getEvento()->getEmail())
            ->setBody(
                $this->renderView('FCMEventoBundle:Emails:cancelamento.html.twig',[
                    'inscricao' => $inscricao,
                ]),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }

    /**
     * Envia e-mail de confirmacao de inscricao
     * @param Inscricao $inscricao
     */
    public function sendInscricao(Inscricao $inscricao){

        $em = $this->getDoctrine()->getManager();

        $valores = $em->getRepository('FCMQuestionBundle:RespostaItem')->createQueryBuilder('a')
            ->select('a')
            ->leftJoin('FCMQuestionBundle:Resposta','b', 'WITH','a.resposta = b.id')
            ->andWhere('b.inscricao = :inscricao')->setParameter('inscricao', $inscricao->getId())
            ->andWhere('a.valor IS NOT NULL')
            ->getQuery()->getResult();

        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Eventos] Inscrição realizada com sucesso')
            ->setFrom($this->getParameter('mail'))
            ->setTo($inscricao->getParticipante()->getEmail())
            ->setReplyTo($inscricao->getEventoAtividade()->getEvento()->getEmail())
            ->setCc($inscricao->getEventoAtividade()->getEvento()->getEmail())
            ->setBody(
                $this->renderView('FCMEventoBundle:Emails:inscricao.html.twig',[
                    'deposito' => $inscricao->getEventoAtividade()->getQuestionarioInscricao(),
                    'valores' =>$valores,
                    'inscricao' => $inscricao,
                ]),
                'text/html'
            );
        $this->get('mailer')->send($message);

    }

    /**
     * Envia e-mail de confirmacao de inscricao
     * @param Inscricao $inscricao
     */
    public function sendPago(Inscricao $inscricao){

        $em = $this->getDoctrine()->getManager();

        $valores = $em->getRepository('FCMQuestionBundle:RespostaItem')->createQueryBuilder('a')
            ->select('a')
            ->leftJoin('FCMQuestionBundle:Resposta','b', 'WITH','a.resposta = b.id')
            ->andWhere('b.inscricao = :inscricao')->setParameter('inscricao', $inscricao->getId())
            ->andWhere('a.valor > 0')
            ->getQuery()->getResult();

        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Eventos] Depósito confirmado com sucesso')
            ->setFrom($this->getParameter('mail'))
            ->setTo($inscricao->getParticipante()->getEmail())
            ->setReplyTo($inscricao->getEventoAtividade()->getEvento()->getEmail())
            ->setCc($inscricao->getEventoAtividade()->getEvento()->getEmail())
            ->setBody(
                $this->renderView('FCMEventoBundle:Emails:pago.html.twig',[
                    'deposito' => $inscricao->getEventoAtividade()->getQuestionarioInscricao(),
                    'valores' =>$valores,
                    'inscricao' => $inscricao,
                ]),
                'text/html'
            );
        $this->get('mailer')->send($message);

    }

    /**
     * Imprime inscricao
     *
     * @Route("/atividade/{eventoAtividade}/inscricao/{id}/export", name="evento_atividade_inscricao_export")
     * @Method({"GET", "POST"})
     */
    public function printInscricao(Inscricao $inscricao){
        $data = new \DateTime();
        
        $html =  $this->renderView('FCMEventoBundle:PDF:inscricao.html.twig', array(
            'eventoAtividade' => $inscricao->getEventoAtividade(),
            'inscricao' => $inscricao,
            'year' => new \DateTime()
        ));

        include_once('/site/config/MPDF60/mpdf.php');

        $mpdf = new \mPDF('utf-8', 'A4');
        $mpdf->SetHeader('Sistema de eventos - '.$inscricao->getEventoAtividade()->getEvento()->getNome() .' - '
            . $inscricao->getEventoAtividade()->getNome());
        $mpdf->SetFooter($data->format('d M Y H:i:s'));
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;

    }

}
