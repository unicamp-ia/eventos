<?php

namespace FCM\EventoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\Participante;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FCM\EventoBundle\Entity\Token;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Configura Wifi para visitantes
 *
 * @Route("admin/evento/{evento}")
 */
class WifiUnicampController extends Controller
{
    /**
     * Configura Wifi para visitantes
     *
     * @Route("/wifi", name="admin_evento_atividade_wifi")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Evento $evento)
    {
        return $this->render('FCMEventoBundle:Wifi:index.html.twig', array(
            'page_title' => 'Wifi Unicamp-Visitante',
            'page_title_desc' => 'Envie informações dos seus participantes para acessar o Wifi Unicamp-Visitante',
            'back' => $this->generateUrl('admin'),
            'evento' => $evento
        ));
    }

    /**
     * Gera arquivo CSV para importacao em lote para o WIFI
     *
     * @Route("/wifi/list", name="admin_evento_atividade_wifi_list")
     * @Method({"GET", "POST"})
     */
    public function generateCsvAction(Evento $evento)
    {
        $handle = fopen('php://output', 'r+');

        $participantes = new ArrayCollection();

        /** @var EventoAtividade $eventoAtividade */
        foreach ($evento->getEventoAtividades() as $eventoAtividade){

            /** @var Inscricao $inscricao */
            foreach ($eventoAtividade->getInscricoes() as $inscricao){
                $participantes->add($inscricao->getParticipante());
            }
        }

                $response = new Response();

        $sort = Criteria::create();
        $sort->orderBy(Array(
            'nome' => Criteria::ASC
        ));


        /** @var Participante $participante */
        foreach ($participantes->matching($sort) as $participante){
            fputcsv($handle, [
                preg_replace("/[^A-Za-z0-9?![:space:]]/","",$participante->getNome()),
                'CPF',
                preg_replace( '/[^0-9]/', '', $participante->getDocumento()),
                $participante->getEmail(),
                'FCM', // Empresa
                'FCM Evento',
                15
            ],';');
        }

        fclose($handle);

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="listawifi.csv"');

        return $response;
    }
}
