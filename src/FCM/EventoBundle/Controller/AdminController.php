<?php

namespace FCM\EventoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAdmin;
use FCM\EventoBundle\Repository\EventoRepository;
use FCM\UserBundle\Entity\User;
use FCM\UserBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     */
    public function adminAction(Request $request)
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

        /** @var EventoRepository $evento_repo */
        $evento_repo = $em->getRepository('FCMEventoBundle:Evento');

        $eventos = array();
        $eventos_aval = array();

        // Admins
        if (true === $this->get('security.authorization_checker')->isGranted('ROLE_17_0')){
            foreach($evento_repo->findBy(array(),['created' => 'DESC'],10) as $row)
                $eventos[$row->getId()] = $row;
        }

        // Docentes
        if (true === $this->get('security.authorization_checker')->isGranted('ROLE_DOCENTE')
            or true === $this->get('security.authorization_checker')->isGranted('ROLE_AVALIADOR')
        ){
            foreach($evento_repo->byEmPeriodoDeAvaliacao() as $row)
                $eventos_aval[$row->getId()] = $row;
        }

        foreach($evento_repo->byUser($user) as $row)
            $eventos[$row->getId()] = $row;

        if(!count($eventos) and !count($eventos_aval)){
            $this->addFlash('info','Você não possui nenhum evento, para criar utilize "Criar evento" no menu a esquerda');
        }
        $row1 = array();
        $row2 = array();
        $row3 = array();

        $number = 0;
        /** @var Evento $evento */
        foreach($eventos as $row) {

                if (count($row1) == count($row2) and count($row1) == count($row3)) {
                    $row1[] = $row;
                } elseif (count($row2) < count($row1) and count($row2) == count($row3)) {
                    $row2[] = $row;
                } elseif (count($row3) < count($row2) and count($row3) < count($row1)) {
                    $row3[] = $row;
                };

        }

        $form = $this->createFormBuilder()
            ->add('eventos', Select2EntityType::class, [
                'label' => '',
                'multiple' => false,
                'class' => 'FCMEventoBundle:Evento',
                'remote_route' => 'ajax_evento',
                'primary_key' => 'id',
                'text_property' => 'nome',
                'minimum_input_length' => 3,
                'page_limit' => 20,
                'allow_clear' => true,
                'required' => true,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => 'pt-br',
                'placeholder' => 'Evento',
            ])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted()){
            return $this->redirect($this->generateUrl('evento_show', ['id' => $request->get('form')['eventos']]));
        }

        return $this->render('FCMEventoBundle:Admin:admin.html.twig', array(
            'form' => $form->createView(),
            'col1' => $row1,
            'col2' => $row2,
            'col3' => $row3,
            'eventosAvaliacao' =>  $eventos_aval,
            'tabs' => $this->tabs($request),
        ));
    }

    /**
     * @Security("has_role('ROLE_17_1')")
     * @param Request $request
     * @param Evento $evento
     * @return array
     */
    public function tabs(Request $request){
        $routeName = $request->get('_route');
        return [
            'index' => [
                'label' => 'Meus Eventos',
                'url' => $this->generateUrl('admin'),
                'active' => $routeName == 'admin' ? true : false,
                'role' => 'ROLE_17_1'
            ],
        ];
    }



}
