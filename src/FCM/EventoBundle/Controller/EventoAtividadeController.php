<?php

namespace FCM\EventoBundle\Controller;

use FCM\EventoBundle\Entity\Atividade;
use FCM\EventoBundle\Entity\AtividadeToken;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\Token;
use FCM\QuestionBundle\Entity\Pergunta;
use FCM\QuestionBundle\Entity\Questionario;
use FCM\QuestionBundle\Entity\Resposta;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Eventoatividade controller.
 *
 * @Route("admin/evento/{evento}/atividade")
 */
class EventoAtividadeController extends Controller
{
    /**
     * Lists all token entities.
     *
     * @Route("/", name="admin_evento_atividade_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, Evento $evento)
    {
        $this->menu($request, $evento);
        $elements = $evento->getEventoAtividades();

        $rows = array();

        /** @var EventoAtividade $row */
        foreach($elements as $row){
            $rows[] = [$row->getId(), $row->getNome(),
                'action_links' => [
                    'edit' => [
                        'label' => '<i class="material-icons">create</i>',
                        'url' => $this->generateUrl('admin_evento_atividade_edit', [
                            'id' => $row->getId(),
                            'evento' => $row->getEvento()->getId()
                        ]),
                    ]
                ]
            ];
        }

        return $this->render('FCMEventoBundle:Default:list.html.twig', array(
            'page_title' => $evento->getNome() .' - Atividades',
            'action_links' => [
                'new' => [
                    'label' => 'Incluir Atividade',
                    'url' => $this->generateUrl('admin_evento_atividade_new', ['evento' => $evento->getId()]),
                ]
            ],
            'table_header' => ['#', 'Nome'],
            'table_rows' => $rows,
        ));
    }

    /**
     * Creates a new evento_atividade entity.
     *
     * @Route("/new", name="admin_evento_atividade_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Evento $evento)
    {
        $this->menu($request, $evento);
        $this->get('fcm_evento.main_menu')->getChild("Eventos")->addChild('Incluir atividade',
            array('route' => 'admin_evento_atividade_new', 'routeParameters' => ['evento' => $evento->getId()])
        );

        $evento_atividade = new EventoAtividade();

        $form = $this->createForm('FCM\EventoBundle\Form\EventoAtividadeType', $evento_atividade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $evento_atividade->setAtividade(null);
            $evento_atividade->setModelo($this->getParameter('modelo_default'));

            $em = $this->getDoctrine()->getManager();
            $evento_atividade->setEvento($evento);

            if($this->getParameter('aviso_new_evento_atividade'))
                $this->sendNotifyNew($evento_atividade);


            $em->persist($evento_atividade);
            $em->flush($evento_atividade);

            $this->addFlash('info', 'Atividade criada com sucesso, revise o nome da atividade e o modelo do certificado, após clique em salvar');

            return $this->redirectToRoute('admin_evento_atividade_edit', [
                'id' => $evento_atividade->getId(),
                'evento' => $evento->getId()
            ]);

        }

        return $this->render('FCMEventoBundle:EventoAtividade:edit.html.twig', array(
            'page_title' => 'Incluir atividade',
            'page_title_desc' => $evento->getNome(),
            'form' => $form->createView(),
            'isnew' => true,
            'back' => $this->generateUrl('evento_show', ['id' => $evento->getId()]),
        ));
    }

    /**
     * Displays a form to edit an existing evento_atividade entity.
     *
     * @Route("/{id}/edit", name="admin_evento_atividade_edit")
     * @Security("has_role('ROLE_17_1')")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, EventoAtividade $evento_atividade)
    {

        $atividade_orig = $evento_atividade->getAtividade();
        $form = $this->createForm('FCM\EventoBundle\Form\EventoAtividadeType', $evento_atividade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $evento_atividade->setAtividade($atividade_orig);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Alteração efetuada com sucesso');
            return $this->redirectToRoute('admin');
        }

        $perguntas = [
            'nome' => ['pergunta' => 'user.nome', 'nome' => 'Nome do participante'],
            'evento' => ['pergunta' => 'evento.nome', 'nome' => 'Nome do evento'],
            'local' => ['pergunta' => 'evento.local', 'nome' => 'Local do evento'],
            'data' => ['pergunta' => 'evento.data', 'nome' => 'Data de início do evento'],
            'duracao' => ['pergunta' => 'evento.duracao', 'nome' => 'Duração do evento (em horas)']
        ];

        /** @var Questionario $questionario */
        if($questionario = $evento_atividade->getQuestionarioInscricao()) {
            /** @var Pergunta $pergunta */
            foreach ($questionario->getPerguntas() as $pergunta) {
                $perguntas[$pergunta->getId()] = ['pergunta' => 'pergunta.' .$pergunta->getId(), 'nome' => $pergunta->getNome()];
            }
        }


        if($evento_atividade->getAtividade() instanceof Atividade) {
            if ($atividadeTokens = $evento_atividade->getAtividade()->getAtividadeTokens()){

                /** @var AtividadeToken $atividadeToken */
                foreach ($atividadeTokens as $atividadeToken) {
                    $token = $atividadeToken->getToken();
                    $perguntas[$token->getId()] = ['pergunta' => 'token.' . $token->getId(), 'nome' => $token->getNome()];
                }
            }
        }

        $func = array();

        $action_links['presenca'] = [
            'label' => 'Inscrições',
            'url' => $this->generateUrl('admin_evento_atividade_presenca_index',[
                'evento' => $evento_atividade->getEvento()->getId(),'eventoAtividade' => $evento_atividade->getId(),
            ])
        ];

        $action_links['avaliador'] = [
            'label' => 'Avaliadores',
            'url' => $this->generateUrl('admin_evento_avaliador_index', ['evento' => $evento_atividade->getEvento()->getId()]),
        ];

        // FUNCIONALIDADES
        /** @var Questionario $questionario */
        if($type0 = $evento_atividade->getQuestionarioInscricao()){
            $func[0] = $this->generateUrl('admin_evento_atividade_questionario_edit',[
                'id' => $type0->getId(),
                'eventoAtividade' => $evento_atividade->getId(),
                'evento' => $evento_atividade->getEvento()->getId(),
            ]);
        } else $func[0] = $this->generateUrl('admin_evento_atividade_questionario_new',[
            'eventoAtividade' => $evento_atividade->getId(),
            'evento' => $evento_atividade->getEvento()->getId(),
            'type' => 0,
        ]);

        // Etiqueta
        $func[5] = $this->generateUrl('admin_eventoAtividade_etiqueta_edit',['eventoAtividade' => $evento_atividade->getId()]);

        if($type1 = $evento_atividade->getAtividadeBoleto()){
            $func[1] = $this->generateUrl('admin_evento_atividade_boletofuncamp_edit',[
                'id' => $type1->getId(),
                'eventoAtividade' => $evento_atividade->getId(),
                'evento' => $evento_atividade->getEvento()->getId(),
            ]);
        } else $func[1] = $this->generateUrl('admin_evento_atividade_boletofuncamp_new',[
            'eventoAtividade' => $evento_atividade->getId(),
            'evento' => $evento_atividade->getEvento()->getId(),
        ]);

        // Feedback
        if($type3 = $evento_atividade->getQuestionarioPosInscricao()){
            $func[3] = $this->generateUrl('admin_evento_atividade_questionario_edit',[
                'id' => $type3->getId(),
                'eventoAtividade' => $evento_atividade->getId(),
                'evento' => $evento_atividade->getEvento()->getId(),
            ]);
        } else $func[3] = $this->generateUrl('admin_evento_atividade_questionario_new',[
            'eventoAtividade' => $evento_atividade->getId(),
            'evento' => $evento_atividade->getEvento()->getId(),
            'type' => 2,
        ]);

        // Avaliacao
        if($type4 = $evento_atividade->getQuestionarioAvaliador()){
            $func[4] = $this->generateUrl('admin_evento_atividade_questionario_edit',[
                'id' => $type4->getId(),
                'eventoAtividade' => $evento_atividade->getId(),
                'evento' => $evento_atividade->getEvento()->getId(),
            ]);
        } else $func[4] = $this->generateUrl('admin_evento_atividade_questionario_new',[
            'eventoAtividade' => $evento_atividade->getId(),
            'evento' => $evento_atividade->getEvento()->getId(),
            'type' => 1,
        ]);


        return $this->render('FCMEventoBundle:EventoAtividade:edit.html.twig', array(
            'page_title' => 'Editar atividade',
            'form' => $form->createView(),
            'func' => $func,
            'evento' => $evento_atividade->getEvento(),
            'eventoAtividade' => $evento_atividade,
            'action_links' => $action_links,
            'isnew' => false,
            'delete' => $this->get('router')->generate('admin_evento_atividade_confirmdelete', [
                'id' => $evento_atividade->getId(),
                'evento' => $evento_atividade->getEvento()->getId()
            ]),
            'perguntas' => $perguntas,
            'back' => $this->get('router')->generate('evento_show', ['id' => $evento_atividade->getEvento()->getId()]),
        ));
    }

    /**
     * Confirm the delete
     * @Route("/{id}/delete/confirm", name="admin_evento_atividade_confirmdelete")
     */
    public function deleteConfirmAction(Request $request, EventoAtividade $evento_atividade)
    {
        /** @var EventoAtividade $eventoAtividade */
        if($evento_atividade->getInscricoes()->count()){
            $this->addFlash('error','Essa atividade possui '
                . $evento_atividade->getInscricoes()->count() .' inscrições. Cancele todas as inscrições antes de excluir a atividade');
            return $this->redirectToRoute('admin_evento_atividade_edit',[
                'id' => $evento_atividade->getId(),
                'evento' => $evento_atividade->getEvento()->getId(),
            ]);
        }


        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_evento_atividade_delete', array(
                    'id' => $evento_atividade->getId(),
                    'evento' => $evento_atividade->getEvento()->getId()
                )
            ))
            ->setMethod('DELETE')
            ->getForm();

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('aluno_disciplina_index');
        }

        return $this->render('FCMEventoBundle:Default:delete-confirm.html.twig', array(
            'page_title' => 'Excluir',
            'title' => $evento_atividade->getNome(),
            'form' => $form->createView(),
            'cancel_route' => $this->generateUrl('admin_evento_atividade_index', ['evento' => $evento_atividade->getEvento()->getId()])
        ));
    }

    /**
     * Deletes an entity.
     *
     * @Route("/{id}/delete", name="admin_evento_atividade_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, EventoAtividade $evento_atividade, Evento $evento)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($evento_atividade);
        $em->flush();
        $this->addFlash('success','Exclusão efetuada com sucesso');

        return $this->redirectToRoute('admin_evento_atividade_index', ['evento' => $evento->getId()]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @param Request $request
     * @param Evento $evento
     * @return array
     */
    public function menu(Request $request, Evento $evento){
        $this->get('fcm_evento.main_menu')->getChild("Eventos")->addChild($evento->getNome(),
            array('route' => 'evento_show', 'routeParameters' => ['id' => $evento->getId()])
        );

        /** @var EventoAtividade $eventoAtividade */
        foreach($evento->getEventoAtividades() as $eventoAtividade){
            $this->get('fcm_evento.main_menu')->getChild("Eventos")->getChild($evento->getNome())->addChild($eventoAtividade->getNome(),
                array('route' => 'admin_evento_atividade_presenca_index', 'routeParameters' => [
                    'evento' => $evento->getId(),
                    'eventoAtividade' => $eventoAtividade->getId()
                ])
            );
        }
    }

    /**
     * Displays a form to edit an existing evento_atividade entity.
     *
     * @Route("/{id}/message", name="admin_evento_atividade_message")
     * @Method({"GET", "POST"})
     */
    public function messageAction(Request $request, EventoAtividade $evento_atividade){

        if($evento_atividade->getQuestionarioAvaliador()){
            if($evento_atividade->getQuestionarioAvaliador()->getAproveRequired()){
                $form = $this->createFormBuilder()
                    ->add('assunto', TextType::class, [
                            'label' => 'Assunto',
                            'data' => '[IA/Eventos] ' . 'Mensagem de ' . $evento_atividade->getEvento()->getNome() . ': ']
                    )
                    ->add('message', CKEditorType::class, [
                        'label' => 'Mensagem',
                        'required' => true,
                    ])
                    ->add('approve',CheckboxType::class,[
                        'label' => 'Enviar mensagem somente para trabalhos aprovados',
                        'required' => false,
                ])->getForm();
            } else {
                $form = $this->createFormBuilder()
                    ->add('assunto', TextType::class, [
                            'label' => 'Assunto',
                            'data' => '[IA/Eventos] ' . 'Mensagem de ' . $evento_atividade->getEvento()->getNome() . ': ']
                    )
                    ->add('message', CKEditorType::class, [
                        'label' => 'Mensagem',
                        'required' => true,
                    ])->getForm();
            }
        } else {
            $form = $this->createFormBuilder()
                ->add('assunto', TextType::class, [
                        'label' => 'Assunto',
                        'data' => '[IA/Eventos] ' . 'Mensagem de ' . $evento_atividade->getEvento()->getNome() . ': ']
                )
                ->add('message', CKEditorType::class, [
                    'label' => 'Mensagem',
                    'required' => true,
                ])->getForm();
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mails = array();


            /** @var Inscricao $row */
            foreach($evento_atividade->getInscricoes() as $row){

                $send = true;

                // Se definido para exibir somente ao aprovados o sitema deve verificar
                if(isset($request->get('form')['approve'])){

                    /** @var Resposta $resposta */
                    $resposta = $row->getRespostaType(0)->first();
                    if(!$resposta->isApproved()){
                        $send = false;
                    }
                }

                if($send) $mails[] = $row->getParticipante()->getEmail();

            }

            $this->sendMessageAction($evento_atividade, $mails,
                $form->get('message')->getViewData(),$form->get('assunto')->getViewData()
            );

            $this->addFlash('success',count($mails) . ' mensagens enviada com sucesso');
            return $this->redirectToRoute('admin_evento_atividade_presenca_index', [
                'evento' => $evento_atividade->getEvento()->getId(),
                'eventoAtividade' => $evento_atividade->getId()
            ]);
        }

        return $this->render('FCMEventoBundle:EventoAtividade:message.html.twig', array(
            'page_title' => 'Envio de mensagem aos participantes',

            'eventoAtividade' => $evento_atividade,
            'evento' => $evento_atividade->getEvento(),
            'inscricoes' => $evento_atividade->getInscricoes(),
            'questionarios' => $evento_atividade->getQuestionarios(),
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_evento_atividade_presenca_index', [
                'evento' => $evento_atividade->getEvento()->getId(),
                'eventoAtividade' => $evento_atividade->getId(),
            ]),
        ));
    }

    /**
     * Caso parametro aviso_new_evento_atividade envia e-mail quando atividade for criada
     * @param EventoAtividade $eventoAtividade
     */
    public function sendNotifyNew(EventoAtividade $eventoAtividade){
        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Eventos] Novo evento/atividade criado')
            ->setFrom($this->getParameter('mail'))
            ->setTo($this->getParameter('aviso_new_evento_atividade'))
            ->setReplyTo($eventoAtividade->getEvento()->getEmail())
            ->setBody(
                $this->renderView('FCMEventoBundle:Emails:notify.new.html.twig',[
                    'eventoAtividade' => $eventoAtividade,
                    'title' => 'Novo evento/atividade criado',
                ]),
                'text/html'
            );

        $this->get('mailer')->send($message);
    }

    /**
     * Envia mensagem para um grupo de usuarios
     * @param EventoAtividade $eventoAtividade
     * @param array $mails
     * @param $message
     */
    public function sendMessageAction(EventoAtividade $eventoAtividade, Array $mails, $message, $subject){
        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->getParameter('mail'))
            ->setTo($eventoAtividade->getEvento()->getEmail())
            ->setReplyTo($eventoAtividade->getEvento()->getEmail())
            ->setBody(
                $this->renderView('FCMEventoBundle:Emails:message.html.twig',[
                    'message' => $message,
                    'subject' => $subject,
                    'eventoAtividade' => $eventoAtividade,
                ]),
                'text/html'
            );

        foreach($mails as $mail){
            $message->addBcc($mail);
        }

        $this->get('mailer')->send($message);
    }

}
