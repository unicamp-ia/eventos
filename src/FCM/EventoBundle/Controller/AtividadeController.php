<?php

namespace FCM\EventoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use FCM\EventoBundle\Entity\Atividade;
use FCM\EventoBundle\Entity\AtividadeToken;
use FCM\EventoBundle\Repository\AtividadeTokenRepository;
use FCM\EventoBundle\Repository\TokenRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Atividade controller.
 *
 * @Route("admin/atividade")
 */
class AtividadeController extends Controller
{
    /**
     * Lists all token entities.
     *
     * @Route("/", name="admin_atividade_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $elements = $em->getRepository('FCMEventoBundle:Atividade')->findAll();

        $rows = array();

        /** @var Atividade $row */
        foreach($elements as $row){
            $rows[] = [
                $row->getId(),
                $row->getActive() ? 'A' : 'I',
                $row->getNome(),
                'action_links' => [
                    'edit' => [
                        'label' => '<i class="material-icons">create</i>',
                        'url' => $this->generateUrl('admin_atividade_edit', array('id' => $row->getId())),
                    ]
                ]
            ];
        }

        return $this->render('FCMEventoBundle:Default:list.html.twig', array(
            'page_title' => 'Atividades',
            'action_links' => [
                'new' => [
                    'label' => 'Incluir atividade',
                    'url' => $this->generateUrl('admin_atividade_new'),
                ]
            ],
            'table_header' => ['#','Ativo|Inativo', 'Nome'],
            'table_rows' => $rows,
        ));
    }

    /**
     * Creates a new atividade entity.
     *
     * @Route("/new", name="admin_atividade_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $atividade = new Atividade();
        $atividade->setActive(true);

        $form = $this->createForm('FCM\EventoBundle\Form\AtividadeType', $atividade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $tokens = $form->get('tokens')->getViewData();

            /** @var TokenRepository $token_repo */
            $token_repo = $em->getRepository('FCMEventoBundle:Token');

            foreach($tokens as $token_id){
                $atividade_token = new AtividadeToken();
                $atividade_token->setAtividade($atividade);
                $atividade_token->setToken($token_repo->find($token_id));
                $em->persist($atividade_token);
            }

            $em->persist($atividade);
            $em->flush();

            return $this->redirectToRoute('admin_atividade_index', array('id' => $atividade->getId()));
        }

        return $this->render('FCMEventoBundle:Atividade:edit.html.twig', array(
            'page_title' => 'Atividades - Incluir',
            'atividade' => $atividade,
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_atividade_index'),
        ));
    }

    /**
     * Finds and displays a atividade entity.
     *
     * @Route("/{id}", name="admin_atividade_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Atividade $atividade)
    {
        return $this->render('FCMEventoBundle:Atividade:show.html.twig', array(
            'page_title' => 'Atividades - ' .$atividade->getNome(),
            'entity' => $atividade,
        ));
    }

    /**
     * Displays a form to edit an existing atividade entity.
     *
     * @Route("/{id}/edit", name="admin_atividade_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Atividade $atividade)
    {
        $original = new ArrayCollection();
        /** @var AtividadeToken $atividade_token */
        foreach ($atividade->getAtividadeTokens() as $atividade_token) {
            $original->add($atividade_token);
        }

        $em = $this->getDoctrine()->getManager();

        /** @var AtividadeTokenRepository $atividadetoken_repo */
        $atividadetoken_repo = $em->getRepository('FCMEventoBundle:AtividadeToken');

        /** @var TokenRepository $token_repo */
        $token_repo = $em->getRepository('FCMEventoBundle:Token');

        $form = $this->createForm('FCM\EventoBundle\Form\AtividadeType', $atividade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tokens = $form->get('tokens')->getViewData();

            /** @var AtividadeToken $atividadeToken */
            foreach ($original as $atividadeToken) {
                // Procura itens removidos
                if (false === array_search($atividadeToken->getToken()->getId(), $tokens)) {
                    $atividade_token = $atividadetoken_repo->find($atividadeToken->getId());
                    $atividade->getAtividadeTokens()->removeElement($atividade_token);
                    $em->remove($atividadeToken);
                }
            }

            // Insere novos elementos
            foreach($tokens as $token){
                if(is_null($atividadetoken_repo->findOneBy(['atividade' => $atividade->getId(), 'token' => $token]))){
                    $atividadeToken = new AtividadeToken();
                    $atividadeToken->setAtividade($atividade);
                    $atividadeToken->setToken($token_repo->find($token));
                    $atividade->getAtividadeTokens()->add($atividadeToken);
                    $em->persist($atividadeToken);
                }
            }

            $em->persist($atividade);
            $em->flush();

            return $this->redirectToRoute('admin_atividade_index');
        }

        return $this->render('FCMEventoBundle:Atividade:edit.html.twig', array(
            'page_title' => 'Atividades - Editar ' .$atividade->getNome(),
            'form' => $form->createView(),
            'delete' => $this->generateUrl('admin_atividade_confirmdelete', ['id' => $atividade->getId()]),
            'back' => $this->generateUrl('admin_atividade_index'),
        ));
    }

    /**
     * Confirm the delete
     * @Route("/{id}/delete/confirm", name="admin_atividade_confirmdelete")
     */
    public function deleteConfirmAction(Request $request, Atividade $atividade)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_atividade_delete', array(
                    'id' => $atividade->getId(),
                )
            ))
            ->setMethod('DELETE')
            ->getForm();

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('aluno_disciplina_index');
        }

        return $this->render('FCMEventoBundle:Default:delete-confirm.html.twig', array(
            'page_title' => 'Atividades - Excluir ' .$atividade->getNome(),
            'title' => $atividade->getNome(),
            'form' => $form->createView(),
            'cancel_route' => $this->generateUrl('admin_atividade_index')
        ));
    }

    /**
     * Deletes an entity.
     *
     * @Route("/{id}/delete", name="admin_atividade_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Atividade $atividade)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($atividade);
        $em->flush();
        $this->addFlash('success','Exclusão efetuada com sucesso');

        return $this->redirectToRoute('admin_atividade_index');
    }
}
