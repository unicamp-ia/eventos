<?php

namespace FCM\EventoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Ldap\LdapClient;
use Symfony\Component\Security\Core\Authentication\Provider\LdapBindAuthenticationProvider;

class AuthController extends Controller
{
    /**
     * @Route("/participante/login", name="participante_login")
     */
    public function participanteLoginAction(Request $request)
    {
        if($request->get('documento')){
            $documento = $request->get('documento');
        } else $documento = '';
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if($request->get('destination')){
            $target = $request->get('destination');
        } else {
            $target  = $this->generateUrl('home');
        }

        return $this->render('FCMEventoBundle:Auth:login.user.html.twig', array(
            'page_title' => 'Login',
            'last_username' => $lastUsername,
            'error' => $error,
            'documento' => $documento,
            'body_class' => 'login-form',
            'target' => $target,
            'login' => $this->generateUrl('participante_login'),
        ));
    }

    /**
     * @Route("ajax/participante/login", name="ajax_participante_login",options={"expose"=true})
     */
    public function ajaxParticipanteLoginAction(Request $request)
    {
        if($request->get('documento')){
            $documento = $request->get('documento');
        } else $documento = '';
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if($request->get('destination')){
            $target = $request->get('destination');
        } else {
            $target  = $this->generateUrl('home');
        }

        $template = $this->render('FCMEventoBundle:Auth:block.auth.html.twig', array(
            'page_title' => 'Login',
            'last_username' => $lastUsername,
            'error' => $error,
            'documento' => $documento,
            'body_class' => 'login-form',
            'target' => $target,
            'login' => $this->generateUrl('participante_login'),
        ));

        return new JsonResponse($template->getContent());
    }


    /**
     * @Route("/auth/participante", name="auth_participante")
     */
    public function participanteAuthAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        if($error) {
            return $this->redirectToRoute('home');
        }

    }

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if($request->get('destination')){
            $target = $request->get('destination');
        } else {
            $target  = $this->generateUrl('admin');
        }

        return $this->render('FCMEventoBundle:Auth:login.admin.html.twig', array(
            'target' => $target,
            'login' => $this->generateUrl('login'),
            'page_title' => 'Login',
            'last_username' => $lastUsername,
            'error' => $error,
            'body_class' => 'login-form',
        ));
    }

    /**
     * @Route("/auth/admin", name="auth_admin")
     */
    public function adminAuthAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        if($error) {
            return $this->redirectToRoute('login');
        }

        return $this->render('FCMEventoBundle:Auth:login.admin.html.twig', array(
            'page_title' => 'Login',
            'last_username' => $lastUsername,
            'error' => $error,
            'body_class' => 'login-form',
        ));

    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request){
        $this->get('security.token_storage')->setToken(null);
        $this->get('request')->getSession()->invalidate();
    }

    /**
     * @Route("/logout", name="evento_auth_reset")
     */
    public function resetAction(Request $request){
        $this->get('security.token_storage')->setToken(null);
        $this->get('request')->getSession()->invalidate();
    }


}
