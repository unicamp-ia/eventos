<?php

namespace FCM\EventoBundle\Controller;

use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Participante;
use FCM\EventoBundle\Repository\ParticipanteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Choice;

/**
 * Evento controller.
 *
 * @Route("")
 */
class EventoController extends Controller
{
    /**
     * Lists all token entities.
     *
     * @Route("/admin/evento", name="admin_evento_index")
     * @Security("has_role('ROLE_17_1')")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $elements = $em->getRepository('FCMEventoBundle:Evento')->findAll();

        $rows = array();


        /** @var Evento $row */
        foreach($elements as $row){
            $rows[] = [$row->getId(), $row->getNome(),
                'action_links' => [
                    'edit' => [
                        'label' => '<i class="material-icons">create</i>',
                        'url' => $this->generateUrl('admin_evento_edit', array('id' => $row->getId())),
                    ]
                ]
            ];
        }

        return $this->render('FCMEventoBundle:Default:list.html.twig', array(
            'action_links' => [
                'new' => [
                    'label' => 'Incluir evento',
                    'url' => $this->generateUrl('admin_evento_new'),
                ]
            ],
            'table_header' => ['#', 'Nome'],
            'table_rows' => $rows,
        ));
    }

    /**
     * Procura participante para inscricao manual
     *
     * @Route("/admin/ajax/evento", name="ajax_evento",options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function ajaxEventoAction(Request $request){

        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('FCMEventoBundle:Evento')->createQueryBuilder('a')
            ->select('a');

        $query->andWhere($query->expr()->like('a.nome',':nome'))->setParameter('nome','%'.$request->get('q').'%');
        $rows = $query->getQuery()->getResult();

        $resp = array();
        /** @var Evento $row */
        foreach ($rows as $row){

            // Incluir somente eventos em que ele seja admin
            //if($row->isAdmin($this->getUser())){
                $resp[] = [
                    'id' => $row->getId(),
                    'text' => $row->getNome(),
                ];
            //}

        }

        return new JsonResponse($resp);

    }

    /**
     * Creates a new evento entity.
     *
     * @Route("/admin/evento/new", name="admin_evento_new")
     * @Security("has_role('ROLE_17_1')")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $evento = new Evento();
        $form = $this->createForm('FCM\EventoBundle\Form\EventoType', $evento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $evento->setCreated(new \DateTime());
            $evento->setCreatedBy($user);
            $em->persist($evento);

            $em->flush($evento);
            $this->addFlash('success','Seu evento foi criado com sucesso, agora você deve criar uma atividade');

            return $this->redirectToRoute('admin_evento_atividade_new', ['evento' => $evento->getId()]);
        }

        return $this->render('FCMEventoBundle:Evento:edit.html.twig', array(
            'page_title' =>'Criar evento',
            'evento' => $evento,
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin'),
        ));
    }

    /**
     * Finds and displays a evento entity.
     *
     * @Route("/evento/{id}", name="evento_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Evento $evento)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        // Inscricao de atividades privadas
        if($request->get('at')){
            /** @var EventoAtividade $eventoAtividade */
            $eventoAtividade = $em->getRepository('FCMEventoBundle:EventoAtividade')->find($request->get('at'));


            if($eventoAtividade instanceof EventoAtividade){
                if($eventoAtividade->checkTokenInscricao($request->get('token'))){
                    $this->get('session')->set('privateEventoAtividade', $eventoAtividade->getId());
                    $this->addFlash('success','Você está habilitado a se inscrever para a atividade: ' .$eventoAtividade->getNome());
                } else {
                    $this->addFlash('error','Token inválido para a atividade');
                }
            } else {
                $this->addFlash('error','Atividade não encontrada');
            }

        }

        /** @var ParticipanteRepository $repo_part */
        $repo_part = $em->getRepository('FCMEventoBundle:Participante');
        $translator = $this->get('translator');

        $form = $this->createFormBuilder()
            ->add('cpf', TextType::class, [
                'label' => 'Insira seu CPF',
                'required' => false,
            ])
            ->add('passport', TextType::class, [
                'label' => 'Passport (foreigners only)',
                'required' => false,

            ])
            ->add('submit', SubmitType::class, ['label' => $translator->trans('realizarinscric')])
            ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted()) {

            // CPF
            if($form->get('cpf')->getViewData()) {
                $documento = $form->get('cpf')->getViewData();

                $documento = str_pad($documento, 11, "0", STR_PAD_LEFT);

                if (!Participante::validateCPF($documento)) {
                    $this->addFlash('error', 'CPF incorreto, verifique e tente novamente');
                    return $this->render('FCMEventoBundle:Evento:show.html.twig', array(
                        'page_title' => $evento->getNome(),
                        'form' => $form->createView(),
                        'entity' => $evento,
                        'back' => $this->generateUrl('home'),
                    ));
                }
            } else {
                $documento = $form->get('passport')->getViewData();
            }


            /** @var Participante $participante */
            $participante = $repo_part->findOneBy(['documento' => $documento]);

            if($participante instanceof Participante){


                $this->addFlash('success','Participante encontrado com sucesso, insira sua senha');
                return $this->redirectToRoute('participante_login', [
                    'documento' => $documento,
                    'destination' => $this->generateUrl('evento_atividade_index', ['evento' => $evento->getId()])]);

            } else {
                $this->addFlash('success','Participante não encontrado, proceda para o cadastro de participantes');
                return $this->redirectToRoute('participante_new', [
                    'documento' => $documento,
                    'destination' => $this->generateUrl('evento_atividade_index',['evento' => $evento->getId()])
                ]);
            }
        }

        return $this->render('FCMEventoBundle:Evento:show.html.twig', array(
            'page_title' => $evento->getNome(),
            'form' => $form->createView(),
            'entity' => $evento,
            'evento' => $evento,
            'back' => $this->generateUrl('home'),
        ));
    }

    /**
     * Encontra evento a partir de um alias
     *
     * @Route("/{alias}", name="evento_alias")
     */
    public function showAliasAction(Request $request, $alias)
    {
        $evento = $this->get('doctrine')->getRepository('FCMEventoBundle:Evento')->findOneBy(['alias' => $alias]);

        if($evento instanceof Evento)
            return $this->redirectToRoute('evento_show',['id' => $evento->getId()]);

        $this->addFlash('error','Evento não encontrado');
        return $this->redirectToRoute('home');
    }

    /**
     * Displays a form to edit an existing evento entity.
     *
     * @Route("/admin/evento/{id}/edit", name="admin_evento_edit")
     * @Security("has_role('ROLE_17_1')")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Evento $evento)
    {
        $form = $this->createForm('FCM\EventoBundle\Form\EventoType', $evento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $evento->setCreated(new \DateTime());
            $em->persist($evento);

            $em->flush();

            return $this->redirectToRoute('evento_show', ['id' => $evento->getId()]);
        }

        return $this->render('FCMEventoBundle:Evento:edit.html.twig', array(
            'page_title' => $evento->getNome() .' - Editar',
            'form' => $form->createView(),
            'action_links' => [
                'newatividade' => [
                    'url' => $this->generateUrl('admin_evento_atividade_new', ['evento' => $evento->getId()]),
                    'label' => 'Incluir atividade'
                ],
                'admin' => [
                    'url' => $this->generateUrl('admin_evento_admin_index', ['evento' => $evento->getId()]),
                    'label' => 'Administradores'
                ],
            ],
            'evento' => $evento,
            'delete' => $this->generateUrl('admin_evento_confirmdelete', ['id' => $evento->getId()]),
            'back' => $this->generateUrl('evento_show', ['id' => $evento->getId()]),
        ));
    }

    /**
     * Confirm the delete
     * @Route("/admin/evento/{id}/delete/confirm", name="admin_evento_confirmdelete")
     * @Security("has_role('ROLE_17_1')")
     */
    public function deleteConfirmAction(Request $request, Evento $evento)
    {
        /** @var EventoAtividade $eventoAtividade */
        foreach ($evento->getEventoAtividades() as $eventoAtividade){
            if($eventoAtividade->getInscricoes()->count()){
                $this->addFlash('error','Cancele todas as inscrições antes de excluir um evento');
                return $this->redirectToRoute('admin_evento_edit',['id' => $evento->getId()]);
            }
        }

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_evento_delete', array(
                    'id' => $evento->getId(),
                )
            ))
            ->setMethod('DELETE')
            ->getForm();

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('aluno_disciplina_index');
        }

        return $this->render('FCMEventoBundle:Default:delete-confirm.html.twig', array(
            'page_title' => 'Excluir',
            'title' => $evento->getNome(),
            'form' => $form->createView(),
            'cancel_route' => $this->generateUrl('admin')
        ));
    }

    /**
     * Deletes an entity.
     *
     * @Route("/admin/evento/{id}/delete", name="admin_evento_delete")
     * @Security("has_role('ROLE_17_1')")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Evento $evento)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($evento);
        $em->flush();
        $this->addFlash('success','Exclusão efetuada com sucesso');

        return $this->redirectToRoute('admin');
    }

    /**
     * Deletes an entity.
     *
     */
    public function cardAdminAction(Request $request, EventoAtividade $eventoAtividade){
        return $this->render('FCMEventoBundle:Evento:card.admin.html.twig', array(
            'eventoAtividade' => $eventoAtividade,
        ));
    }



    /**
     * Deletes an entity.
     */
    public function cardAvaliadorAction(Request $request, Evento $evento){
        return $this->render('FCMEventoBundle:Evento:card.avaliador.html.twig', array(
            'evento' => $evento,
        ));
    }


}