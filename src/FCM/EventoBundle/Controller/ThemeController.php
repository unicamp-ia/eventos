<?php

namespace FCM\EventoBundle\Controller;

use FCM\EventoBundle\Entity\Participante;
use FCM\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ThemeController extends Controller
{

    public function headerAction($simplified = false)
    {
        $result = null;
        $nome = 'Não logado';
        $tipo = null;
        $home = $this->generateUrl('participante_login');

        /** @var User $user */
        if($user = $this->get('security.token_storage')->getToken()){
            $user = $this->get('security.token_storage')->getToken()->getUser();
        }

        if ($this->get('security.authorization_checker')->isGranted('ROLE_PARTICIPANTE')) {
            $nome = $user->getParticipante()->getNome();
            $tipo = 'Participante';
            $home = $this->generateUrl('home');
        }

        // Administrador
        if ($this->get('security.authorization_checker')->isGranted('ROLE_17_1')) {
            $nome = $user->getNome();
            $tipo = 'Administrador';
            $home = $this->generateUrl('admin');
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_DOCENTE')){
            $nome = $user->getNome();
            $tipo = 'Administrador e Avaliador';
            $home = $this->generateUrl('admin');
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_AVALIADOR')){
            $nome = $user->getNome();
            $tipo = 'Avaliador';
            $home = $this->generateUrl('admin');
        }

        return $this->render('FCMEventoBundle:Theme:header.html.twig', array(
            'simplified' => $simplified,
            'user' => $result,
            'nome' => $nome,
            'tipo' => $tipo,
            'home' => $home,
        ));
    }

}
