<?php

namespace FCM\EventoBundle\Controller;

use FCM\EventoBundle\Entity\Avaliacao;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\InscricaoToken;
use FCM\EventoBundle\Repository\EventoRepository;
use FCM\UserBundle\Entity\User;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Avaliacao controller.
 *
 * @Route("")
 */
class AvaliacaoController extends Controller
{


    /**
     * Lista os trabalhos para avaliacao
     *
     * @Route("/admin/{evento}/atividade/{eventoAtividade}/avaliacao", name="admin_evento_atividade_avaliacao_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request, EventoAtividade $eventoAtividade)
    {
        $this->menu($request, $eventoAtividade->getEvento());
        $this->get('fcm_evento.main_menu')->getChild("Eventos")->getChild($eventoAtividade->getEvento()->getNome())
            ->getChild($eventoAtividade->getNome())->setCurrent(true);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        // Verifica se eh administrador
        if($eventoAtividade->getEvento()->getCreatedBy() == $user){
            $page_title_desc = 'Administre os trabalhos avaliados';
            $admin = true;
        } else {
            $page_title_desc = 'Selecione os trabalhos que deseja avaliar';
            $admin = false;
        }

        $area[''] = 'Todos';

        /** @var Inscricao $inscricao */
        foreach($eventoAtividade->getInscricoes() as $inscricao){
            $rows[$inscricao->getId()]['usuario'] = $inscricao->getParticipante();
            $rows[$inscricao->getId()]['aval'] = $inscricao->getRespostaType(1)->count();
            $rows[$inscricao->getId()]['inscricao_id'] = $inscricao->getId();

            $rows[$inscricao->getId()]['status'] = 0;

            // Verifica se atingiu o numero maximo de avaliacoes
            if($inscricao->getRespostaType(1)->count() >= $eventoAtividade->getMaxNotas())
                $rows[$inscricao->getId()]['status'] = 2;

            // Verifica se ja foi avaliado por aquele revisor
            if($inscricao->isAvaliadoPor($user))
                $rows[$inscricao->getId()]['status'] = 1;


            /** @var InscricaoToken $inscricaoToken */
            foreach($inscricao->getInscricaoTokens() as $inscricaoToken){

                if(is_numeric(strpos($inscricaoToken->getAtividadeToken()->getToken()->getNome(),'Título')))
                    $rows[$inscricao->getId()]['titulo'] = $inscricaoToken->getVal();


                if(is_numeric(strpos($inscricaoToken->getAtividadeToken()->getToken()->getNome(),'Área'))) {
                    $rows[$inscricao->getId()]['area'] = $inscricaoToken->getVal();
                    $area[$inscricaoToken->getVal()] = $inscricaoToken->getVal();
                }
            }
        }

        $form = $this->createFormBuilder()
            ->add('area', ChoiceType::class,['required' => false,'label' => 'Área','choices' => array_flip($area),'required' => false,])
            ->add('filter', SubmitType::class, array('label' => 'Filtrar'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $resp = $form->getData();

            if($resp['area'] != '') {
                foreach ($rows as $key => $row) {
                    if ($row['area'] != $resp['area']) unset($rows[$key]);
                }
            }
        }

        /** @var Paginator $paginator */
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($rows, $request->query->getInt('page', 1), 1000);

        return $this->render('FCMEventoBundle:Avaliacao:index.html.twig', array(
            'max_notas' => $eventoAtividade->getMaxNotas(),
            'page_title_desc' => $eventoAtividade->getEvento()->getNome() . ' - ' . $eventoAtividade->getNome(),
            'page_title' => 'Avaliar trabalhos',
            'admin' => $admin,
            'questionario' => $eventoAtividade->getQuestionarioAvaliador(),
            'pagination' => $pagination,
            'form' => $form->createView(),
            'back' => empty($request->get('back')) ? $this->generateUrl('admin') : $request->get('back')

        ));
    }

    /**
     * Creates a new avaliacao entity.
     *
     * @Route("/admin/evento/{evento}/atividade/{eventoAtividade}/inscricao/{inscricao}/avaliacao/new",
     * name="admin_evento_atividade_inscricao_avaliacao_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Inscricao $inscricao)
    {
        $avaliacao = new Avaliacao();
        $form = $this->createForm('FCM\EventoBundle\Form\AvaliacaoType', $avaliacao);
        $form->handleRequest($request);
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $avaliacao->setCreated(new \DateTime());
            $avaliacao->setAvaliador($user);
            $avaliacao->setInscricao($inscricao);

            $em->persist($avaliacao);
            $em->flush($avaliacao);

            $this->addFlash('success','Avaliação efetuada com sucesso');

            return $this->redirectToRoute('admin_evento_atividade_avaliacao_index', [
                'evento' => $inscricao->getEventoAtividade()->getEvento()->getId(),
                'eventoAtividade' => $inscricao->getEventoAtividade()->getId(),
            ]);
        }

        return $this->render('FCMEventoBundle:Avaliacao:edit.html.twig', array(
            'page_title' => $inscricao->getEventoAtividade()->getEvento()->getNome() . ' - ' .
                $inscricao->getEventoAtividade()->getNome() . ' - Avaliação',
            'avaliacao' => $avaliacao,
            'trabalho' => $inscricao->getTrabalho(),
            'titulo' => $inscricao->getTitulo(),
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_evento_atividade_avaliacao_index', [
                'evento' => $inscricao->getEventoAtividade()->getEvento()->getId(),
                'eventoAtividade' => $inscricao->getEventoAtividade()->getId(),
            ])
        ));
    }

    /**
     * Finds and displays a inscricao entity.
     *
     * @Route("/admin/evento/{evento}/atividade/{eventoAtividade}/inscricao/{id}", name="admin_evento_atvidade_inscricao_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Inscricao $inscricao)
    {
        $this->menu($request, $inscricao->getEventoAtividade()->getEvento());
        $this->get('fcm_evento.main_menu')->getChild("Eventos")->getChild($inscricao->getEventoAtividade()->getEvento()->getNome())
            ->getChild($inscricao->getEventoAtividade()->getNome())->setCurrent(true);

        return $this->render('FCMEventoBundle:Avaliacao:show.html.twig', array(
            'page_title' => $inscricao->getEventoAtividade()->getEvento()->getNome() . ' - '
            . $inscricao->getEventoAtividade()->getNome(),
            'titulo' => $inscricao->getTitulo(),
            'autor' => $inscricao->getAutor(),
            'trabalho' => $inscricao->getTrabalho(),
            'inscricao' => $inscricao,
            'back' => $this->generateUrl('admin_evento_atividade_avaliacao_index', [
                'evento' => $inscricao->getEventoAtividade()->getEvento()->getId(),
                'eventoAtividade' => $inscricao->getEventoAtividade()->getId(),
            ])
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @param Request $request
     * @param Evento $evento
     * @return array
     */
    public function menu(Request $request, Evento $evento){
        $this->get('fcm_evento.main_menu')->getChild("Eventos")->addChild($evento->getNome(),
            array('route' => 'evento_show', 'routeParameters' => ['id' => $evento->getId()])
        );

        /** @var EventoAtividade $eventoAtividade */
        foreach($evento->getEventoAtividades() as $eventoAtividade){
            $this->get('fcm_evento.main_menu')->getChild("Eventos")->getChild($evento->getNome())->addChild($eventoAtividade->getNome(),
                array('route' => 'admin_evento_atividade_presenca_index', 'routeParameters' => [
                    'evento' => $evento->getId(),
                    'eventoAtividade' => $eventoAtividade->getId()
                ])
            );
        }
    }


}

