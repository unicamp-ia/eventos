<?php

namespace FCM\EventoBundle\Controller;

use FCM\EventoBundle\Entity\Token;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Token controller.
 *
 * @Route("admin/token")
 */
class TokenController extends Controller
{
    /**
     * Lists all token entities.
     *
     * @Route("/", name="admin_token_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $elements = $em->getRepository('FCMEventoBundle:Token')->findAll();

        $rows = array();

        /** @var Token $row */
        foreach($elements as $row){
            $rows[] = [$row->getId(), $row->getNome(),
                'action_links' => [
                    'edit' => [
                        'label' => '<i class="material-icons">create</i>',
                        'url' => $this->generateUrl('admin_token_edit', array('id' => $row->getId())),
                    ]
                ]
            ];
        }

        return $this->render('FCMEventoBundle:Default:list.html.twig', array(
            'page_title' => 'Token',
            'action_links' => [
                'new' => [
                    'label' => 'Incluir token',
                    'url' => $this->generateUrl('admin_token_new'),
                ]
            ],
            'table_header' => ['#', 'Nome'],
            'table_rows' => $rows,
        ));
    }

    /**
     * Creates a new token entity.
     *
     * @Route("/new", name="admin_token_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $token = new Token();
        $form = $this->createForm('FCM\EventoBundle\Form\TokenType', $token);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($token);
            $em->flush($token);

            return $this->redirectToRoute('admin_token_index', array('id' => $token->getId()));
        }

        return $this->render('FCMEventoBundle:Token:edit.html.twig', array(
            'token' => $token,
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_token_index'),
        ));
    }

    /**
     * Finds and displays a token entity.
     *
     * @Route("/{id}", name="admin_token_show")
     * @Method("GET")
     */
    public function showAction(Token $token)
    {
        return $this->render('FCMEventoBundle:Token:show.html.twig', array(
            'entity' => $token,
        ));
    }

    /**
     * Displays a form to edit an existing token entity.
     *
     * @Route("/{id}/edit", name="admin_token_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Token $token)
    {
        $form = $this->createForm('FCM\EventoBundle\Form\TokenType', $token);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_token_index');
        }

        return $this->render('FCMEventoBundle:Token:edit.html.twig', array(
            'page_title' => 'Editar token ' . $token->getNome(),
            'form' => $form->createView(),
            'delete' => $this->generateUrl('admin_token_confirmdelete', ['id' => $token->getId()]),
            'back' => $this->generateUrl('admin_token_index'),
        ));
    }

    /**
     * Confirm the delete
     * @Route("/{id}/delete/confirm", name="admin_token_confirmdelete")
     */
    public function deleteConfirmAction(Request $request, Token $token)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_token_delete', array(
                    'id' => $token->getId(),
                )
            ))
            ->setMethod('DELETE')
            ->getForm();

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('aluno_disciplina_index');
        }

        return $this->render('FCMEventoBundle:Default:delete-confirm.html.twig', array(
            'page_title' => 'Excluir',
            'title' => $token->getNome(),
            'form' => $form->createView(),
            'cancel_route' => $this->generateUrl('admin_token_index')
        ));
    }

    /**
     * Deletes an entity.
     *
     * @Route("/{id}/delete", name="admin_token_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Token $token)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($token);
        $em->flush();
        $this->addFlash('success','Exclusão efetuada com sucesso');

        return $this->redirectToRoute('admin_token_index');
    }
}
