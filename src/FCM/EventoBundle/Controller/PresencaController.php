<?php

namespace FCM\EventoBundle\Controller;

use Doctrine\ORM\EntityManager;
use FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto;
use FCM\EventoBundle\Entity\Atividade;
use FCM\EventoBundle\Entity\AtividadeToken;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\InscricaoToken;
use FCM\EventoBundle\Entity\Participante;
use FCM\EventoBundle\Entity\Presenca;
use FCM\EventoBundle\Form\PresencaType;
use FCM\EventoBundle\Repository\InscricaoRepository;
use FCM\QuestionBundle\Entity\Pergunta;
use FCM\QuestionBundle\Entity\PerguntaItem;
use FCM\QuestionBundle\Entity\Questionario;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\QuestionBundle\Entity\RespostaItem;
use FCM\UserBundle\Entity\User;
use FCM\UserBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 * Presenca controller.
 *
 * @Route("admin/evento/{evento}/atividade/{eventoAtividade}/presenca")
 */
class PresencaController extends Controller
{
    /**
     * Lists all presenca entities.
     *
     * @Route("", name="admin_evento_atividade_presenca_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request, EventoAtividade $eventoAtividade)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var InscricaoRepository $inscr_repo */
        $inscr_repo = $em->getRepository('FCMEventoBundle:Inscricao');

        $inscricoes = $inscr_repo->findBy(['eventoAtividade' => $eventoAtividade->getId()]);


        /** @var contador de presentes $presentes */
        $presentes = 0;

        if($questionario = $eventoAtividade->getQuestionarioAvaliador()){
            $action_links = $this->generateUrl('questionario_avaliacao', [
                'questionario' => $questionario->getId(),
                'back' => $this->generateUrl('admin_evento_atividade_presenca_index',[
                    'evento' => $eventoAtividade->getEvento()->getId(),
                    'eventoAtividade' => $eventoAtividade->getId(),
            ])
            ]);
        } else $action_links = array();


        return $this->render('FCMEventoBundle:Presenca:index.html.twig', array(
            'page_title' => '',
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'questionarios' => $eventoAtividade->getQuestionarios()->getValues(),
            'isPago' => $eventoAtividade->getAtividadeBoleto() instanceof EventoAtividadeBoleto ? true : false,
            'inscricoes' => $inscricoes,
            'trabalhos' => $action_links,
            'presentes' => $presentes,
            'back' => $this->generateUrl('admin_evento_atividade_edit', [
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId(),
            ]),
        ));
    }

    /**
     * Lists all presenca entities.
     *
     * @Route("/resumo", name="admin_evento_atividade_presenca_resumo")
     * @Method({"GET", "POST"})
     */
    public function resumoAction(Request $request, EventoAtividade $eventoAtividade)
    {


        return $this->render('FCMEventoBundle:Presenca:resumo.html.twig', array(
            'page_title' => '',
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'questionarios' => $eventoAtividade->getQuestionarios()->getValues(),
            'back' => $this->generateUrl('admin_evento_atividade_edit', [
                'evento' => $eventoAtividade->getEvento()->getId(),
                'id' => $eventoAtividade->getId(),
            ]),
        ));
    }

    /**
     * @Route("/ocupacao", name="admin_evento_atividade_presenca_ocupacao")
     * @Method({"GET", "POST"})
     */
    public function ocupacacaoAction(Request $request, EventoAtividade $eventoAtividade)
    {

        $pagantes = 0;

        /** @var Inscricao $inscricoe */
        foreach ($eventoAtividade->getInscricoes() as $inscricoe){
            /** @var Resposta $resposta */
            foreach ($inscricoe->getRespostas() as $resposta){
                if($resposta->getPago()) $pagantes++;
            }


        }

        return $this->render('FCMEventoBundle:Presenca:ocupacao.html.twig',[
            'page_title' => $eventoAtividade->getEvento()->getNome() . ' - '
                .  $eventoAtividade->getNome() . ' - ' . 'Acompanhamento de vagas',
            'pagantes' => $pagantes,
            'back' => $this->generateUrl('admin_evento_atividade_presenca_index',[
                'evento' => $eventoAtividade->getEvento()->getId(),
                'eventoAtividade' => $eventoAtividade->getId(),
            ]),
            'eventoAtividae' => $eventoAtividade,
            'evento' => $eventoAtividade->getEvento(),
            'questionario' => $eventoAtividade->getQuestionarioInscricao(),
        ]);
    }



    /**
     * @Route("/ocupacao/{perguntaItem}", name="admin_evento_atividade_presenca_ocupacao_detail", options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function ocupacaoDetailsAction(Request $request, PerguntaItem $perguntaItem, EventoAtividade $eventoAtividade)
    {

        return $this->render('FCMEventoBundle:Presenca:ocupacao.perguntaItem.html.twig',[
            'page_title' => $eventoAtividade->getEvento()->getNome() . ' - '
                .  $eventoAtividade->getNome() . ' - ' . $perguntaItem->getTextInscricao(),
            'eventoAtividae' => $eventoAtividade,
            'perguntaItem' => $perguntaItem,
            'back' => $this->generateUrl('admin_evento_atividade_presenca_ocupacao',[
                'eventoAtividade' => $eventoAtividade->getId(),
                'evento' => $eventoAtividade->getEvento()->getId(),
                ]),
        ]);
    }

    /**
     * Procura participante para inscricao manual
     *
     * @Route("/{inscricao}/ajax", name="admin_evento_atividade_presenca_ajax",options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function ajaxPresencaAction(Request $request, Inscricao $inscricao){

        if(!$inscricao->getPresenca()) {
            $inscricao->setPresenca(true);
            $inscricao->setLiberado(true);
            $inscricao->setToken();
            $this->sendPresencaAction($inscricao);
        } else {
            $inscricao->setPresenca(false);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($inscricao);
        $em->flush();

        /** @var Response $template */
        $template = $this->render('FCMThemeBundle:Default:ajax.ok.html.twig',[
            'inscricao' => $inscricao
        ]);

        $json = json_encode($template->getContent());
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    /**
     * Procura participante para inscricao manual
     *
     * @Route("/{inscricao}/ajax/button", name="admin_evento_atividade_presenca_ajax_button",options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function ajaxPresencaButtonAction(Request $request, Inscricao $inscricao){

        if(!$inscricao->getPresenca()) {
            $inscricao->setPresenca(true);
            $inscricao->setLiberado(true);
            $inscricao->setToken();
            $this->sendPresencaAction($inscricao);
            $em = $this->getDoctrine()->getManager();
            $em->persist($inscricao);
            $em->flush();
        }

        /** @var Response $template */
        $template = $this->render('FCMEventoBundle:Presenca:button.certificado.html.twig',[
            'inscricao' => $inscricao,
        ]);

        $json = json_encode($template->getContent());
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    /**
     * Procura participante para inscricao manual
     *
     * @Route("/inscrever", name="admin_evento_atividade_presenca_inscrever")
     * @Method({"GET", "POST"})
     */
    public function inscreverAction(Request $request, EventoAtividade $eventoAtividade){
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm('FCM\EventoBundle\Form\PresencaInscreverType');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $documento = $form->get('documento')->getViewData();
            $email = $form->get('email')->getViewData();

            if($documento){
                // Verifica CPF
                if(!Usuario::valCPF($documento) and !$request->get('estrangeiro')){
                    $this->addFlash('error', 'Verifique o CPF e tente novamente');
                    return $this->redirectToRoute('admin_evento_atividade_presenca_inscrever',[
                        'evento' =>$eventoAtividade->getEvento()->getId(),
                        'eventoAtividade' => $eventoAtividade->getId(),
                    ]);
                }

                $participante = $em->getRepository('FCMEventoBundle:Participante')->findOneBy(['documento' => $documento]);
            // Caso de e-mail
            } else {
                $participante = $em->getRepository('FCMEventoBundle:Participante')->findOneBy(['email' => $email]);
            }

            if($participante instanceof Participante){
                $this->addFlash('success','Participante '. $participante->getNome() . ' encontrado');
                return $this->redirectToRoute('evento_inscricao_new', [
                    'evento' => $eventoAtividade->getEvento()->getId(),
                    'eventoAtividade' => $eventoAtividade->getId(),
                    'participante' => $participante->getId()
                ]);
            } else {
                $this->addFlash('info','Participante não encontrado proceda para o cadastro');
                return $this->redirectToRoute('admin_evento_atividade_presenca_inscrever_auto', [
                    'evento' => $eventoAtividade->getEvento()->getId(),
                    'eventoAtividade' => $eventoAtividade->getId(),
                    'documento' => $documento,
                    'email' => $email,
                ]);
            }
        }

        return $this->render('FCMEventoBundle:Presenca:inscrever.html.twig', array(
            'page_title' => $eventoAtividade->getEvento()->getNome() . ' ' . $eventoAtividade->getNome() . ' - Inscrever participante',
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_evento_atividade_presenca_index', [
                'evento' => $eventoAtividade->getEvento()->getId(),
                'eventoAtividade' => $eventoAtividade->getId()
            ]),
        ));
    }

    /**
     * Creates a new participante entity.
     *
     * @Route("/inscrever/auto", name="admin_evento_atividade_presenca_inscrever_auto")
     * @Method({"GET", "POST"})
     */
    public function newAutoAction(Request $request, EventoAtividade $eventoAtividade)
    {
        $participante = new Participante();
        if($request->get('documento')){
            $participante->setDocumento($request->get('documento'));
        }

        if($request->get('email')){
            $participante->setEmail($request->get('email'));
        }

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm('FCM\EventoBundle\Form\PresencaInscreverAutoType',$participante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = new User();
            $user->setVinculo('Eventos');

            if($participante->getDocumento()) {
                $user->setUsername($participante->getDocumento());
            } else {
                $user->setUsername($participante->getEmail());
            }

            $em->persist($user);
            $participante->setUser($user);

            $pass = $participante->setRandomPass();
            $em->persist($participante);

            $em->flush();

            $this->sendNewAutoAction($participante, $eventoAtividade, $pass);
            $this->addFlash('success','Participante '. $participante->getNome() . ' cadastrado com sucesso. Um e-mail orientação foi enviado.');

            return $this->redirectToRoute('evento_inscricao_new', [
                'evento' => $eventoAtividade->getEvento()->getId(),
                'eventoAtividade' => $eventoAtividade->getId(),
                'participante' => $participante->getId()
            ]);
        }

        return $this->render('FCMEventoBundle:Presenca:inscrever.auto.html.twig', array(
            'page_title' => $eventoAtividade->getEvento()->getNome() . ' ' . $eventoAtividade->getNome() . ' - Inscrever participante',
            'evento' => $eventoAtividade->getEvento(),
            'eventoAtividade' => $eventoAtividade,
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_evento_atividade_presenca_index', [
                'evento' => $eventoAtividade->getEvento()->getId(),
                'eventoAtividade' => $eventoAtividade->getId()
            ]),
        ));
    }

    /**
     * @param Participante $participante
     * @param EventoAtividade $eventoAtividade
     */
    public function sendNewAutoAction(Participante $participante, EventoAtividade $eventoAtividade, $pass){
        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Eventos] Cadastro como participante efetuado com sucesso')
            ->setFrom($this->getParameter('mail'))
            ->setReplyTo($eventoAtividade->getEvento()->getEmail())
            ->setCc($eventoAtividade->getEvento()->getEmail())
            ->setTo($participante->getEmail())
            ->setBody(
                $this->renderView('FCMEventoBundle:Emails:presenca.new.auto.html.twig',[
                    'participante' => $participante,
                    'pass' => $pass,
                    'eventoAtividade' => $eventoAtividade,
                    'link' => $this->generateUrl('home', array(), true)
                ]),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }

    /**
     * Exporta PDF com todas as inscricoes
     *
     * @Route("/export", name="admin_evento_atividade_presenca_export_index")
     * @Method({"GET", "POST"})
     */
    public function printInscricoes(EventoAtividade $eventoAtividade){
        $data = new \DateTime();
        $em = $this->getDoctrine()->getManager();

        /** @var InscricaoRepository $inscr_repo */
        $inscr_repo = $em->getRepository('FCMEventoBundle:Inscricao');

        $inscricoes = $inscr_repo->findBy(['eventoAtividade' => $eventoAtividade->getId()]);

        $html =  $this->renderView('FCMEventoBundle:PDF:inscricoes.html.twig', array(
            'eventoAtividade' => $eventoAtividade,
            'inscricoes' => $inscricoes,
            'year' => new \DateTime()
        ));

        include_once('/site/config/MPDF60/mpdf.php');

        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
        ]);
        $mpdf->SetHeader('Sistema de eventos - '.$eventoAtividade->getEvento()->getNome() .' - '. $eventoAtividade->getNome());
        $mpdf->SetFooter($data->format('d M Y H:i:s'). ' - Pág.{PAGENO} ');

        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }

    /**
     * @param Inscricao $inscricao
     */
    public function sendPresencaAction(Inscricao $inscricao){
        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Eventos] Liberação do certificado: ' . $inscricao->getEventoAtividade()->getEvento()->getNome())
            ->setFrom($this->getParameter('mail'))
            ->setReplyTo($inscricao->getEventoAtividade()->getEvento()->getEmail())
            ->setCc($inscricao->getEventoAtividade()->getEvento()->getEmail())
            ->setTo($inscricao->getParticipante()->getEmail())
            ->setBody(
                $this->renderView('FCMEventoBundle:Emails:presenca.html.twig',[
                    'inscricao' => $inscricao,
                    'link' => $this->generateUrl('certificado_show', [
                        'token' => $inscricao->getToken(),
                    ],UrlGenerator::ABSOLUTE_URL)
                ]),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }

    /**
     * @Route("/lista/aprovacao", name="admin_evento_atividade_presenca_lista_aprovacao")
     * @param EventoAtividade $eventoAtividade
     */
    public function listaAprovacaoAction(EventoAtividade $eventoAtividade){
        /** @var \PHPExcel $phpExcelObject */
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $this->printDadosParticipantes($phpExcelObject,$eventoAtividade, true);
        $this->printPagamento($phpExcelObject, $eventoAtividade);
        $this->printDadosInscricao($phpExcelObject,$eventoAtividade);

        if($eventoAtividade->getQuestionarioAvaliador()){
            $this->printAvaliacao($phpExcelObject, $eventoAtividade);
        }

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'lista-de-aprovacao.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;

    }

    /**
     * @Route("/lista/byoptions", name="admin_evento_atividade_presenca_lista_byoptions")
     * @param EventoAtividade $eventoAtividade
     */
    public function listaByOptionsAction(EventoAtividade $eventoAtividade){
        /** @var \PHPExcel $phpExcelObject */
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $this->printDadosParticipantes($phpExcelObject,$eventoAtividade);
        $this->printPagamento($phpExcelObject, $eventoAtividade);
        $this->printDadosInscricao($phpExcelObject,$eventoAtividade);

        if($eventoAtividade->getQuestionarioAvaliador()){
            $this->printAvaliacao($phpExcelObject, $eventoAtividade);
        }

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'lista-de-presenca.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    /**
     * @Route("/lista", name="admin_evento_atividade_presenca_lista")
     * @param EventoAtividade $eventoAtividade
     */
    public function listaAction(EventoAtividade $eventoAtividade){
        /** @var \PHPExcel $phpExcelObject */
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $this->printDadosParticipantes($phpExcelObject,$eventoAtividade);
        $this->printPagamento($phpExcelObject, $eventoAtividade);
        $this->printDadosInscricao($phpExcelObject,$eventoAtividade);

        if($eventoAtividade->getQuestionarioAvaliador()){
            $this->printAvaliacao($phpExcelObject, $eventoAtividade);
        }

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'lista-de-presenca.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    /**
     * @param \PHPExcel $phpExcelObject
     * @param EventoAtividade $eventoAtividade
     * @throws \PHPExcel_Exception
     */
    function printDadosParticipantes(\PHPExcel &$phpExcelObject, EventoAtividade $eventoAtividade, $isAprovacao = false)
    {
        $row = 5;

        $phpExcelObject->getActiveSheet()
            ->setCellValue('A1', $eventoAtividade->getEvento()->getNome())
            ->setCellValue('A2', $eventoAtividade->getNome())
            ->setCellValue('A4', 'Inscrição')
            ->setCellValue('B4', 'Documento')
            ->setCellValue('C4', 'Nome')
            ->setCellValue('D4', 'E-mail');

        /** @var Inscricao $inscricao */
        foreach ($eventoAtividade->getInscricoes() as $inscricao) {
            $nextCell = 'A';
            $phpExcelObject->getActiveSheet()->setCellValue($nextCell . $row, $inscricao->getId());

            if($isAprovacao){
                // Se aprovacao visualizar trabalho e notas
                $link = $this->generateUrl('questionario_inscricao_avaliacao_show', [
                    'inscricao' => $inscricao->getId(),
                    'eventoAtividade' => $eventoAtividade->getId(),
                    'questionario' => $eventoAtividade->getQuestionarioAvaliador()->getId(),
                ], UrlGenerator::ABSOLUTE_URL);
            } else {
                // Senao editar inscricao
                $link = $this->generateUrl('evento_inscricao_edit', [
                    'id' => $inscricao->getId(),
                    'eventoAtividade' => $eventoAtividade->getId(),
                    'evento' => $eventoAtividade->getEvento()->getId()
                ], UrlGenerator::ABSOLUTE_URL);
            }


            // Visualizar a resposta
            $phpExcelObject->getActiveSheet()->getCell($nextCell . $row)
                ->getHyperlink()->setUrl($link);

            $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell . $row, $inscricao->getParticipante()->getDocumento());
            $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell . $row, $inscricao->getParticipante()->getNome());
            $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell . $row, $inscricao->getParticipante()->getEmail());
            $row++;
        }

        // Style
        $maxColumn = $phpExcelObject->getActiveSheet()->getHighestColumn();
        $phpExcelObject->getActiveSheet()->mergeCells('A1:'.$maxColumn.'1');
        $phpExcelObject->getActiveSheet()->mergeCells('A2:'.$maxColumn.'2');
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $phpExcelObject->getActiveSheet()->getCell('A1')->getStyle()->getFont()->setSize('18')->setBold(true);
        $phpExcelObject->getActiveSheet()->getCell('A2')->getStyle()->getFont()->setSize('14')->setBold(true);

        $phpExcelObject->getActiveSheet()->getCell('A4')->getStyle()->getFont()->setSize('12')->setBold(true);
        $phpExcelObject->getActiveSheet()->getCell('B4')->getStyle()->getFont()->setSize('12')->setBold(true);
        $phpExcelObject->getActiveSheet()->getCell('C4')->getStyle()->getFont()->setSize('12')->setBold(true);
        $phpExcelObject->getActiveSheet()->getCell('D4')->getStyle()->getFont()->setSize('12')->setBold(true);
    }

    /**
     * @param \PHPExcel $phpExcelObject
     * @param EventoAtividade $eventoAtividade
     * @throws \PHPExcel_Exception
     */
    function printDadosInscricao(\PHPExcel &$phpExcelObject, EventoAtividade $eventoAtividade){

        $em = $this->getDoctrine();

        $active = $phpExcelObject->getActiveSheet();

        $qInscricao = $eventoAtividade->getQuestionarioInscricao();
        $nextColumn = $active->getHighestColumn(4);

        if(!$qInscricao) return;

        /** @var Pergunta $pergunta */
        foreach ($qInscricao->getPerguntas() as $pergunta){
            // Preenche com o texto da pergunta
            $active->getCell(++$nextColumn.'4')->setValue($pergunta->getNome());

            $phpExcelObject->getActiveSheet()->getCell($nextColumn.'4')
                ->getStyle()->getFont()->setBold(true);

            /**
             * Para cada pergunta eh pesquisado se possui alguma resposta associada para uma dada inscricao
             * @var \PHPExcel_Worksheet_Row $row */
            foreach ($phpExcelObject->getActiveSheet()->getRowIterator(5) as $row){
                $inscricaoId = $active->getCellByColumnAndRow(0, $row->getRowIndex())->getValue();

                /** @var RespostaItem $respostaItem */
                $respostaItem = $em->getRepository('FCMQuestionBundle:RespostaItem')
                    ->createQueryBuilder('a')
                    ->join('a.resposta', 'b','WITH')
                    ->andWhere('a.pergunta = :pergunta')->setParameter('pergunta', $pergunta->getId())
                    ->andWhere('b.inscricao = :inscricao')->setParameter('inscricao',$inscricaoId)
                    ->getQuery()->getResult();

                /** @var RespostaItem $item */
                foreach ($respostaItem as $item){

                    $text = strip_tags($item->getText());
                    $text = html_entity_decode($text);
                    $active->getCell($nextColumn.$row->getRowIndex())
                        ->setValue(substr($text,0, 200));
                }

            }
        }


    }

    /**
     * Imprima informacoes sobre deposito
     * @param \PHPExcel $phpExcelObject
     * @param EventoAtividade $eventoAtividade
     * @throws \PHPExcel_Exception
     */
    function printPagamento(\PHPExcel &$phpExcelObject, EventoAtividade $eventoAtividade){
        $em = $this->getDoctrine();

        if(!$eventoAtividade->getQuestionarioInscricao()) return;
        if(!$eventoAtividade->getQuestionarioInscricao()->getDeposito()) return;

        $active = $phpExcelObject->getActiveSheet();

        $nextColumn = $active->getHighestColumn(4);

        $active->getCell($nextColumn.'4')->setValue('Depósito');
        /**
         * Para cada pergunta eh pesquisado se possui alguma resposta associada para uma dada inscricao
         * @var \PHPExcel_Worksheet_Row $row */
        foreach ($phpExcelObject->getActiveSheet()->getRowIterator(5) as $row) {
            $inscricaoId = $active->getCellByColumnAndRow(0, $row->getRowIndex())->getValue();

            $resposta = $em->getRepository('FCMQuestionBundle:Resposta')
                ->findOneBy(['inscricao' => $inscricaoId,
                    'questionario' => $eventoAtividade->getQuestionarioInscricao()->getId()]);

            if($resposta){
                $active->getCell($nextColumn.$row->getRowIndex())->setValue($resposta->isPagoLabel());
            }


        }
    }

    /**
     * @param \PHPExcel $phpExcelObject
     * @param EventoAtividade $eventoAtividade
     * @throws \PHPExcel_Exception
     */
    function printAvaliacao(\PHPExcel &$phpExcelObject, EventoAtividade $eventoAtividade){

        $em = $this->getDoctrine();

        $active = $phpExcelObject->getActiveSheet();


        $qAvaliacao = $eventoAtividade->getQuestionarioAvaliador();
        $nextColumn = $active->getHighestColumn(4);

        /** @var Questionario $questionario */
        if($qAvaliacao and $qAvaliacao->getAproveRequired()){

            $phpExcelObject->getActiveSheet()
                ->getCell(++$nextColumn.'4')->setValue('Trabalho liberado');
            $phpExcelObject->getActiveSheet()->getCell($nextColumn.'4')
                ->getStyle()->getFont()->setBold(true);

            /**
             * Para cada pergunta eh pesquisado se possui alguma resposta associada
             * @var \PHPExcel_Worksheet_Row $row */
            foreach ($phpExcelObject->getActiveSheet()->getRowIterator(5) as $row) {
                $inscricaoId = $active->getCellByColumnAndRow(0, $row->getRowIndex())->getValue();
                $inscricao = $em->getRepository('FCMEventoBundle:Inscricao')->find($inscricaoId);
                $nextColumn = $active->getHighestColumn($row->getRowIndex());
                $nextColumn++;


                /** @var Resposta $resposta */
                foreach($inscricao->getRespostaType(0) as $resposta){
                    $active->getCell($nextColumn++.$row->getRowIndex())
                        ->setValue($resposta->isApprovedLabel());
                }

                // Imprime resposta dos avaliadores
                foreach($inscricao->getRespostaType(1) as $resposta){
                    /** @var RespostaItem $respostaIten */
                    foreach ($resposta->getRespostaItens() as $respostaIten){
                        $active->getCell($nextColumn++.$row->getRowIndex())
                            ->setValue($respostaIten->getText());
                    }

                }

            }

        }



    }


    /**
     * @param \PHPExcel $phpExcelObject
     * @param EventoAtividade $eventoAtividade
     * @param null $questionario
     * @throws \PHPExcel_Exception
     */
    function printQuestionarioAvaliacao(\PHPExcel &$phpExcelObject, EventoAtividade $eventoAtividade, $questionario = null){
        $comments = array();

        if($questionario) {
            switch ($questionario->getType()) {
                case 0:
                    $phpExcelObject->setActiveSheetIndex(0)->setTitle('Inscrições');
                    break;
                case 1:
                    $phpExcelObject->setActiveSheetIndex(1)->setTitle('Avaliações');
                    break;
            }
        }

        $phpExcelObject->getActiveSheet()
            ->setCellValue('A1',$eventoAtividade->getEvento()->getNome())
            ->setCellValue('A2',$eventoAtividade->getNome())
            ->setCellValue('A4','Inscrição')
            ->setCellValue('B4','Nome')
            ->setCellValue('C4','E-mail');

        $nextCell = 2;

        /** @var Questionario $questionario */
        if($questionario){
            // Se aprovacao eh necessaria
            if($questionario->getAproveRequired()){
                $phpExcelObject->getActiveSheet()
                    ->setCellValueExplicitByColumnAndRow(++$nextCell,4,'Trabalho liberado');
            }

            //Imprime inscricao, somente campo com opcoes
            foreach ($questionario->getEventoAtividade()->getQuestionarioInscricao()->getPerguntas() as $pergunta){
                if($pergunta->getType() == 'Symfony\Component\Form\Extension\Core\Type\ChoiceType'){
                    $phpExcelObject->getActiveSheet()
                        ->setCellValueExplicitByColumnAndRow(++$nextCell,4,$pergunta->getNome())
                        ->getStyle()->getFont()->setBold(true);
                }

            }

            /** @var Pergunta $pergunta */
            foreach($questionario->getPerguntas() as $pergunta){
                // Imprime perguntas
                $phpExcelObject->getActiveSheet()
                    ->setCellValueExplicitByColumnAndRow(++$nextCell,4,$pergunta->getNome())
                    ->getStyle()->getFont()->setBold(true);
            }
        }


        $row = 5;

        /** @var Inscricao $inscricao */
        foreach($eventoAtividade->getInscricoes() as $inscricao){
            $nextCell = 'A';

            $phpExcelObject->getActiveSheet()->setCellValue($nextCell.$row,$inscricao->getId());

            $phpExcelObject->getActiveSheet()->getCell($nextCell.$row)
                ->getHyperlink()->setUrl($this->generateUrl('questionario_inscricao_avaliacao_show',[
                    'inscricao' =>$inscricao->getId(),
                    'questionario' => $questionario->getId(),
                ],UrlGenerator::ABSOLUTE_URL));

            $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell.$row,$inscricao->getParticipante()->getNome());
            $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell.$row,$inscricao->getParticipante()->getEmail());

            if($questionario) {

                // Imprime se trabalho esta aprovado
                if ($questionario->getAproveRequired()) {
                    $respostaInscricao = $inscricao->getRespostaType(0)->first();
                    $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell . $row, $respostaInscricao->isApprovedLabel());
                }

                // Resposta do participante
                /** @var Resposta $resposta */
                foreach ($inscricao->getRespostaType(0) as $resposta){

                    /** @var RespostaItem $respostaItem */
                    foreach ($resposta->getRespostaItens() as $respostaItem) {
                        if($respostaItem->getText() != $phpExcelObject->getActiveSheet()->getCell($nextCell.$row)->getValue()){

                            if(strlen($respostaItem->getText()) > 100){
                                $comments[$nextCell.$row] = $respostaItem->getText();
                                $phpExcelObject->getActiveSheet()
                                    ->setCellValue(++$nextCell . $row, '...');

                            } else {
                                $phpExcelObject->getActiveSheet()
                                    ->setCellValue(++$nextCell . $row, $respostaItem->getText());
                            }
                            $phpExcelObject->getActiveSheet()->getColumnDimension($nextCell)->setAutoSize(true);
                        }
                    }

                }

                // Resposta dos avaliadores
                /** @var Resposta $resposta */
                foreach ($inscricao->getRespostaType($questionario->getType()) as $resposta) {

                    /** @var RespostaItem $respostaItem */
                    foreach ($resposta->getRespostaItens() as $respostaItem) {
                        $phpExcelObject->getActiveSheet()->setCellValue(++$nextCell . $row, $respostaItem->getText());
                        $phpExcelObject->getActiveSheet()->getColumnDimension($nextCell)->setAutoSize(true);
                    }
                }


            }

            $row++;
        }

        $phpExcelObject->getActiveSheet()->setComments($comments);

        // Style
        $maxColumn = $phpExcelObject->getActiveSheet()->getHighestColumn();
        $phpExcelObject->getActiveSheet()->mergeCells('A1:'.$maxColumn.'1');
        $phpExcelObject->getActiveSheet()->mergeCells('A2:'.$maxColumn.'2');
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $phpExcelObject->getActiveSheet()->getCell('A1')->getStyle()->getFont()->setSize('18')->setBold(true);
        $phpExcelObject->getActiveSheet()->getCell('A2')->getStyle()->getFont()->setSize('14')->setBold(true);

        $phpExcelObject->getActiveSheet()->getCell('A4')->getStyle()->getFont()->setSize('12')->setBold(true);
        $phpExcelObject->getActiveSheet()->getCell('B4')->getStyle()->getFont()->setSize('12')->setBold(true);
        $phpExcelObject->getActiveSheet()->getCell('C4')->getStyle()->getFont()->setSize('12')->setBold(true);

    }
}
