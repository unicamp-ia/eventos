<?php

namespace FCM\EventoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\Participante;
use FCM\EventoBundle\Form\ParticipanteType;
use FCM\EventoBundle\Repository\EventoRepository;
use FCM\UserBundle\Entity\User;
use FCM\UserBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Participante controller.
 *
 */
class ParticipanteController extends Controller
{


    /**
     * Lists all token entities.
     * @Route("/admin/participante", name="admin_participante_index")
     * @Security("has_role('ROLE_17_1')")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        return $this->render('FCMEventoBundle:Participante:index.html.twig', array(
            'page_title' => 'Participantes',

            'back' => $this->generateUrl('home')
        ));
    }


    /**
     * @Route("/ajax/participante", name="ajax_participante",options={"expose"=true})
     * @param Request $request
     */
    public function ajaxParticipante(Request $request){
        $em = $this->getDoctrine()->getManager();

        $participantes = $em->getRepository('FCMEventoBundle:Participante')->findAll();

        $result = array();

        /** @var Participante $participante */
        foreach ($participantes as $participante){
            $result['data'][] = [
                $this->render('default\link.html.twig',[
                    'text' => $participante->getDocumento(),
                    'link' => $this->generateUrl('participante_edit', [
                        'id' => $participante->getId()
                    ])
                ])->getContent(),

                $participante->getNome(),
                $participante->getEmail()
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * Creates a new participante entity.
     *
     * @Route("participante/new", name="participante_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $participante = new Participante();

        if($request->get('documento')){
            $participante->setDocumento($request->get('documento'));
        }

        $form = $this->createForm('FCM\EventoBundle\Form\ParticipanteType', $participante,['translator' => $this->get('translator')]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $translator = $this->get('translator');
            $em = $this->getDoctrine()->getManager();

            if(!Usuario::valCPF($participante->getDocumento()) and !$participante->getEstrangeiro()){
                $this->addFlash('error', 'Verifique o CPF e tente novamente');
                return $this->redirectToRoute('participante_new');
            }

            if (!preg_match_all('$\S*(?=\S{8,})(?=\S*[a-zA-Z])(?=\S*[\d])$', $form->get('pass')->getViewData()['first'])){
                $request->getSession()->getFlashBag()->add('error', $translator->trans('errosenha'));
            } else {
                $user = new User();
                $user->setUsername($participante->getDocumento());

                $user->setVinculo('Eventos');

                $em->persist($user);
                $participante->setUser($user);
                $em->persist($participante);
                $em->flush();
                $pass = $request->request->get('fcm_eventobundle_participante')['pass']['first'];

                if ($request->get('destination')) {
                    $this->addFlash('success', $translator->trans('msgsucessoparticipante'));
                    return $this->redirectToRoute('auth_participante', [
                            'documento' => $participante->getDocumento(),
                            'pass' => $pass,
                            '_target_path' => $request->get('destination')
                        ]
                    );
                } else
                    return $this->redirectToRoute('home');
            }
        }

        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('error', 'Seu documento ou nome já está cadastrado, recupere sua senha e tente novamente');
            return $this->redirectToRoute('resetpass_request');
        }


        $translator = $this->get('translator');

        return $this->render('FCMEventoBundle:Participante:edit.html.twig', array(
            'page_title' => $translator->trans('incluirnovopart'),
            'participante' => $participante,
            'form' => $form->createView(),
            'back' => $this->generateUrl('home'),
        ));
    }

    /**
     * Finds and displays a participante entity.
     *
     * @Route("participante/{id}", name="participante_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Participante $participante)
    {
        return $this->render('FCMEventoBundle:Participante:show.html.twig', array(
            'page_title' => 'Participantes - ' .$participante->getNome(),
            'entity' => $participante,
        ));
    }

    /**
     * Displays a form to edit an existing participante entity.
     *
     * @Route("participante/{id}/edit", name="participante_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Participante $participante)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(ParticipanteType::class, $participante,[
            'translator' => $this->get('translator')
        ]);


        $oldPass = $participante->getPass();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if(!Usuario::valCPF($participante->getDocumento()) and !$participante->getEstrangeiro()){
                $this->addFlash('error', 'Verifique o CPF e tente novamente');
                return $this->redirectToRoute('participante_edit',['id' => $participante->getId()]);
            }

            if(empty($request->get('fcm_eventobundle_participante')['pass']['first'])){
                $participante->setPassEncrypted($oldPass);
            }

            $participante->getUser()->setUsername($participante->getDocumento());


            $em->persist($participante->getUser());
            $em->persist($participante);
            $em->flush();

            $this->addFlash('success', 'Participante alterado com sucesso');

            if (true === $this->get('security.authorization_checker')->isGranted('ROLE_PARTICIPANTE')){
                return $this->redirectToRoute('home');
            } else {
                if(!$back = $request->get('back')) {
                    return $this->redirectToRoute('admin_participante_index');
                } else {
                    return $this->redirect($back);
                }

            }
        }

        if (true === $this->get('security.authorization_checker')->isGranted('ROLE_PARTICIPANTE')){
            $back = $this->generateUrl('home');
        } else {
            if(!$back = $request->get('back')) {
                $back = $this->generateUrl('admin_participante_index');
            }
        }

        return $this->render('FCMEventoBundle:Participante:edit.html.twig', array(
            'page_title' => 'Editar participante ' .$participante->getNome(),
            'form' => $form->createView(),
            'participante' => $participante,
            'delete' => $this->generateUrl('participante_confirmdelete', ['id' => $participante->getId()]),
            'back' => $back,
        ));
    }

    /**
     * Confirm the delete
     * @Route("admin/participante/{id}/delete/confirm", name="participante_confirmdelete")
     */
    public function deleteConfirmAction(Request $request, Participante $participante)
    {
        
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('participante_delete', array(
                    'id' => $participante->getId(),
                )
            ))
            ->setMethod('DELETE')
            ->getForm();

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('aluno_disciplina_index');
        }

        return $this->render('FCMEventoBundle:Default:delete-confirm.html.twig', array(
            'page_title' => 'Participantes - Excluir ' .$participante->getNome(),
            'title' => $participante->getNome(),
            'form' => $form->createView(),
            'cancel_route' => $this->generateUrl('home')
        ));
    }

    /**
     * Deletes an entity.
     *
     * @Route("admin/participante/{id}/delete", name="participante_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Participante $participante)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($participante);
        $em->flush();
        $this->addFlash('success','Exclusão efetuada com sucesso');

        return $this->redirectToRoute('home');
    }
}
