<?php

namespace FCM\EventoBundle\Controller;

use FCM\EventoBundle\Entity\Inscricao;
use FCM\QuestionBundle\Entity\PerguntaItem;
use FCM\QuestionBundle\Entity\Resposta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomPageController extends Controller
{
    /**
     * @Route("/custompage1")
     *
     * @Method({"GET", "POST"})
     */
    public function comauAction(Request $request)
    {
        return $this->render('FCMEventoBundle:CustomPage:comau.html.twig');
    }

}
