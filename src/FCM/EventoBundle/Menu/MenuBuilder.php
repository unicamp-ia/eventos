<?php

namespace FCM\EventoBundle\Menu;

use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\Evento;
use FCM\EventoBundle\Entity\EventoAtividade;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MenuBuilder
{
    private $factory;

    /** @var  EntityManager */
    private $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, EntityManager $entityManager,
                                AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->factory = $factory;
        $this->em = $entityManager;
        $this->authorizationChecker = $authorizationChecker;


    }

    /**
     * Menu Planes
     * @param RequestStack $requestStack
     * @return \Knp\Menu\ItemInterface
     */
    public function mainMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('Administração',[
            'route' => 'home',
        ]);

        $menu->addChild('Home', array(
            'attributes' => array('class' => 'expand material-icon'),
            'route' => 'home'
        ));

        $menu->addChild('Criar evento', array(
            'attributes' => array('class' => 'expand material-icon'),
            'route' => 'admin_evento_new'
        ));
        
        $menu->addChild('Eventos', array(
            'attributes' => array('class' => 'expand material-icon'),
            'route' => 'admin'
        ));

        if($this->authorizationChecker->isGranted('ROLE_17_1')) {
            $menu->addChild('Participantes', array(
                'attributes' => array('class' => 'expand material-icon'),
                'route' => 'admin_participante_index'
            ));
        }

        return $menu;
    }


    /**
     * Menu Planes
     * @param RequestStack $requestStack
     * @return \Knp\Menu\ItemInterface
     */
    public function eventoMenu(RequestStack $requestStack)
    {
        $evento = false;

        if($requestStack->getMasterRequest()->get('evento')){
            $idEvento = $requestStack->getMasterRequest()->get('evento');
        }

        if(isset($idEvento)){
            /** @var Evento $evento */
            $evento = $this->em->getRepository('FCMEventoBundle:Evento')->find($idEvento);
        }

        if(!$evento) return $this->factory->createItem('Eventos');

        $menu = $this->factory->createItem($evento->getNome(), [
            'route' => 'admin_evento_edit', 'routeParameters' => ['id' => $evento->getId()]
        ]);


        /** @var EventoAtividade $eventoAtividade */
        foreach ($evento->getEventoAtividades() as $eventoAtividade){
            $menu->addChild($eventoAtividade->getNome(), array(
                'attributes' => array('class' => 'expand material-icon'),
                'route' => 'admin_evento_atividade_edit',
                'routeParameters' => [
                    'id' => $eventoAtividade->getId(),
                    'evento' => $evento->getId(),
                ]
            ));
        }

        return $menu;
    }


}