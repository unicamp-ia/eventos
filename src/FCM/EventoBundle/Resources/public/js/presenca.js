$(document).on("click", ".admin_evento_atividade_presenca_ajax", function (e) {
    var $element = $(this);
    $($element).html('<img src="/eventos/bundles/fcmevento/images/loading.svg">');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: Routing.generate('admin_evento_atividade_presenca_ajax', {
            'inscricao' : $($element).attr('inscricao'),
            'eventoAtividade' : $($element).attr('eventoAtividade'),
            'evento' : $($element).attr('evento')
        }),
        async: false //you won't need that if nothing in your following code is dependend of the result
    })
        .done(function(response){
            var result = '#admin_evento_atividade_presenca_ajax'+$element.attr('inscricao');
            $(result).html(response); //Change the html of the div with the id = "your_div"
        })
        .fail(function(jqXHR, textStatus, errorThrown){
            alert('Error : ' + errorThrown);
        });
});

$(document).on("click", ".admin_evento_atividade_presenca_ajax_button", function (e) {
    var $element = $(this);

    $.ajax({
        type: "POST",
        dataType: 'json',
        url: Routing.generate('admin_evento_atividade_presenca_ajax_button', {
            'inscricao' : $($element).attr('inscricao'),
            'eventoAtividade' : $($element).attr('eventoAtividade'),
            'evento' : $($element).attr('evento')
        }),
        async: false //you won't need that if nothing in your following code is dependend of the result
    })
        .done(function(response){
            var result = '#admin_evento_atividade_presenca_ajax_button'+$element.attr('inscricao');
            $(result).html(response); //Change the html of the div with the id = "your_div"
        })
        .fail(function(jqXHR, textStatus, errorThrown){
            alert('Error : ' + errorThrown);
        });

});






