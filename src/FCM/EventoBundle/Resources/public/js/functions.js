$(document).ready(function () {

    $.fn.datetimepicker.dates['br'] = {

        days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sxta", "Sábado", "Domingo"],
        daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom"],
        daysMin: ["D", "S", "T", "Q", "Q", "S", "S", "D"],
        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
        meridiem:["am","pm"],
        suffix:["st","nd","rd","th"],
        today:"Hoje",
        clear:"Clear"
    };

    startDates();

    $('.ui.checkbox').checkbox();

});

function startDates() {
    $('.form_date_picker_datetime').datetimepicker({
        format : 'dd/mm/yyyy hh:ii',
        autoclose : true,
        todayHighlight : true,
        todayBtn : true,
        language: 'br',

    });
    $('.form_date_picker_date').datetimepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        todayHighlight : true,
        minView: 'month',
        todayBtn : true,
        language: 'en',
    });
    $('.form_date_picker_time').datetimepicker({
        format : 'hh:ii',
        autoclose : true,
        todayHighlight : true,
        minView: 'hour',
        todayBtn : true,
        language: 'en',
    });
    $('.form_date_picker_month').datetimepicker({
        format : 'mm/yyyy',
        autoclose : true,
        minView: 'year',
        language: 'en',
    });
}
