<?php

namespace FCM\EventoBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class EventoValidator extends ConstraintValidator
{
    protected $entityManager;
    /**
     * EventoValidator constructor.
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \FCM\EventoBundle\Entity\Evento $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        if($value->getAlias()) {
            $exist = $this->entityManager->getRepository('FCMEventoBundle:Evento')
                ->createQueryBuilder('a')
                ->select('a')
                ->where('a.alias = :alias')->setParameter('alias', $value->getAlias())
                ->andWhere('a.id != :id')->setParameter('id', $value->getId())
                ->getQuery()->getResult();

            /** @var \FCM\EventoBundle\Entity\Evento $value */
            foreach ($exist as $value) {
                $this->context->buildViolation('Endereço já utilizado em ' . $value->getNome())
                    ->atPath('alias')
                    ->addViolation();
            }
        }

    }
}