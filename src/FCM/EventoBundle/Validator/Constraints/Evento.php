<?php
namespace FCM\EventoBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Evento extends Constraint
{
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return EventoValidator::class;
    }

    public $message = 'Endereço já utilizado, crie um outro';


}