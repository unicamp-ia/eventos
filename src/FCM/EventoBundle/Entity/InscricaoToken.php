<?php

namespace FCM\EventoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InscricaoToken
 *
 * @ORM\Table(name="inscricao_token")
 * @ORM\Entity(repositoryClass="FCM\EventoBundle\Repository\InscricaoTokenRepository")
 */
class InscricaoToken
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Inscricao", inversedBy="inscricaoTokens")
     * @ORM\JoinColumn(name="inscricao_id", referencedColumnName="id")
     */
    private $inscricao;

    /**
     * @ORM\ManyToOne(targetEntity="AtividadeToken", inversedBy="inscricaoTokens")
     * @ORM\JoinColumn(name="atividade_token_id", referencedColumnName="id")
     */
    private $atividadeToken;

    /**
     * @var string
     *
     * @ORM\Column(name="val_text", type="text", nullable=true)
     */
    private $valText;

    /**
     * @var string
     *
     * @ORM\Column(name="val", type="text", nullable=true)
     */
    private $val;

    /**
     * @var int
     *
     * @ORM\Column(name="val_choice", type="integer", nullable=true)
     */
    private $valChoice;

    /**
     * @var bool
     *
     * @ORM\Column(name="val_bool", type="boolean", nullable=true)
     */
    private $valBool;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valText
     *
     * @param string $valText
     *
     * @return InscricaoToken
     */
    public function setValText($valText)
    {
        $this->valText = $valText;

        return $this;
    }

    /**
     * Get valText
     *
     * @return string
     */
    public function getValText()
    {
        return $this->valText;
    }

    /**
     * Set valChoice
     *
     * @param integer $valChoice
     *
     * @return InscricaoToken
     */
    public function setValChoice($valChoice)
    {
        $this->valChoice = $valChoice;

        return $this;
    }

    /**
     * Get valChoice
     *
     * @return int
     */
    public function getValChoice()
    {
        return $this->valChoice;
    }

    /**
     * Set valBool
     *
     * @param boolean $valBool
     *
     * @return InscricaoToken
     */
    public function setValBool($valBool)
    {
        $this->valBool = $valBool;

        return $this;
    }

    /**
     * Get valBool
     *
     * @return bool
     */
    public function getValBool()
    {
        return $this->valBool;
    }

    /**
     * Set inscricao
     *
     * @param \FCM\EventoBundle\Entity\Inscricao $inscricao
     *
     * @return InscricaoToken
     */
    public function setInscricao(\FCM\EventoBundle\Entity\Inscricao $inscricao = null)
    {
        $this->inscricao = $inscricao;

        return $this;
    }

    /**
     * Get inscricao
     *
     * @return \FCM\EventoBundle\Entity\Inscricao
     */
    public function getInscricao()
    {
        return $this->inscricao;
    }

    /**
     * Set val
     *
     * @param string $val
     *
     * @return InscricaoToken
     */
    public function setVal($val)
    {
        $this->val = $val;

        return $this;
    }

    /**
     * Get val
     *
     * @return string
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * Set atividadeToken
     *
     * @param \FCM\EventoBundle\Entity\AtividadeToken $atividadeToken
     *
     * @return InscricaoToken
     */
    public function setAtividadeToken(\FCM\EventoBundle\Entity\AtividadeToken $atividadeToken)
    {
        $this->atividadeToken = $atividadeToken;

        return $this;
    }

    /**
     * Get atividadeToken
     *
     * @return \FCM\EventoBundle\Entity\AtividadeToken
     */
    public function getAtividadeToken()
    {
        return $this->atividadeToken;
    }
}
