<?php

namespace FCM\EventoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Token
 *
 * @ORM\Table(name="token")
 * @ORM\Entity(repositoryClass="FCM\EventoBundle\Repository\TokenRepository")
 */
class Token
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function __construct()
    {
        $this->atividadeTokens = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="AtividadeToken", mappedBy="token")
     */
    private $atividadeTokens;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_exibicao", type="string", length=255)
     */
    private $nomeExibicao;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", nullable=true)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="options", type="text", nullable=true)
     */
    private $options;

    /**
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param string $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    private $tipo;

    /**
     * @var bool
     *
     * @ORM\Column(name="obrigatorio", type="boolean")
     */
    private $obrigatorio;

    /**
     * @var int
     *
     * @ORM\Column(name="tamanho", type="integer", nullable=true)
     */
    private $tamanho;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Token
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set nomeExibicao
     *
     * @param string $nomeExibicao
     *
     * @return Token
     */
    public function setNomeExibicao($nomeExibicao)
    {
        $this->nomeExibicao = $nomeExibicao;

        return $this;
    }

    /**
     * Get nomeExibicao
     *
     * @return string
     */
    public function getNomeExibicao()
    {
        return $this->nomeExibicao;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Token
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Token
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set obrigatorio
     *
     * @param boolean $obrigatorio
     *
     * @return Token
     */
    public function setObrigatorio($obrigatorio)
    {
        $this->obrigatorio = $obrigatorio;

        return $this;
    }

    /**
     * Get obrigatorio
     *
     * @return bool
     */
    public function getObrigatorio()
    {
        return $this->obrigatorio;
    }

    /**
     * Set tamanho
     *
     * @param integer $tamanho
     *
     * @return Token
     */
    public function setTamanho($tamanho)
    {
        $this->tamanho = $tamanho;

        return $this;
    }

    /**
     * Get tamanho
     *
     * @return int
     */
    public function getTamanho()
    {
        return $this->tamanho;
    }

    /**
     * Add atividadeToken
     *
     * @param \FCM\EventoBundle\Entity\AtividadeToken $atividadeToken
     *
     * @return Token
     */
    public function addAtividadeToken(\FCM\EventoBundle\Entity\AtividadeToken $atividadeToken)
    {
        $this->atividadeTokens[] = $atividadeToken;

        return $this;
    }

    /**
     * Remove atividadeToken
     *
     * @param \FCM\EventoBundle\Entity\AtividadeToken $atividadeToken
     */
    public function removeAtividadeToken(\FCM\EventoBundle\Entity\AtividadeToken $atividadeToken)
    {
        $this->atividadeTokens->removeElement($atividadeToken);
    }

    /**
     * Get atividadeTokens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAtividadeTokens()
    {
        return $this->atividadeTokens;
    }

    public static function getChoicesTipo(){
        return [
            'Symfony\Component\Form\Extension\Core\Type\TextType' => 'Texto',
            'Symfony\Component\Form\Extension\Core\Type\TextareaType' => 'Caixa de texto',
            'Symfony\Component\Form\Extension\Core\Type\ChoiceType' => 'Opção',
        ];
    }
}
