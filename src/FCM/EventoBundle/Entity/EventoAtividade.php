<?php

namespace FCM\EventoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FCM\QuestionBundle\Entity\Questionario;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * EventoAtividade
 *
 * @ORM\Table(name="evento_atividade")
 * @ORM\Entity(repositoryClass="FCM\EventoBundle\Repository\EventoAtividadeRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class EventoAtividade
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function __construct()
    {
        $this->inscricoes = new ArrayCollection();
        $this->atividade = new ArrayCollection();
        $this->questionarios = new ArrayCollection();
        //$this->atividadeBoleto = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="Inscricao", mappedBy="eventoAtividade", cascade={"remove"})
     */
    private $inscricoes;

    /**
     * @ORM\OneToOne(targetEntity="\FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto", mappedBy="eventoAtividade", cascade={"remove"})
     */
    private $atividadeBoleto;

    /**
     * @ORM\OneToMany(targetEntity="\FCM\QuestionBundle\Entity\Questionario", mappedBy="eventoAtividade", cascade={"remove"})
     */
    private $questionarios;


    /**
     * @ORM\ManyToOne(targetEntity="Atividade", inversedBy="eventoAtividades")
     * @ORM\JoinColumn(name="atividade_id", referencedColumnName="id")
     */
    private $atividade;

    /**
     * @ORM\ManyToOne(targetEntity="Evento", inversedBy="eventoAtividades")
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="id")
     * @Serializer\Expose()
     */
    private $evento;

    /**
     * @ORM\OneToOne(targetEntity="\FCM\LabelBundle\Entity\Etiqueta", mappedBy="eventoAtividade", cascade={"remove","persist"})
     */
    private $etiqueta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ini_inscricao", type="datetime")
     *
     */
    private $dataIniInscricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_fim_inscricao", type="datetime")
     */
    private $dataFimInscricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ini_atividade", type="datetime")
     */
    private $dataIniAtividade;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_fim_atividade", type="datetime")
     */
    private $dataFimAtividade;

    /**
     * @var int
     *
     * @ORM\Column(name="max_inscricao", type="integer", nullable=true)
     */
    private $maxInscricao;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo", type="text", nullable=true)
     */
    private $modelo;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     * @Serializer\Expose()
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descriacao", type="text", nullable=true)
     *
     */
    private $descricao;

    /**
     * Indica se inscricao sera oculta
     * @var int
     *
     * @ORM\Column(name="hide", type="boolean", nullable=true)
     */
    private $hide;

    /**
     * @return int
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * @param int $hide
     */
    public function setHide($hide)
    {
        $this->hide = $hide;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Atividade
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set dataIniInscricao
     *
     * @param \DateTime $dataIniInscricao
     *
     * @return EventoAtividade
     */
    public function setDataIniInscricao($dataIniInscricao)
    {
        $this->dataIniInscricao = $dataIniInscricao;

        return $this;
    }

    /**
     * Get dataIniInscricao
     *
     * @return \DateTime
     */
    public function getDataIniInscricao()
    {
        return $this->dataIniInscricao;
    }

    /**
     * Set dataFimInscricao
     *
     * @param \DateTime $dataFimInscricao
     *
     * @return EventoAtividade
     */
    public function setDataFimInscricao($dataFimInscricao)
    {
        $this->dataFimInscricao = $dataFimInscricao;

        return $this;
    }

    /**
     * Get dataFimInscricao
     *
     * @return \DateTime
     */
    public function getDataFimInscricao()
    {
        return $this->dataFimInscricao;
    }

    /**
     * Set dataIniAtividade
     *
     * @param \DateTime $dataIniAtividade
     *
     * @return EventoAtividade
     */
    public function setDataIniAtividade($dataIniAtividade)
    {
        $this->dataIniAtividade = $dataIniAtividade;

        return $this;
    }

    /**
     * Get dataIniAtividade
     *
     * @return \DateTime
     */
    public function getDataIniAtividade()
    {
        return $this->dataIniAtividade;
    }

    /**
     * Set dataFimAtividade
     *
     * @param \DateTime $dataFimAtividade
     *
     * @return EventoAtividade
     */
    public function setDataFimAtividade($dataFimAtividade)
    {
        $this->dataFimAtividade = $dataFimAtividade;

        return $this;
    }

    /**
     * Get dataFimAtividade
     *
     * @return \DateTime
     */
    public function getDataFimAtividade()
    {
        return $this->dataFimAtividade;
    }

    /**
     * Set maxInscricao
     *
     * @param integer $maxInscricao
     *
     * @return EventoAtividade
     */
    public function setmaxInscricao($maxInscricao)
    {
        $this->maxInscricao = $maxInscricao;

        return $this;
    }

    /**
     * Get maxInscricao
     *
     * @return integer
     */
    public function getmaxInscricao()
    {
        return $this->maxInscricao;
    }



    /**
     * Set modelo
     *
     * @param string $modelo
     *
     * @return EventoAtividade
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set atividadeToken
     *
     * @param \FCM\EventoBundle\Entity\Atividade $atividade
     *
     * @return EventoAtividade
     */
    public function setAtividade(\FCM\EventoBundle\Entity\Atividade $atividade = null)
    {
        $this->atividade = $atividade;

        return $this;
    }

    /**
     * Get atividadeToken
     *
     * @return Atividade
     */
    public function getAtividade()
    {
        return $this->atividade;
    }

    /**
     * Set evento
     *
     * @param \FCM\EventoBundle\Entity\Evento $evento
     *
     * @return EventoAtividade
     */
    public function setEvento(\FCM\EventoBundle\Entity\Evento $evento = null)
    {
        $this->evento = $evento;

        return $this;
    }

    /**
     * Get evento
     *
     * @return \FCM\EventoBundle\Entity\Evento
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * Validacoes
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        // Validacoes de horario
        if($this->getDataIniInscricao() >= $this->getDataFimInscricao())
            $context->buildViolation('Data inicial maior que final da inscrição')->atPath('dataIniInscricao')->addViolation();

        if($this->getDataIniAtividade() >= $this->getDataFimAtividade())
            $context->buildViolation('Data inicial maior que final da ativdade')->atPath('dataIniAtividade')->addViolation();

        if($this->getDataFimInscricao() >= $this->getDataIniAtividade())
            $context->buildViolation('Período de inscrição posterior a data da atividade')->atPath('dataIniAtividade')->addViolation();

    }

    /**
     * Add inscrico
     *
     * @param \FCM\EventoBundle\Entity\Inscricao $inscrico
     *
     * @return EventoAtividade
     */
    public function addInscrico(\FCM\EventoBundle\Entity\Inscricao $inscrico)
    {
        $this->inscricoes[] = $inscrico;

        return $this;
    }

    /**
     * Remove inscrico
     *
     * @param \FCM\EventoBundle\Entity\Inscricao $inscrico
     */
    public function removeInscrico(\FCM\EventoBundle\Entity\Inscricao $inscrico)
    {
        $this->inscricoes->removeElement($inscrico);
    }

    /**
     * Get inscricoes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscricoes()
    {
        return $this->inscricoes;
    }

    /**
     * Verifica se um participante esta inscrito
     * @param Participante $participante
     * @return bool|int
     */
    public function isInscrito(Participante $participante = null){
        if(!($participante instanceof Participante)) return false;

        $inscricoes = new ArrayCollection();

        /** @var Inscricao $inscricao */
        foreach($this->getInscricoes() as $inscricao){
            if($inscricao->getParticipante()->getId() == $participante->getId()) {
                $inscricoes->add($inscricao);
            }
        }

        if($inscricoes->count()) return $inscricoes;
        return false;
    }

    public function getInscricoesByParticipante(Participante $participante){
        $inscricoes = $this->getInscricoes()->filter(
            function(Inscricao $entry) use ($participante){
                if($entry->getParticipante() == $participante) return $entry;
            }
        );

        return $inscricoes;
    }

    /**
     * Verifica se eh possivel inscrever participante
     * @param Participante $participante
     * @return bool
     */
    public function isInscricaoPossible(Session $session = null){
        $today = new \DateTime();
        $today->setTime(0,0,0);

        //Limite de vagas excedido
        if(!$this->isVagasDisponiveis()) return false;

        // Inscricoes nao comecaram
        if($this->getDataIniInscricao() > $today) return false;

        // Inscricoes terminaram
        if($this->getDataFimInscricao() < $today) return false;

        // Se evento eh privado
        if($this->getHide()){
            // Verifica se na sessao tem a liberacao
            if(!($session instanceof Session)) return false;
            if($session->get('privateEventoAtividade') != $this->getId()) return false;
        }

        return true;
    }

    /**
     * Verifica se eh para mostrar a atividade para o participante
     * @param Participante $participante
     * @return bool
     */
    public function showActivity(Participante $participante = null){
        $today = new \DateTime();
        $today->setTime(0,0,0);

        // Inscricoes nao comecaram
        if($this->getDataIniInscricao() > $today) return false;

        // Inscricoes terminaram e o particpante nao esta cadastrado
        if($this->getDataFimInscricao() < $today && ! $this->isInscrito($participante)) return false;

        return true;
    }

    /**
     * Verifica se vagas estao esgotadas
     * @return bool
     */
    public function isVagasDisponiveis(){
        if(is_int($this->getmaxInscricao()))
            if($this->getInscricoes()->count() >= $this->getmaxInscricao()) return false;
        return true;
    }

    public function getPresentes(){
        $cont = 0;
        /** @var Inscricao $inscricao */
        foreach($this->getInscricoes() as $inscricao){
            if($inscricao->getPresenca()) $cont++;
        }

        return $cont;
    }

    /**
     * Add questionario
     *
     * @param \FCM\QuestionBundle\Entity\Questionario $questionario
     *
     * @return EventoAtividade
     */
    public function addQuestionario(\FCM\QuestionBundle\Entity\Questionario $questionario)
    {
        $this->questionarios[] = $questionario;

        return $this;
    }

    /**
     * Remove questionario
     *
     * @param \FCM\QuestionBundle\Entity\Questionario $questionario
     */
    public function removeQuestionario(\FCM\QuestionBundle\Entity\Questionario $questionario)
    {
        $this->questionarios->removeElement($questionario);
    }

    /**
     * Get questionarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionarios()
    {
        return $this->questionarios;
    }

    /**
     * Retorna a entidade do questinario
     */
    public function getQuestionario()
    {
        return $this->getQuestionarios()->filter(
            function(Questionario $entry){
                return $entry;
            }
        )->first();

        return false;
    }

    /**
     * Retorna questionario do tipo avaliador
     * @return Questionario
     */
    public function getQuestionarioAvaliador(){
        return $this->getQuestionarios()->filter(
            function(Questionario $entry){
                if($entry->getType() == 1) return $entry;
            }
        )->first();

        return false;
    }


    /**
     * Retorna true ou false dependnedo se o feedback for obrigatorio ou nao
     * 
     * @return boolean
     */
    public function isQuestionarioObrigatorio(){
        return $this->getQuestionarios()->filter(
            function(Questionario $entry){
                return $entry->getFeedback();
            }
        )->first();

        return false;
    }

    /**
     * Verifica se o usuario respondeu ao questionario
     * 
     * @return boolean
     */
    public function userAnsweredQuestionario(Participante $participante = null){
        if(!($participante instanceof Participante)) return false;

        /** @var Inscricao $inscricao */
        foreach($this->getInscricoes() as $inscricao){
            if($inscricao->getParticipante()->getId() == $participante->getId()) {
                //Verifica se o usuario respondeu pelo menos 1 questao
                return $inscricao->hasAsnweredOneQuestion();
            }
        }

        return false;
    }

    /**
     * Retorna questionario do tipo Inscricao
     * @return Questionario
     */
    public function getQuestionarioInscricao(){
        return $this->getQuestionarios()->filter(
            function(Questionario $entry){
                if($entry->getType() == 0) return $entry;
            }
        )->first();

        return false;
    }

    /**
     * Retorna questionario do tipo pós Inscricao
     * @return Questionario
     */
    public function getQuestionarioPosInscricao(){
        return $this->getQuestionarios()->filter(
            function(Questionario $entry){
                if($entry->getType() == 2) return $entry;
            }
        )->first();

        return false;
    }

    /**
     * Gera um token de tamanho 6
     * @return string
     */
    public function getTokenInscricao(){
        return substr(sha1($this->getId()),0,5);
    }

    /**
     * @param $token
     * @return bool
     */
    public function checkTokenInscricao($token){
        if(substr($token,0,5) == substr($this->getTokenInscricao(),0,5)) return true;
        return false;
    }

    

    /**
     * Set atividadeBoleto
     *
     * @param \FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto $atividadeBoleto
     *
     * @return EventoAtividade
     */
    public function setAtividadeBoleto(\FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto $atividadeBoleto = null)
    {
        $this->atividadeBoleto = $atividadeBoleto;

        return $this;
    }

    /**
     * Get atividadeBoleto
     *
     * @return \FCM\BoletoFuncampBundle\Entity\EventoAtividadeBoleto
     */
    public function getAtividadeBoleto()
    {
        return $this->atividadeBoleto;
    }

    /**
     * Verifica se inscricao eh multipla
     * @return bool
     */
    public function isInscricaoMultiple(){
        /** @var Questionario $questionario */
        if($questionario =  $this->getQuestionarioInscricao()){
            if(is_null($questionario->getMax()) or $questionario->getMax() > 1) return true;
        }

        return false;
    }

    /**
     * Set etiqueta
     *
     * @param \FCM\LabelBundle\Entity\Etiqueta $etiqueta
     *
     * @return EventoAtividade
     */
    public function setEtiqueta(\FCM\LabelBundle\Entity\Etiqueta $etiqueta = null)
    {
        $this->etiqueta = $etiqueta;

        return $this;
    }

    /**
     * Get etiqueta
     *
     * @return \FCM\LabelBundle\Entity\Etiqueta
     */
    public function getEtiqueta()
    {
        return $this->etiqueta;
    }
}
