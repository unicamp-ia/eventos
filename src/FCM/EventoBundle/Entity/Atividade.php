<?php

namespace FCM\EventoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Atividade
 *
 * @ORM\Table(name="atividade")
 * @ORM\Entity(repositoryClass="FCM\EventoBundle\Repository\AtividadeRepository")
 */
class Atividade
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function __construct()
    {
        $this->atividadeTokens = new ArrayCollection();
        $this->eventoAtividades = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="AtividadeToken", mappedBy="atividade")
     * @ORM\OrderBy({"delta" = "ASC"})
     */
    private $atividadeTokens;

    /**
     * @ORM\OneToMany(targetEntity="EventoAtividade", mappedBy="atividade")
     */
    private $eventoAtividades;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo", type="text")
     */
    private $modelo;

    /**
     * @var int
     *
     * @ORM\Column(name="avaliacao", type="boolean")
     */
    private $avaliacao;


    /**
     * Indica se podera haver multiplas inscricoes para essa atividade
     * @var int
     *
     * @ORM\Column(name="multiple", type="boolean")
     */
    private $multiple;

    /**
     * Caso ativa, será exibida no combo
     * @var int
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = 1;

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }


    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Atividade
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     *
     * @return Atividade
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Add atividadeToken
     *
     * @param \FCM\EventoBundle\Entity\AtividadeToken $atividadeToken
     *
     * @return Atividade
     */
    public function addAtividadeToken(\FCM\EventoBundle\Entity\AtividadeToken $atividadeToken)
    {
        $this->atividadeTokens[] = $atividadeToken;

        return $this;
    }

    /**
     * Remove atividadeToken
     *
     * @param \FCM\EventoBundle\Entity\AtividadeToken $atividadeToken
     */
    public function removeAtividadeToken(\FCM\EventoBundle\Entity\AtividadeToken $atividadeToken)
    {
        $this->atividadeTokens->removeElement($atividadeToken);
    }

    /**
     * Get atividadeTokens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAtividadeTokens()
    {
        return $this->atividadeTokens;
    }

    /**
     * Add eventoAtividade
     *
     * @param \FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade
     *
     * @return Atividade
     */
    public function addEventoAtividade(\FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade)
    {
        $this->eventoAtividades[] = $eventoAtividade;

        return $this;
    }

    /**
     * Remove eventoAtividade
     *
     * @param \FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade
     */
    public function removeEventoAtividade(\FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade)
    {
        $this->eventoAtividades->removeElement($eventoAtividade);
    }

    /**
     * Get eventoAtividades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventoAtividades()
    {
        return $this->eventoAtividades;
    }

    /**
     * Set avaliacao
     *
     * @param boolean $avaliacao
     *
     * @return EventoAtividade
     */
    public function setAvaliacao($avaliacao)
    {
        $this->avaliacao = $avaliacao;

        return $this;
    }

    /**
     * Get avaliacao
     *
     * @return boolean
     */
    public function getAvaliacao()
    {
        return $this->avaliacao;
    }

    /**
     * Set multiple
     *
     * @param boolean $multiple
     *
     * @return Atividade
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * Get multiple
     *
     * @return boolean
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    public function hasTokens(){
        return count($this->atividadeTokens);
    }
}
