<?php

namespace FCM\EventoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Session\Session;



/**
 * Participante
 *
 * @ORM\Table(name="participante")
 * @ORM\Entity(repositoryClass="FCM\EventoBundle\Repository\ParticipanteRepository")
 * @UniqueEntity(
 *     fields = "email",
 *     errorPath="email",
 *     message="E-mail já cadastrado, recupere sua senha"
 * )
 * @UniqueEntity(
 *     fields = "documento",
 *     errorPath="documento",
 *     message="Documento já cadastrado, recupere sua senha"
 * )
 * * @UniqueEntity(
 *     fields = "nome",
 *     errorPath="nome",
 *     message="Nome já cadastrado, recupere sua senha"
 * )
 * @Serializer\ExclusionPolicy("all")
 */
class Participante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    public function __construct()
    {
        $this->inscricoes = new ArrayCollection();
    }

    /**
     * @ORM\OneToOne(targetEntity="\FCM\UserBundle\Entity\User", inversedBy="participante")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * @ORM\OneToMany(targetEntity="Inscricao", mappedBy="participante")
     */
    private $inscricoes;

    /**
     * @var string
     *
     * @ORM\Column(name="documento", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    private $documento;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Serializer\Expose()
     */
    private $email;

    /**
     * @ORM\Column(name="estrangeiro", type="boolean", nullable=true)
     */
    private $estrangeiro;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     * @Serializer\Expose()
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=255)
     */
    private $pass;

    /**
     * @var string
     *
     * @ORM\Column(name="reset_pass", type="string", length=255, nullable=true)
     */
    private $resetPass;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documento
     *
     * @param string $documento
     *
     * @return Participante
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return string
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Participante
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Participante
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set pass
     *
     * @param string $pass
     *
     * @return Participante
     */
    public function setPass($pass)
    {

        $this->pass = md5($pass);

        return $this;
    }

    public function setPassEncrypted($pass){
        $this->pass = $pass;
        return $this;
    }

    /**
     * Get pass
     *
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set resetPass
     *
     * @param string $resetPass
     *
     * @return Participante
     */
    public function setResetPass($resetPass)
    {
        $this->resetPass = $resetPass;

        return $this;
    }

    /**
     * Get resetPass
     *
     * @return string
     */
    public function getResetPass()
    {
        return $this->resetPass;
    }

    /**
     * Add inscrico
     *
     * @param \FCM\EventoBundle\Entity\Inscricao $inscrico
     *
     * @return Participante
     */
    public function addInscrico(\FCM\EventoBundle\Entity\Inscricao $inscrico)
    {
        $this->inscricoes[] = $inscrico;

        return $this;
    }

    /**
     * Remove inscrico
     *
     * @param \FCM\EventoBundle\Entity\Inscricao $inscrico
     */
    public function removeInscrico(\FCM\EventoBundle\Entity\Inscricao $inscrico)
    {
        $this->inscricoes->removeElement($inscrico);
    }

    /**
     * Get inscricoes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscricoes()
    {
        return $this->inscricoes;
    }

    /**
     * Set user
     *
     * @param \FCM\UserBundle\Entity\User $user
     *
     * @return Participante
     */
    public function setUser(\FCM\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \FCM\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Check pass
     * @param $pass
     * @return bool
     */
    public function checkPass($pass){
        // @todo: PLEASEEE DONT USE MD5!
        if($this->getPass() == md5($pass)) return true;
        return false;
    }

    /**
     * Generates an random pass
     */
    public function setRandomPass(){
        $pass = substr($this->getNome(),0,3) . substr(time(),0,5);
        $this->setPass($pass);
        return $pass;
    }

    public function isInscricaoPossible(EventoAtividade $eventoAtividade, Session $session = null){

        // Verifica condicoes da atividade
        if(!$eventoAtividade->isInscricaoPossible($session)) return false;

        // Nao multiplo e ja inscrito
        if($eventoAtividade->isInscrito($this) and !$eventoAtividade->isInscricaoMultiple())
            return false;

        return true;
    }

    /**
     * Valida CPF
     * @param null $cpf
     * @return bool
     */
    public static function validateCPF($cpf)
    {
        // Verifica se um número foi informado
        if (empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999'
        ) {
            return false;
            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }


    /**
     * Set estrangeiro
     *
     * @param boolean $estrangeiro
     *
     * @return Participante
     */
    public function setEstrangeiro($estrangeiro)
    {
        $this->estrangeiro = $estrangeiro;

        return $this;
    }

    /**
     * Get estrangeiro
     *
     * @return boolean
     */
    public function getEstrangeiro()
    {
        return $this->estrangeiro;
    }
}
