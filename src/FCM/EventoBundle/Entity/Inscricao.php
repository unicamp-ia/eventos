<?php

namespace FCM\EventoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\UserBundle\Entity\User;
use JMS\Serializer\Annotation as Serializer;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Inscricao
 *
 * @ORM\Table(name="inscricao")
 * @ORM\Entity(repositoryClass="FCM\EventoBundle\Repository\InscricaoRepository")
 * @Vich\Uploadable
 * @Serializer\ExclusionPolicy("all")
 */
class Inscricao
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @param int $id

     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function __construct()
    {
        $this->inscricaoTokens = new ArrayCollection();
        $this->avaliacoes = new ArrayCollection();
        $this->respostas = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="InscricaoToken", mappedBy="inscricao", cascade={"remove"})
     */
    private $inscricaoTokens;

    /**
     * @ORM\OneToMany(targetEntity="Avaliacao", mappedBy="inscricao")
     */
    private $avaliacoes;

    /**
     * @ORM\OneToMany(targetEntity="\FCM\QuestionBundle\Entity\Resposta", mappedBy="inscricao", cascade={"remove","persist"})
     * @Serializer\Expose()
     */
    private $respostas;

    /**
     * @ORM\ManyToOne(targetEntity="EventoAtividade", inversedBy="inscricoes")
     * @ORM\JoinColumn(name="evento_atividade_id", referencedColumnName="id")
     * @Serializer\Expose()
     */
    private $eventoAtividade;

    /**
     * @ORM\ManyToOne(targetEntity="Participante", inversedBy="inscricoes")
     * @ORM\JoinColumn(name="participante_id", referencedColumnName="id")
     * @Serializer\Expose()
     */
    private $participante;

    /**
     * @ORM\OneToOne(targetEntity="\FCM\BoletoFuncampBundle\Entity\InscricaoBoleto", mappedBy="inscricao", cascade={"remove"})
     * @Serializer\Expose()
     */
    private $boleto;

    /**
     * @var bool
     *
     * @ORM\Column(name="pago", type="boolean", nullable=true)
     * @Serializer\Expose()
     */
    private $pago;

    /**
     * @var bool
     *
     * @ORM\Column(name="aprovado", type="boolean", nullable=true)
     * @Serializer\Expose()
     */
    private $aprovado = false;

    /**
     * @return bool
     */
    public function isAprovado()
    {
        return $this->aprovado;
    }

    /**
     * @param bool $aprovado
     */
    public function setAprovado($aprovado)
    {
        $this->aprovado = $aprovado;
    }

    /**
     * @var bool
     *
     * @ORM\Column(name="emitiu", type="boolean")
     */
    private $emitiu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Serializer\Expose()
     */
    private $created;

    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @ORM\Column(name="presenca", type="boolean")
     */
    private $presenca;

    /**
     * @var bool
     *
     * @ORM\Column(name="liberado", type="boolean")
     * @Serializer\Expose()
     */
    private $liberado;

    /**
     * @return boolean
     */
    public function isLiberado()
    {
        return $this->liberado;
    }

    /**
     * @param boolean $liberado
     */
    public function setLiberado($liberado)
    {
        $this->liberado = $liberado;
    }

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     *
     * @var string
     * @Serializer\Expose()
     */
    private $token;

    /**
     * Definido para ocultar na impressao da etiqueta
     * @var
     */
    private $hidePrint;

    /**
     * @return mixed
     */
    public function getHidePrint()
    {
        return $this->hidePrint;
    }

    /**
     * @param mixed $hidePrint
     */
    public function setHidePrint($hidePrint)
    {
        $this->hidePrint = $hidePrint;
    }




    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token = null)
    {

        if(is_null($token)){
            $token = hash('sha256', $this->getEventoAtividade()->getId()
            .$this->getParticipante()->getDocumento().$this->getEventoAtividade()->getId().random_bytes(10));
        }
        $this->token = $token;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pago
     *
     * @param boolean $pago
     *
     * @return Inscricao
     */
    public function setPago($pago)
    {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return bool
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * Set emitiu
     *
     * @param boolean $emitiu
     *
     * @return Inscricao
     */
    public function setEmitiu($emitiu)
    {
        $this->emitiu = $emitiu;

        return $this;
    }

    /**
     * Get emitiu
     *
     * @return bool
     */
    public function getEmitiu()
    {
        return $this->emitiu;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Inscricao
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set eventoAtividade
     *
     * @param \FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade
     *
     * @return Inscricao
     */
    public function setEventoAtividade(\FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade = null)
    {
        $this->eventoAtividade = $eventoAtividade;

        return $this;
    }

    /**
     * Get eventoAtividade
     *
     * @return \FCM\EventoBundle\Entity\EventoAtividade
     */
    public function getEventoAtividade()
    {
        return $this->eventoAtividade;
    }

    /**
     * Set participante
     *
     * @param \FCM\EventoBundle\Entity\Participante $participante
     *
     * @return Inscricao
     */
    public function setParticipante(\FCM\EventoBundle\Entity\Participante $participante = null)
    {
        $this->participante = $participante;

        return $this;
    }

    /**
     * Get participante
     *
     * @return \FCM\EventoBundle\Entity\Participante
     */
    public function getParticipante()
    {
        return $this->participante;
    }

    /**
     * Add inscricaoToken
     *
     * @param \FCM\EventoBundle\Entity\InscricaoToken $inscricaoToken
     *
     * @return Inscricao
     */
    public function addInscricaoToken(\FCM\EventoBundle\Entity\InscricaoToken $inscricaoToken)
    {
        $this->inscricaoTokens[] = $inscricaoToken;

        return $this;
    }

    /**
     * Remove inscricaoToken
     *
     * @param \FCM\EventoBundle\Entity\InscricaoToken $inscricaoToken
     */
    public function removeInscricaoToken(\FCM\EventoBundle\Entity\InscricaoToken $inscricaoToken)
    {
        $this->inscricaoTokens->removeElement($inscricaoToken);
    }

    /**
     * Get inscricaoTokens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscricaoTokens()
    {
        return $this->inscricaoTokens;
    }



    /**
     * Add avaliaco
     *
     * @param \FCM\EventoBundle\Entity\Avaliacao $avaliaco
     *
     * @return Inscricao
     */
    public function addAvaliaco(\FCM\EventoBundle\Entity\Avaliacao $avaliaco)
    {
        $this->avaliacoes[] = $avaliaco;

        return $this;
    }

    /**
     * Remove avaliaco
     *
     * @param \FCM\EventoBundle\Entity\Avaliacao $avaliaco
     */
    public function removeAvaliaco(\FCM\EventoBundle\Entity\Avaliacao $avaliaco)
    {
        $this->avaliacoes->removeElement($avaliaco);
    }

    /**
     * Get avaliacoes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAvaliacoes()
    {
        return $this->avaliacoes;
    }

    public function getTrabalho()
    {
        /** @var InscricaoToken $inscricaoToken */
        foreach ($this->getInscricaoTokens() as $inscricaoToken) {
            if (is_numeric(strpos($inscricaoToken->getAtividadeToken()->getToken()->getNome(), 'Trabalho')))
                return $inscricaoToken->getVal();
        }
    }

    public function getAutor()
    {
        /** @var InscricaoToken $inscricaoToken */
        foreach ($this->getInscricaoTokens() as $inscricaoToken) {
            if (is_numeric(strpos($inscricaoToken->getAtividadeToken()->getToken()->getNome(), 'Autor')))
                return $inscricaoToken->getVal();
        }
    }

    public function getTitulo()
    {
        /** @var InscricaoToken $inscricaoToken */
        foreach ($this->getInscricaoTokens() as $inscricaoToken) {
            if (is_numeric(strpos($inscricaoToken->getAtividadeToken()->getToken()->getNome(), 'Título')))
                return $inscricaoToken->getVal();
        }
    }

    /**
     * Verifica se trabalho foi avaliado por User
     * @param User $user
     * @return bool
     */
    public function isAvaliadoPor(User $user){

        /** @var Resposta $resposta */
        foreach($this->getRespostaType(1) as $resposta){
            if($resposta->getCreatedBy() == $user) return true;
        }

        return false;
    }

    /**
     * Set presenca
     *
     * @param boolean $presenca
     *
     * @return Inscricao
     */
    public function setPresenca($presenca)
    {
        $this->presenca = $presenca;

        return $this;
    }

    /**
     * Get presenca
     *
     * @return boolean
     */
    public function getPresenca()
    {
        return $this->presenca;
    }

    /**
     * Verifica se possui condicoes para liberar certificado
     * @return bool
     */
    public function isCertificadoLiberado(){
        if($this->getPresenca() and $this->isLiberado() and $this->getToken()) return true;
        return false;
    }



    /**
     * Get liberado
     *
     * @return boolean
     */
    public function getLiberado()
    {
        return $this->liberado;
    }

    /**
     * Add resposta
     *
     * @param \FCM\QuestionBundle\Entity\Resposta $resposta
     *
     * @return Inscricao
     */
    public function addResposta(\FCM\QuestionBundle\Entity\Resposta $resposta)
    {
        $this->respostas[] = $resposta;

        return $this;
    }

    /**
     * Remove resposta
     *
     * @param \FCM\QuestionBundle\Entity\Resposta $resposta
     */
    public function removeResposta(\FCM\QuestionBundle\Entity\Resposta $resposta)
    {
        $this->respostas->removeElement($resposta);
    }

    /**
     * Get respostas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRespostas()
    {
        return $this->respostas;
    }

    /**
     * Verifica se pelo menos 1 resposta foi respondida
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function hasAsnweredOneQuestion()
    {
        return count($this->respostas) > 0 ? true : false;
    }

    /**
     * @param $type
     * @return bool|ArrayCollection
     */
    public function getRespostaType($type){
        return $this->getRespostas()->filter(
            function(Resposta $resposta) use ($type){
                if($resposta->getQuestionario()->getType() == $type) return $resposta;
            }
        );

        return false;
    }

    /**
     * @return bool|Resposta
     */
    public function getDeposito(){
        if($resposta = $this->getRespostaType(0)){
            return $resposta->first();
        }
        return false;
    }

    /**
     * Set boleto
     *
     * @param \FCM\BoletoFuncampBundle\Entity\InscricaoBoleto $boleto
     *
     * @return Inscricao
     */
    public function setBoleto(\FCM\BoletoFuncampBundle\Entity\InscricaoBoleto $boleto = null)
    {
        $this->boleto = $boleto;

        return $this;
    }

    /**
     * Get boleto
     *
     * @return \FCM\BoletoFuncampBundle\Entity\InscricaoBoleto
     */
    public function getBoleto()
    {
        return $this->boleto;
    }



    /**
     * Get aprovado
     *
     * @return boolean
     */
    public function getAprovado()
    {
        return $this->aprovado;
    }
}
