<?php

namespace FCM\EventoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AtividadeToken
 *
 * @ORM\Table(name="atividade_token")
 * @ORM\Entity(repositoryClass="FCM\EventoBundle\Repository\AtividadeTokenRepository")
 */
class AtividadeToken
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function __construct()
    {
        $this->inscricaoTokens = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="InscricaoToken", mappedBy="atividadeToken")
     */
    private $inscricaoTokens;

    /**
     * @ORM\ManyToOne(targetEntity="Atividade", inversedBy="atividadeTokens")
     * @ORM\JoinColumn(name="atividade_id", referencedColumnName="id")
     */
    private $atividade;

    /**
     * @ORM\ManyToOne(targetEntity="Token", inversedBy="atividadeTokens")
     * @ORM\JoinColumn(name="token_id", referencedColumnName="id")
     *
     */
    private $token;

    /**
     * @var int
     *
     * @ORM\Column(name="delta", type="integer", nullable=false)
     */
    private $delta = 0;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set atividade
     *
     * @param \FCM\EventoBundle\Entity\Atividade $atividade
     *
     * @return AtividadeToken
     */
    public function setAtividade(\FCM\EventoBundle\Entity\Atividade $atividade = null)
    {
        $this->atividade = $atividade;

        return $this;
    }

    /**
     * Get atividade
     *
     * @return \FCM\EventoBundle\Entity\Atividade
     */
    public function getAtividade()
    {
        return $this->atividade;
    }

    /**
     * Set token
     *
     * @param \FCM\EventoBundle\Entity\Token $token
     *
     * @return AtividadeToken
     */
    public function setToken(\FCM\EventoBundle\Entity\Token $token = null)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * Get token
     *
     * @return \FCM\EventoBundle\Entity\Token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Add inscricaoToken
     *
     * @param \FCM\EventoBundle\Entity\InscricaoToken $inscricaoToken
     *
     * @return AtividadeToken
     */
    public function addInscricaoToken(\FCM\EventoBundle\Entity\InscricaoToken $inscricaoToken)
    {
        $this->inscricaoTokens[] = $inscricaoToken;

        return $this;
    }

    /**
     * Remove inscricaoToken
     *
     * @param \FCM\EventoBundle\Entity\InscricaoToken $inscricaoToken
     */
    public function removeInscricaoToken(\FCM\EventoBundle\Entity\InscricaoToken $inscricaoToken)
    {
        $this->inscricaoTokens->removeElement($inscricaoToken);
    }

    /**
     * Get inscricaoTokens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscricaoTokens()
    {
        return $this->inscricaoTokens;
    }

    /**
     * Set delta
     *
     * @param integer $delta
     *
     * @return AtividadeToken
     */
    public function setDelta($delta)
    {
        $this->delta = $delta;

        return $this;
    }

    /**
     * Get delta
     *
     * @return integer
     */
    public function getDelta()
    {
        return $this->delta;
    }
}
