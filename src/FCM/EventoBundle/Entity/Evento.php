<?php

namespace FCM\EventoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FCM\UserBundle\Entity\User;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use FCM\EventoBundle\Validator\Constraints as EventoAssert;

/**
 * Evento
 *
 * @ORM\Table(name="evento")
 * @ORM\Entity(repositoryClass="FCM\EventoBundle\Repository\EventoRepository")
 * @Vich\Uploadable
 * @EventoAssert\Evento()
 */
class Evento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"evento"})
     */
    private $id;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function __construct()
    {
        $this->eventoAtividades = new ArrayCollection();
        $this->eventoAdmins = new ArrayCollection();

    }

    /**
     * @ORM\OneToMany(targetEntity="EventoAtividade", mappedBy="evento", cascade={"remove"})
     * @ORM\OrderBy({"dataIniAtividade" = "ASC"})
     */
    private $eventoAtividades;

    /**
     * @ORM\OneToMany(targetEntity="EventoAdmin", mappedBy="evento", cascade={"remove"})
     */
    private $eventoAdmins;


    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     * @Serializer\Groups({"evento"})
     */
    private $nome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="local", type="string", length=255)
     */
    private $local;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="templateName")
     *
     * @var File
     */
    private $template;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     *
     * @var string
     */
    private $templateName;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $cssCorpo;

    /**
     * @return string
     */
    public function getCssCorpo()
    {
        return $this->cssCorpo;
    }

    /**
     * @param string $cssCorpo
     */
    public function setCssCorpo($cssCorpo)
    {
        $this->cssCorpo = $cssCorpo;
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="thumbnailName")
     *
     * @var File
     * @Serializer\Expose()
     */
    private $thumbnail;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     *
     * @var string
     */
    private $thumbnailName;

    /**
     * @var string
     *
     * @ORM\Column(name="descriacao", type="text")
     * @Assert\NotBlank()
     */
    private $descricao;

    /**
     * @ORM\ManyToOne(targetEntity="\FCM\UserBundle\Entity\User", inversedBy="eventos")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Serializer\Expose()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", nullable=true)
     */
    private $alias;

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $alias = trim($alias);
        $alias = str_replace(' ' ,'',$alias);
        $alias = strtolower($alias);
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return Evento
     */
    public function setTemplate(File $template = null)
    {
        $this->template = $template;

        if ($template) {
            $this->setCreated(new \DateTime());
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param string $templateName
     *
     * @return Evento
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="assinaturaName")
     *
     * @var File
     */
    private $assinatura;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     *
     * @var string
     */
    private $assinaturaName;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return Evento
     */
    public function setAssinatura(File $assinatura = null)
    {
        $this->assinatura = $assinatura;

        if ($assinatura) {
            $this->setCreated(new \DateTime());
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getAssinatura()
    {
        return $this->assinatura;
    }

    /**
     * @param string $assinaturaName
     *
     * @return Evento
     */
    public function setAssinaturaName($assinaturaName)
    {
        $this->assinaturaName = $assinaturaName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAssinaturaName()
    {
        return $this->assinaturaName;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return Evento
     */
    public function setThumbnail(File $thumbnail = null)
    {
        $this->thumbnail = $thumbnail;

        if ($thumbnail) {
            $this->setCreated(new \DateTime('now'));
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param string $thumbnailName
     *
     * @return Evento
     */
    public function setThumbnailName($thumbnailName)
    {
        $this->thumbnailName = $thumbnailName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getThumbnailName()
    {
        return $this->thumbnailName;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Evento
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Evento
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set local
     *
     * @param string $local
     *
     * @return Evento
     */
    public function setLocal($local)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return string
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Add eventoAtividade
     *
     * @param \FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade
     *
     * @return Evento
     */
    public function addEventoAtividade(\FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade)
    {
        $this->eventoAtividades[] = $eventoAtividade;

        return $this;
    }

    /**
     * Remove eventoAtividade
     *
     * @param \FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade
     */
    public function removeEventoAtividade(\FCM\EventoBundle\Entity\EventoAtividade $eventoAtividade)
    {
        $this->eventoAtividades->removeElement($eventoAtividade);
    }

    /**
     * Get eventoAtividades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventoAtividades()
    {
        return $this->eventoAtividades;
    }



    /**
     * Add eventoAdmin
     *
     * @param \FCM\EventoBundle\Entity\EventoAdmin $eventoAdmin
     *
     * @return Evento
     */
    public function addEventoAdmin(\FCM\EventoBundle\Entity\EventoAdmin $eventoAdmin)
    {
        $this->eventoAdmins[] = $eventoAdmin;

        return $this;
    }

    /**
     * Remove eventoAdmin
     *
     * @param \FCM\EventoBundle\Entity\EventoAdmin $eventoAdmin
     */
    public function removeEventoAdmin(\FCM\EventoBundle\Entity\EventoAdmin $eventoAdmin)
    {
        $this->eventoAdmins->removeElement($eventoAdmin);
    }

    /**
     * Get eventoAdmins
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventoAdmins()
    {
        return $this->eventoAdmins;
    }

    /**
     * Verifica se a pessoa tem pelo menos uma inscrição em alguma atividade no evento
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function algumaInscricao($user)
    {
        $found = false;

        foreach ($this->getEventoAtividades()->getValues() as $key => $atividade) {
            if ( $atividade->isInscrito($user)){
                $found = true;
            }
        }
        
        return $found ? true : false;
    }

    public function isAdmin(User $user = null){


        if(!$user) return false;

        $roles = $user->getRoles();

        if(isset($roles['ROLE_17_0'])) return true;

        if($this->getCreatedBy()->getUsername() == $user->getUsername()) return true;

        /** @var EventoAdmin $eventoAdmin */
        foreach ($this->getEventoAdmins() as $eventoAdmin){
            if($eventoAdmin->getUser() instanceof User){
                if($eventoAdmin->getUser()->getUsername() == $user->getUsername()) return true;
            }

        }



        return false;
    }
}
