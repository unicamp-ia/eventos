<?php

namespace FCM\EventoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventoAdmin
 *
 * @ORM\Table(name="evento_admin")
 * @ORM\Entity(repositoryClass="FCM\EventoBundle\Repository\EventoAdminRepository")
 */
class EventoAdmin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Evento", inversedBy="eventoAdmins")
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="id")
     */
    private $evento;

    /**
     * @ORM\ManyToOne(targetEntity="\FCM\UserBundle\Entity\User", inversedBy="eventoAdminCreatedBys")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="\FCM\UserBundle\Entity\User", inversedBy="eventoAdminUsers")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set evento
     *
     * @param \FCM\EventoBundle\Entity\Evento $evento
     *
     * @return EventoAdmin
     */
    public function setEvento(\FCM\EventoBundle\Entity\Evento $evento = null)
    {
        $this->evento = $evento;

        return $this;
    }

    /**
     * Get evento
     *
     * @return \FCM\EventoBundle\Entity\Evento
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * Set createdBy
     *
     * @param \FCM\UserBundle\Entity\User $createdBy
     *
     * @return EventoAdmin
     */
    public function setCreatedBy(\FCM\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \FCM\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set user
     *
     * @param \FCM\UserBundle\Entity\User $user
     *
     * @return EventoAdmin
     */
    public function setUser(\FCM\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \FCM\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
