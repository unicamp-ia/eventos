<?php

namespace FCM\EventoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FCM\UserBundle\Entity\User;

/**
 * Avaliacao
 *
 * @ORM\Table(name="avaliacao")
 * @ORM\Entity(repositoryClass="FCM\EventoBundle\Repository\AvaliacaoRepository")
 */
class Avaliacao
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Inscricao", inversedBy="avaliacoes")
     * @ORM\JoinColumn(name="inscricao_id", referencedColumnName="id")
     */
    private $inscricao;

    /**
     * @ORM\ManyToOne(targetEntity="\FCM\UserBundle\Entity\User", inversedBy="avaliacoes")
     * @ORM\JoinColumn(name="avaliador_id", referencedColumnName="id")
     */
    private $avaliador;

    /**
     * @return mixed
     */
    public function getAvaliador()
    {
        return $this->avaliador;
    }

    /**
     * @param User $avaliador
     */
    public function setAvaliador(User $avaliador)
    {
        $this->avaliador = $avaliador;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="nota", type="smallint")
     */
    private $nota;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nota
     *
     * @param integer $nota
     *
     * @return Avaliacao
     */
    public function setNota($nota)
    {
        $this->nota = $nota;

        return $this;
    }

    /**
     * Get nota
     *
     * @return int
     */
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Avaliacao
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set inscricao
     *
     * @param \FCM\EventoBundle\Entity\Inscricao $inscricao
     *
     * @return Avaliacao
     */
    public function setInscricao(\FCM\EventoBundle\Entity\Inscricao $inscricao = null)
    {
        $this->inscricao = $inscricao;

        return $this;
    }

    /**
     * Get inscricao
     *
     * @return \FCM\EventoBundle\Entity\Inscricao
     */
    public function getInscricao()
    {
        return $this->inscricao;
    }

    /**
     * Set participante
     *
     * @param \FCM\EventoBundle\Entity\Participante $participante
     *
     * @return Avaliacao
     */
    public function setParticipante(\FCM\EventoBundle\Entity\Participante $participante = null)
    {
        $this->participante = $participante;

        return $this;
    }

    /**
     * Get participante
     *
     * @return \FCM\EventoBundle\Entity\Participante
     */
    public function getParticipante()
    {
        return $this->participante;
    }
}
