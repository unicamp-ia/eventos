<?php

namespace FCM\EventoBundle\Command;

use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RequestContext;

class FcmEventoNotifyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fcm:evento:notify')
            ->setDescription('Notifica participantes 2 dias antes do evento')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        $rows = $em->getRepository('FCMEventoBundle:EventoAtividade')->findNearEventoAtividade();

        /** @var EventoAtividade $row */
        foreach($rows as $row){
            $this->sendMessageAction($row);
        }


        $output->writeln('Finalizado');
    }

    /**
     * Envia mensagem para um grupo de usuarios
     * @param EventoAtividade $eventoAtividade
     * @param array $mails
     * @param $message
     */
    public function sendMessageAction(EventoAtividade $eventoAtividade){
        /** @var RequestContext $context */
        $context = $this->getContainer()->get('router')->getContext();
        $link = 'http://'.$context->getHost().'/eventos/evento/'.$eventoAtividade->getEvento()->getId();

        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('[IA/Evento] Lembrete do seu evento')

            ->setTo($eventoAtividade->getEvento()->getEmail())
            ->setReplyTo($eventoAtividade->getEvento()->getEmail())
            ->setBody(
                $this->getContainer()->get('templating')->render('notify.new2.html.twig',[
                    'eventoAtividade' => $eventoAtividade,
                    'link' => $link,
                ]),
                'text/html'
            );

        /** @var Inscricao $row */
        foreach($eventoAtividade->getInscricoes() as $row){
            $message->addBcc($row->getParticipante()->getEmail());
        }

        $this->getContainer()->get('mailer')->send($message);
    }

}
