<?php

namespace FCM\EventoBundle\Command;

use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\InscricaoToken;
use FCM\UserBundle\Entity\AD;
use FCM\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FcmEventoFixsemanapesqCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fcm:evento:fixsemanapesq')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        /** @var EventoAtividade $eventoAtividade */
        $eventoAtividade = $em->getRepository('FCMEventoBundle:EventoAtividade')->find(1139);

        $atividadeToken = $em->getRepository('FCMEventoBundle:AtividadeToken')->findOneBy([
            'atividade' => 3, 'token' => 6]);

        /** @var Inscricao $inscricao */
        foreach($eventoAtividade->getInscricoes() as $inscricao){
            $doc = $inscricao->getParticipante()->getDocumento();

            $result = AD::search('(&(employeeID=*'.$doc.'))');
            if($result){
                $inscricao_token = new InscricaoToken();
                $inscricao_token->setAtividadeToken($atividadeToken);
                $inscricao_token->setInscricao($inscricao);
                $inscricao_token->setVal($result[0]['description'][0]);
                $em->persist($inscricao_token);
            } else {
                $output->writeln('Aluno nao encontrado ' . $doc . ' nome: ' .$inscricao->getParticipante()->getNome());
            }
        }

        $em->flush();
        $output->writeln('Finalizado');
    }

}
