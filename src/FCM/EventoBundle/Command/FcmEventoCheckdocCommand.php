<?php

namespace FCM\EventoBundle\Command;

use Doctrine\ORM\EntityManager;
use FCM\EventoBundle\Entity\Participante;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FcmEventoCheckdocCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fcm:evento:checkdoc')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $participantes = $em->getRepository('FCMEventoBundle:Participante')->findAll();
        $cont = 0;
        foreach ($participantes as $participante){
            if(!is_null($participante->getDocumento()) and !$participante->getEstrangeiro() and
                !Participante::validateCPF($participante->getDocumento()) ){
                $cont++;

                $output->writeln('CPF invalido ' . $participante->getDocumento() . ' ' . $participante->getNome());

                $participante->setDocumento(null);
                $em->persist($participante);
                $em->flush();
            }
        }


        $output->writeln('Total ' . $cont);
    }

}
