<?php

namespace FCM\EventoBundle\Command;

use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\QuestionBundle\Entity\Resposta;
use FCM\QuestionBundle\Entity\RespostaItem;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;

class FcmEventoCheckcpemCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fcm:evento:checkcpem')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        /** @var EventoAtividade $eventoAtividade */
        $eventoAtividade = $em->getRepository('FCMEventoBundle:EventoAtividade')->find(1266);

        $cont = 0;

        /** @var Inscricao $inscricoe */
        foreach ($eventoAtividade->getInscricoes() as $inscricoe){
            /** @var Resposta $resposta */
            foreach ($inscricoe->getRespostaType(0) as $resposta){
                if($resposta->getPago() and !$inscricoe->getLiberado()){
                    $cont++;

                    $inscricao = $inscricoe;
                    $inscricao->setLiberado(true);

                    $em->persist($inscricao);
                    $em->flush();



                    /** @var Router $router */
                    $router = $this->getContainer()->get('router');

                    $url = $router->getContext()->getScheme().'://www.fcm.unicamp.br/eventos/' . $router->generate('certificado_show', ['token' => $inscricao->getToken(),],Router::RELATIVE_PATH);

                    $message = \Swift_Message::newInstance()
                        ->setSubject('[IA/Eventos] Liberação do certificado: ' . $inscricao->getEventoAtividade()->getEvento()->getNome())
                        ->setFrom($this->getContainer()->getParameter('mail'))
                        ->setReplyTo($inscricao->getEventoAtividade()->getEvento()->getEmail())
                        ->setCc($inscricao->getEventoAtividade()->getEvento()->getEmail())
                        ->setTo($inscricao->getParticipante()->getEmail())
                        ->setBody(
                            $this->getContainer()->get('templating')->render('FCMEventoBundle:Emails:presenca.html.twig',[
                                'inscricao' => $inscricao,
                                'link' => $url,
                            ]),
                            'text/html'
                        );
                    $this->getContainer()->get('mailer')->send($message);

                    $output->writeln('liberado ' . $inscricao->getParticipante()->getEmail() . ' url ' . $url);

                    if($cont > 50){
                        return;
                    }

                }
            }
        }



        $output->writeln('Liberados ' . $cont);
    }

}
