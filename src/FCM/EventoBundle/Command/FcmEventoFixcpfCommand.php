<?php

namespace FCM\EventoBundle\Command;

use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\Participante;
use FCM\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FcmEventoFixcpfCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fcm:evento:fixcpf')
            ->setDescription('Valida CPF e insere zero a esquerda se necessario')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $users = $this->getContainer()->get('doctrine')->getRepository('FCMUserBundle:User')->findBy(['vinculo' => 'Eventos']);

        foreach ($users as $user) {
            if (is_numeric($user->getDocumento()) and strlen($user->getDocumento()) < 11) {
                if(Participante::validateCPF($user->getDocumento())){
                    $user->setDocumento(str_pad($user->getDocumento(), 11, "0", STR_PAD_LEFT));
                    $user->setUsername(str_pad($user->getDocumento(), 11, "0", STR_PAD_LEFT));

                    /** @var User $userD */
                    if($userD = $this->getContainer()->get('doctrine')->getRepository('FCMUserBundle:User')->findOneBy(
                        ['vinculo' => 'Eventos', 'documento' =>$user->getDocumento()])){
                        $output->writeln('Duplicado ' . $user->getDocumento());

                        /** @var Participante $participante */
                        $participanteD = $userD->getParticipante();
                        $participante = $user->getParticipante();

                        /** @var Inscricao $inscricoe */
                        foreach ($participanteD->getInscricoes() as $inscricoe){
                            $inscricoe->setParticipante($participante);
                            $this->getContainer()->get('doctrine')->getManager()->persist($inscricoe);
                            $this->getContainer()->get('doctrine')->getManager()->flush($inscricoe);

                        }


                        $userD->setDocumento($userD->getDocumento().'X');
                        $userD->setUsername($userD->getDocumento().'X');

                        $this->getContainer()->get('doctrine')->getManager()->persist($userD);
                        $this->getContainer()->get('doctrine')->getManager()->flush($userD);

                        $output->writeln('fixed ' . $user->getDocumento());
                    }

                    $this->getContainer()->get('doctrine')->getManager()->persist($user);
                    $this->getContainer()->get('doctrine')->getManager()->flush($user);
                    $output->writeln('Fixed ' . $user->getDocumento());
                }
            }
        }

        $output->writeln('Fixed user');

        $users = $this->getContainer()->get('doctrine')->getRepository('FCMEventoBundle:Participante')->findAll();

        foreach ($users as $user) {
            if (is_numeric($user->getDocumento()) and Participante::validateCPF($user->getDocumento())) {
                $user->setDocumento(str_pad($user->getDocumento(), 11, "0", STR_PAD_LEFT));
                $this->getContainer()->get('doctrine')->getManager()->persist($user);
                $this->getContainer()->get('doctrine')->getManager()->flush();
            }
        }



        $output->writeln('Fixed participante');
    }
}
