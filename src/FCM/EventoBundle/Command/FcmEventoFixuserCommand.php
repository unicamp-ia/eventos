<?php

namespace FCM\EventoBundle\Command;

use FCM\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FcmEventoFixuserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fcm:evento:fixuser')
            ->setDescription('Exclui usuarios sem eventos')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        $em = $this->getContainer()->get('doctrine');

        $users = $em->getRepository('FCMUserBundle:User')->findAll();

        /** @var User $user */
        foreach ($users as $user){
            $delete = true;

            if($user->getEventoAdminCreatedBys()->count() or $user->getEventoAdminUsers()->count() or $user->getEventos()->count()){
                $delete = false;
            }

            // Nao excluir usuarios eventos e avaliadores
            if(!in_array($user->getVinculo(),['Eventos','Avaliador']) and $delete){
                $em->getManager()->remove($user);
                $output->writeln('Deleting ' . $user->getUsername());
            }
        }

        $em->getManager()->flush();

        $output->writeln('Command result.');
    }

}
