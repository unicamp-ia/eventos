<?php

namespace FCM\EventoBundle\Command;

use FCM\QuestionBundle\Entity\Resposta;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FcmEventoDepositoNotifyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fcm:evento:deposito:notify')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        $em = $this->getContainer()->get('doctrine');

        // Localiza questionarios que utilizam deposito
        $questionarios = $em->getRepository('FCMQuestionBundle:Questionario')->findBy(['deposito' => 1]);
        $today = new \DateTime();

        $today->modify('-1 week');


        foreach ($questionarios as $questionario){
            if($questionario->getEventoAtividade()->getDataIniAtividade() < $today) continue;

            $output->writeln('Procurando nao pagantes em ' . $questionario->getEventoAtividade()->getNome()
                . ' ' . $questionario->getEventoAtividade()->getEvento()->getNome());

            /** @var \Swift_Message $message */
            $message = \Swift_Message::newInstance()
                ->setSubject('[IA/Evento] Lembrete para depósito do evento ' . $questionario->getEventoAtividade()->getEvento()->getNome())
                ->setTo($questionario->getEventoAtividade()->getEvento()->getEmail())
                ->setFrom($this->getContainer()->getParameter('mail'))
                ->setReplyTo($questionario->getEventoAtividade()->getEvento()->getEmail())
                ->setBody(
                    $this->getContainer()->get('templating')->render('FCMEventoBundle:Emails:notify.deposito.html.twig',[
                        'eventoAtividade' => $questionario->getEventoAtividade(),
                    ]),
                    'text/html'
                );


            $total = 0;
            /** @var Resposta $resposta */
            foreach ($questionario->getRespostas() as $resposta){
                if(!$resposta->getDepositoName() and !$resposta->isPago() and $resposta->getInscricao()->getCreated() < $today) {

                    $total++;
                    $output->writeln('Notificando Inscricao :' . $resposta->getInscricao()->getId() . ' - ' .
                        $resposta->getInscricao()->getParticipante()->getNome()
                        . ' criado em ' . $resposta->getInscricao()->getCreated()->format('d/m/Y'). 'e-mail' .
                        $resposta->getInscricao()->getParticipante()->getEmail());

                    $message->setBcc($resposta->getInscricao()->getParticipante()->getEmail());


                }

            }

            $this->getContainer()->get('mailer')->send($message);
            $output->writeln('Total notificado ' . $total);

        }


        $output->writeln('Command result.');
    }

}
