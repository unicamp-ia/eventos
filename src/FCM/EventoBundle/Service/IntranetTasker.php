<?php
namespace FCM\EventoBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class IntranetTasker
{
    /** @var  EntityManager */
    protected $entityManager;

    protected $intranetRoot;


    /**
     * IntranetTasker constructor.
     * @param EntityManagerInterface $entityManager
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(EntityManagerInterface $entityManager, $intranetRoot)
    {
        $this->entityManager = $entityManager;
        $this->intranetRoot = $intranetRoot;


    }

    /**
     * @param $method
     * @param $url
     * @param bool $data
     * @return bool|string
     */
    public function callAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    /**
     * @param $username
     * @param $passowrd
     * @return bool|string
     */
    public function checkPassowrd($username, $passowrd){
        $resp = $this->callAPI('POST', $this->intranetRoot.'/api/user/checkpass',
            ['username' => $username, 'password' => $passowrd]);

        return $resp;
    }

    /**
     * @param $username
     * @return bool|string
     */
    public function getUser($username){

        $resp = $this->callAPI('GET', $this->intranetRoot.'/api/user/getUser',
            ['username' => $username]);

        $resp = json_decode($resp);
        return $resp;
    }

    /**
     * @param $cpf
     * @return bool
     */
    public function validateCPF($cpf){
        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }

        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }


}