<?php

namespace FCM\EventoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParticipanteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $translator = $options['translator'];


        $builder
            ->add('documento', TextType::class, [
                'label'=> $translator->trans('documento'),
                ])
            ->add('email', RepeatedType::class, [
                'type' => EmailType::class,
                'first_options'  => array('label' => 'E-mail'),
                'second_options' => array('label' => $translator->trans('confirmmail')),
                'invalid_message' => $translator->trans('naoigualemail'),
                'required' => true
            ])
            ->add('estrangeiro', CheckboxType::class,[
                'label' => 'Estrangeiro? (foreigners only)',
                'required' => false,

            ])
            ->add('nome', TextType::class,['label'=> $translator->trans('nome')])
            ->add('pass', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options'  => array('label' => $translator->trans('senha')),
                'second_options' => array('label' => $translator->trans('confirmsenha')),
                'options' => array('attr' => array('class' => 'password-field')),
                'invalid_message' => $translator->trans('naoigualsenha'),
                'required' => false,

            ]);

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\EventoBundle\Entity\Participante',
            'pass' => true,
            'translator' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fcm_eventobundle_participante';
    }


}
