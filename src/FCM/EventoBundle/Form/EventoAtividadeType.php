<?php

namespace FCM\EventoBundle\Form;

use FCM\EventoBundle\Entity\Atividade;
use FCM\EventoBundle\Entity\EventoAtividade;
use FCM\EventoBundle\Repository\AtividadeRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventoAtividadeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dataIniInscricao', DateTimeType::class, [
                'label' => 'Data do início das inscrições',
                'attr' => array('class'=>'form_date_picker_date'),
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'required' => false,
                ])
            ->add('dataFimInscricao', DateTimeType::class, [
                'label' => 'Data do término das inscrições',
                'attr' => array('class'=>'form_date_picker_date'),
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'required' => false,
                ])
            ->add('dataIniAtividade', DateTimeType::class, [
                'label' => 'Data e horário de início da atividade',
                'attr' => array('class'=>'form_date_picker_datetime'),
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy HH:mm',
                'required' => true,
                ])
            ->add('dataFimAtividade', DateTimeType::class, [
                'label' => 'Data e horário de término da atividade',
                'attr' => array('class'=>'form_date_picker_datetime'),
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy HH:mm',
                'required' => true,
                ])
            ->add('maxInscricao', NumberType::class, [
                'label' => 'Número máximo de inscrições, deixe em branco para inscrições ILIMITADAS',
                'required' => false,

            ])

            ->add('hide', CheckboxType::class,[
                'label' => 'Ativdade privada? Somente quem possuir o link poderá se inscrever',
                'required' => false,
            ])

            ->add('descricao', CKEditorType::class, [
                'label' => 'Descrição da atividade',
                'required' => false,
            ])
            ->add('modelo', CKEditorType::class, ['label' => 'Modelo do certificado'])

            ->add('nome', TextType::class, ['label' => 'Nome da ativdade']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\EventoBundle\Entity\EventoAtividade'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fcm_eventobundle_eventoatividade';
    }


}
