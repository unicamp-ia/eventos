<?php

namespace FCM\EventoBundle\Form;

use FCM\EventoBundle\Entity\Token;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TokenType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nome')
            ->add('nomeExibicao', TextType::class, [
                'label' => 'Nome para exibição'
            ])
            ->add('descricao', TextType::class, [
                'label' => 'Texto de ajuda do campo',
                'required' => false,
            ])
            ->add('tipo', ChoiceType::class, [
                'choices' => array_flip(Token::getChoicesTipo()),
            ])
            ->add('obrigatorio', CheckboxType::class, [
                'label' => 'Obrigatório',
                'required' => false,
            ])
            ->add('tamanho', NumberType::class, [
                'attr'=>array('placeholder'=> 'Deixe em branco para não limitar'),
                'required' => false,
            ])
            ->add('options', TextareaType::class, [
                'attr'=>array('placeholder'=> 'Insira um item por linha'),
                'required' => false,
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\EventoBundle\Entity\Token'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fcm_eventobundle_token';
    }


}
