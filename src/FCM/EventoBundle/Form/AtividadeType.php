<?php

namespace FCM\EventoBundle\Form;

use Doctrine\Common\Collections\ArrayCollection;
use FCM\EventoBundle\Entity\Atividade;
use FCM\EventoBundle\Entity\AtividadeToken;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AtividadeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Atividade $atividade */
        if($options['data'] instanceof Atividade) {
            $tokens = new ArrayCollection();
            $atividade = $options['data'];
            $atividade_tokens = $atividade->getAtividadeTokens();
            /** @var AtividadeToken $atividade_token */
            foreach($atividade_tokens as $atividade_token){
                $tokens->add(($atividade_token->getToken()));
            }
        }
        else $tokens = array();
        
        $builder->add('nome')
            ->add('multiple', CheckboxType::class, [
                'label' => 'Permitir múltiplas inscrições?',
                'required' => false,
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Ativo? se sim permite aos administradores de evento selecionar',
                'required' => false,
            ])
            ->add('avaliacao', CheckboxType::class, [
                'label' => 'Atividade terá avaliação?',
                'required' => false
            ])
            ->add('modelo', CKEditorType::class)
            ->add('tokens', EntityType::class, [
                'class' => 'FCMEventoBundle:Token',
                'choice_label' => 'nome',
                'mapped' => false,
                'multiple' => true,
                'expanded' => true,
                    'data' => $tokens->toArray(),
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\EventoBundle\Entity\Atividade'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fcm_eventobundle_atividade';
    }


}
