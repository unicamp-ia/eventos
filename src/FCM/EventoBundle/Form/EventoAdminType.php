<?php

namespace FCM\EventoBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class EventoAdminType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('user', Select2EntityType::class, [
            'label' => 'Usuários',
            'multiple' => true,
            'class' => 'FCMUserBundle:User',
            'remote_route' => 'admin_evento_admin_getUser',
            'primary_key' => 'id',
            'text_property' => 'username',
            'minimum_input_length' => 3,
            'page_limit' => 20,
            'allow_clear' => true,
            'delay' => 250,
            'cache' => true,
            'cache_timeout' => 60000, // if 'cache' is true
            'language' => 'pt-br',
            'placeholder' => 'Digite o nome do administrador',
            'mapped' => false,
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\EventoBundle\Entity\EventoAdmin'
        ));
    }

}
