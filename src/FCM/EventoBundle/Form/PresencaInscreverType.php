<?php

namespace FCM\EventoBundle\Form;

use FCM\EventoBundle\Entity\AtividadeToken;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\InscricaoToken;
use FCM\EventoBundle\Entity\Token;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PresencaInscreverType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('documento', TextType::class, [
            'label' => 'Documento',
            'required' => false,
        ])
            ->add('email',EmailType::class, [
                'label' => 'E-mail',
                'required' => false,
            ])
            ->add('estrangeiro', CheckboxType::class,[
                'label' => 'Estrangeiro',
                'required' => false,
            ])
        ;

        return $builder;
    }



    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }


}
