<?php

namespace FCM\EventoBundle\Form;

use FCM\EventoBundle\Entity\Token;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AtividadeTokenType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('atividade', EntityType::class, [
                'class' => 'FCMEventoBundle:Atividade',
                'choice_label' => 'nome'])
            ->add('token', EntityType::class, [
                'class' => 'FCMEventoBundle:Token',
                'choice_label' => 'nome']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\EventoBundle\Entity\AtividadeToken'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fcm_eventobundle_atividadetoken';
    }


}
