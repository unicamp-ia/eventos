<?php

namespace FCM\EventoBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class EventoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nome', TextType::class, [
            'label' => 'Nome do evento'
        ])
            ->add('local', TextType::class, [
                'label' => 'Local de realização do evento',
                'required' => false,
            ])
            ->add('alias', TextType::class, ['label' => 'Endereço do seu evento'])
            ->add('template', VichFileType::class,[
                'label' => 'Enviar modelo do certificado',
                'required' => false,
            ])
            ->add('cssCorpo', TextType::class, [
                'label' => 'CSS corpo do certificado',
                'required' => false,
            ])
            ->add('assinatura', VichFileType::class,[
                'label' => 'Assinatura',
                'required' => false,
            ])
            ->add('thumbnail', VichFileType::class,[
                'label' => 'Imagem de capa',
                'required' => false,
            ])
            ->add('descricao', CKEditorType::class, [
                'label' => 'Descrição do evento',
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mail do evento',
                'required' => true,
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FCM\EventoBundle\Entity\Evento'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fcm_eventobundle_evento';
    }


}
