<?php

namespace FCM\EventoBundle\Form;

use FCM\EventoBundle\Entity\AtividadeToken;
use FCM\EventoBundle\Entity\Inscricao;
use FCM\EventoBundle\Entity\InscricaoToken;
use FCM\EventoBundle\Entity\Token;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PresencaInscreverAutoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('documento', TextType::class, [
                'label' => 'Documento',
                'required' => true,
                'disabled' => true,
            ])
            ->add('nome', TextType::class, [
                'label' => 'Nome',
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mail do participante',
                'required' => true,
            ])
        ;

        return $builder;
    }



    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }


}
